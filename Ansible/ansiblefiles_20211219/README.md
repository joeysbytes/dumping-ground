# Ansible Files

**Note:** I mainly run my Dockerized Ansible server on a Raspberry Pi (I have tested on 2 / 3 / 4).  Building the Docker image is slow (on SD cards), but all other functionality runs just fine.

## Using the Docker Image

### Build the Docker Image

In the *docker* folder, run the *build_ansible.sh* script.  Everytime you run this script, the previous image is destroyed, and a new image is built with the latest version of Ansible.

### Run the Docker Image

#### run_ansible.sh Script Variables

In the *docker* folder, change the *run_ansible.sh* script to suit your needs.  All you should need to do is update the variables near the top.

| Variable | Description |
| --- | --- |
| ANSIBLE_SRC_DIR | Directory for the Ansible playbook and all associated files to run. |
| ANSIBLE_PLAYBOOKS_DIR | Base directory where all the playbooks are stored. |
| ANSIBLE_SSH_DIR | Directory for the .ssh files and known hosts. |
| ANSIBLE_HOSTS_FILE | Hosts (inventory) file to use for Ansible. |
| ANSIBLE_ENTRY_DIR | Directory of the entry file(s) scripts. |
| ANSIBLE_ENTRY_FILE | Script file that runs, which will start Ansible. |
| ANSIBLE_ENTRY_CONFIG_DIR | Directory which holds configuration for running the entry point script (see below). |
| ANSIBLE_VAULT_PASSWORD_FILE | File which contains the password(s) to decrypt Ansible Vault values. |

#### Included Entry Point Scripts

You can pass 1 variable in to *run_ansible.sh*.  If passed in, this becomes your entry point script, which is searched for in *ANSIBLE_ENTRY_DIR*.  Below are included scripts for you.

| Entry Point Script | Description |
| --- | --- |
| encrypt_string.sh | Using *ANSIBLE_VAULT_PASSWORD_FILE*, this script will prompt you for text and return the Ansible encrypted version. |
| entry.sh | The main entry point script which runs an Ansible playbook.  It is configuration driven (see below). |
| goto_bash.sh | A convenience script which will put you in a bash prompt inside the Ansible container. |

### Docker Image Layout

This is the file structure inside the Docker image.

```text
/home
 + ansibleuser
   + .ssh/                  # ANSIBLE_SSH_DIR
/ansible
 + src/                     # ANSIBLE_SRC_DIR
 + hosts                    # ANSIBLE_HOSTS_FILE
 + entry_points_config/     # ANSIBLE_ENTRY_CONFIG_DIR
 + entry_point.sh           # ANSIBLE_ENTRY_FILE
 + vault_passwords          # ANSIBLE_VAULT_PASSWORD_FILE
 + playbooks/               # ANSIBLE_PLAYBOOKS_DIR

```

## Using the Entry Point Script

Once you start *run_ansible.sh*, the container starts up and runs the *entry_point.sh* script.  By default, this is mapped to *entry_points/entry.sh*.

### Entry Point Configuration

This script builds the *ansible-playbook* command which needs to be run, and is configuration-driven.  At the top of the file, change *CONFIG_SOURCE* to match the base name (do not include the file extension) inside the config folder.  This file is sourced in to override the default values of the required variables.

#### Configuration Variables

| Variable | Default | Description / Valid Values |
| --- | --- | --- |
| PLAYBOOK_DIR | (none) | REQUIRED: Within the playbooks directory, the folder which contains the playbook to run. |
| PLAYBOOK | (none) | REQUIRED: The name of the playbook to run, in the playbook directory. |
| USER | (none) | Sets the user to connect as. |
| PASSWORD | 0 | 0 - n/a<br />1 - Prompt for user password. |
| BECOME_PASSWORD | 0 | 0 - n/a<br />1 - Prompt for password for sudo / root privileges. |
| TAGS | (none) | Comma-separated list of tags to run within the playbook. |
| SKIP_TAGS | (none) | Comma-separated list of tags to skip within the playbook. |
| START_AT_TASK | (none) | The task name to start at within the playbook. |
| VERBOSE | 0 | Valid values: 1 - 4.  How verbose the Ansible output should be. |
| VAULT_PASSWORD | 0 | 0 - n/a<br />1 - Use vault file<br />2 - Prompt for vault password |
