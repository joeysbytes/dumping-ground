# TODO: ssh once to get known hosts updated
# TODO: Cannot get ssh-add to work, causes annoyance when restarting and waiting
# TODO: Ansible docker manager

import os
import shutil
import subprocess


ANSIBLE_TEMP_DIR = "/tmp2/ansible"
ANSIBLE_SOURCE_DIR = os.path.abspath("../src")
ANSIBLE_PLAYBOOK_SOURCE_DIR = os.path.join(ANSIBLE_SOURCE_DIR, "playbooks")
USER_HOME_DIR=os.environ["HOME"]
ANSIBLE_PARAMETERS_LINE = "# run-parameters:"

last_playbook = None
last_host = None


def main():
    keep_running = True
    while keep_running:
        main_menu_items = [ "Run Ansible Playbook", "Quit" ]
        choice = menu(main_menu_items, title="Ansible Main Menu")
        if main_menu_items[choice] == "Quit":
            print("\nAll Done!\n\n")
            keep_running = False
        elif main_menu_items[choice] == "Run Ansible Playbook":
            run_playbook()


###########################################################################
# RUN ANSIBLE PLAYBOOK METHODS
###########################################################################

def run_playbook():
    """Get list of playbooks, prompt user which to run."""
    global last_playbook, last_host
    # Set up Ansible in temp directory
    delete_directory(ANSIBLE_TEMP_DIR)
    build_ansible_temp_dir()
    # Choose playbook to run
    playbook_menu_items, playbook_default = get_playbook_list()
    playbook_menu_items.append("Back to Main Menu")
    playbook_choice = menu(playbook_menu_items, title="Select Playbook", default_indexes=playbook_default)
    if playbook_menu_items[playbook_choice] != "Back to Main Menu":
        last_playbook = playbook_menu_items[playbook_choice]
        copy_chosen_playbook(playbook_menu_items[playbook_choice])
        # Choose host to run against
        host_menu_items, host_default = get_host_list()
        host_choice = menu(host_menu_items, title="Select Host", default_indexes=host_default)
        last_host = host_menu_items[host_choice]
        # Run the playbook
        run_playbook_command(playbook_menu_items[playbook_choice].split(">")[1].strip(), host_menu_items[host_choice])


def build_ansible_temp_dir():
    print("Building Ansible temp directory: " + ANSIBLE_TEMP_DIR)
    os.mkdir(ANSIBLE_TEMP_DIR)
    ansible_source_dirs = [ "group_vars", "host_vars", "roles" ]
    for ansible_source_dir in ansible_source_dirs:
        shutil.copytree(os.path.join(ANSIBLE_SOURCE_DIR, ansible_source_dir), os.path.join(ANSIBLE_TEMP_DIR, ansible_source_dir))
    shutil.copyfile(os.path.join(ANSIBLE_PLAYBOOK_SOURCE_DIR, "hosts.ini"), os.path.join(ANSIBLE_TEMP_DIR, "hosts.ini"))


def copy_chosen_playbook(playbook_menu_item):
    print("Copy playbook to run: " + playbook_menu_item)
    playbook_menu_parts = [ menu_part.strip() for menu_part in playbook_menu_item.split(">") ]
    shutil.copyfile(os.path.join(ANSIBLE_PLAYBOOK_SOURCE_DIR, playbook_menu_parts[0], playbook_menu_parts[1]),
                    os.path.join(ANSIBLE_TEMP_DIR, playbook_menu_parts[1]))


def get_playbook_list():
    print("Gather available playbooks to run")
    playbook_menu_items = []
    default_playbook = None
    playbook_dirs = [ playbook_dir for playbook_dir in os.listdir(ANSIBLE_PLAYBOOK_SOURCE_DIR)
                      if os.path.isdir(os.path.join(ANSIBLE_PLAYBOOK_SOURCE_DIR, playbook_dir)) ]
    for playbook_dir in sorted(playbook_dirs):
        playbook_files = [ playbook_file for playbook_file in os.listdir(os.path.join(ANSIBLE_PLAYBOOK_SOURCE_DIR, playbook_dir))
                           if os.path.isfile(os.path.join(ANSIBLE_PLAYBOOK_SOURCE_DIR, playbook_dir, playbook_file))
                           and os.path.splitext(os.path.join(ANSIBLE_PLAYBOOK_SOURCE_DIR, playbook_dir, playbook_file))[1] == ".yml" ]
        for playbook_file in sorted(playbook_files):
            playbook_menu_items.append(playbook_dir + " > " + playbook_file)
    if last_playbook is not None:
        for i in range(0, len(playbook_menu_items)):
            if last_playbook == playbook_menu_items[i]:
                default_playbook = i
                break
    return playbook_menu_items, default_playbook


def get_host_list():
    print("Gather available hosts to run playbook against")
    host_items = set()
    default_host = None
    with open(os.path.join(ANSIBLE_TEMP_DIR, "hosts.ini"), "rt") as host_file:
        for host_line in host_file:
            host_data = host_line.strip()
            if len(host_data) > 0 and not host_data.startswith("#"):
                host_items.add(host_data)
    host_item_list = list(sorted(host_items))
    if last_host is not None:
        for i in range(0, len(host_item_list)):
            if last_host == host_item_list[i]:
                default_host = i
                break
    return host_item_list, default_host


def run_playbook_command(playbook_to_run, host_to_use):
    print("Run the playbook")
   # Build the Ansible Command
    ansible_command = "ansible-playbook "
    ansible_command += "--inventory=./hosts.ini "
    # Get the host to run against
    host_to_use = host_to_use.replace("[", "").replace("]", "").strip()
    ansible_command += "--extra-vars=\\\"the_host=" + host_to_use + "\\\" "
    # Get the parameters from inside the playbook
    with open(os.path.join(ANSIBLE_TEMP_DIR, playbook_to_run), 'rt') as playbook_file:
        for playbook_line in playbook_file:
            if playbook_line.find(ANSIBLE_PARAMETERS_LINE, 0, len(ANSIBLE_PARAMETERS_LINE)) == 0:
                ansible_command += playbook_line[playbook_line.find(":") + 1:].strip() + " "
                break
    ansible_command += playbook_to_run
    # Build the Docker command
    docker_command = "docker run --rm --interactive --tty "
    docker_command += "--name ansible "
    docker_command += "--volume \"${HOME}/.ssh/known_hosts\":\"/home/ansibleuser/.ssh/known_hosts\" "
    docker_command += "--volume \"${HOME}/.ssh/id_rsa\":\"/home/ansibleuser/.ssh/id_rsa\" "
    docker_command += "--volume \"${HOME}/.ssh/id_rsa.pub\":\"/home/ansibleuser/.ssh/id_rsa.pub\" "
    docker_command += "--volume \"" + ANSIBLE_TEMP_DIR + "\":\"/ansible/data\" "
    docker_command += "joeysbytes/ansible bash -c \""
    docker_command += ansible_command + "\""
    # Run the Docker command
    print("\n" + str(docker_command) + "\n")
    subprocess.run(docker_command, shell=True)


###########################################################################
# UTILITY METHODS
###########################################################################

def delete_directory(directory):
    """If a directory exists, delete it and all its contents."""
    if os.path.isdir(directory):
        print("Deleting directory: " + directory)
        shutil.rmtree(directory)


def menu(items, title=None, prompt="Enter choice: ", pre_menu_text=None, post_menu_text=None,
        multiple_choice=False, default_indexes=None):
    """One-Method-Menu: https://gitlab.com/joeysbytes/one-method-python/tree/master/one-method-menu"""
    newline, menu_choices, menu_text, user_selections, item_separator = "\n", None, "\n\n", None, ","
    if len(items) == 0: raise ValueError("No menu items provided")
    if default_indexes is not None:
        if not hasattr(default_indexes, "__iter__"): default_indexes = [ default_indexes ]
        default_indexes = tuple(sorted(set([ int(idx) for idx in default_indexes ])))
        if not multiple_choice: default_indexes = (default_indexes[0],)
    if title is not None: menu_text += ("=" * len(str(title)) + newline + str(title) + newline + "=" * len(str(title)) + newline * 2)
    if pre_menu_text is not None: menu_text += (str(pre_menu_text) + newline * 2)
    for i in range(0, len(items)):
        menu_text += " " if default_indexes is None or i not in default_indexes else "*"
        menu_text += "{:2d}".format(i + 1) + ") " + str(items[i]) + newline
    if post_menu_text is not None: menu_text += (newline + str(post_menu_text) + newline)
    menu_text += (newline + str(prompt if prompt is not None else ""))
    while menu_choices is None:
        try:
            if not multiple_choice:
                user_choice = input(menu_text).strip()
                user_choice = default_indexes[0] + 1 if default_indexes is not None and len(user_choice) == 0 else int(user_choice)
                if user_choice < 1 or user_choice > len(items): raise ValueError
                menu_choices = user_choice - 1
            else:
                user_choices = input(menu_text)
                if default_indexes is not None and len(user_choices.strip()) == 0:
                    user_selections = [ idx + 1 for idx in default_indexes ]
                else:
                    user_selections = [ int(user_choice.strip()) for user_choice in user_choices.split(item_separator) ]
                menu_set = set()
                for user_choice in user_selections:
                    if user_choice < 1 or user_choice > len(items): raise ValueError
                    menu_set.add(user_choice - 1)
                menu_choices = tuple(sorted(list(menu_set)))
        except ValueError: print(newline + "*** INVALID CHOICE(S) ***")
    return menu_choices


main()
