#!/bin/bash
set -e

###########################################################################
# Convenience script to run Ansible playbooks with Docker.
#
# Special Requirement:
#   The playbook needs a commented line in the format:
#     # run: ansible-playbook command to run
#   The command will be extracted from this and passed to the Docker container.
###########################################################################

# VirtualBox variables
RESTORE_SNAPSHOT="false"
VM_NAME="LilServer01 vm00"
VM_SNAPSHOT="Initial Steps Done"
RESTART_HEADLESS="false"

# Ansible Playbook variables
RUN_PLAYBOOK="true"
PLAYBOOK_DIR="joey_workstations"
PLAYBOOK="adhoc.yml"
THE_HOST="joey_workstation_test"
# THE_HOST="joey_laptop"
TEMP_DIR="/tmp2/ansible"


###########################################################################
# Main Processing Logic
###########################################################################
function main() {
    if [ "${RESTORE_SNAPSHOT}" == "true" ]
    then
        restore_virtualbox_snapshot
    fi
    if [ "${RUN_PLAYBOOK}" == "true" ]
    then
        clean_temp_directory
        build_temp_directory
        run_ansible
        #clean_temp_directory
    fi
}


###########################################################################
# Remove temp Ansible directory
###########################################################################
function clean_temp_directory() {
    if [ -d "${TEMP_DIR}" ]
    then
        echo "Removing temp Ansible directory: ${TEMP_DIR}"
        rm -rf "${TEMP_DIR}"
    fi
}


###########################################################################
# Copy Ansible source files to temp directory
###########################################################################
function build_temp_directory() {
    echo "Building Ansible temp directory: ${TEMP_DIR}"
    mkdir -p "${TEMP_DIR}"
    cp -r "../src/group_vars" "${TEMP_DIR}/"
    cp -r "../src/host_vars" "${TEMP_DIR}/"
    cp -r "../src/roles" "${TEMP_DIR}/"
    cp "../src/playbooks/hosts.ini" "${TEMP_DIR}/"
    cp "../src/playbooks/${PLAYBOOK_DIR}/${PLAYBOOK}" "${TEMP_DIR}/"
}


###########################################################################
# Run Ansible playbook
###########################################################################
function run_ansible() {
    echo "Running Ansible Playbook within Docker: ${PLAYBOOK_DIR}/${PLAYBOOK}"
    local ansible_command=$(cat "${TEMP_DIR}/${PLAYBOOK}"|grep "#"|grep "run:"|cut -f2 -d:|sed -e 's/^[[:space:]]*//')
    if [ "${ansible_command}" == "" ]
    then
        echo "No Ansible command found in playbook"
        exit 1
    fi
    ansible_command="${ansible_command} --extra-vars \"the_host=${THE_HOST}\""
    echo "Ansible command: ${ansible_command}"
    docker run \
        --name ansible \
        --volume "${HOME}/.ssh/known_hosts":"/home/ansibleuser/.ssh/known_hosts" \
        --volume "${HOME}/.ssh/id_rsa":"/home/ansibleuser/.ssh/id_rsa" \
        --volume "${HOME}/.ssh/id_rsa.pub":"/home/ansibleuser/.ssh/id_rsa.pub" \
        --volume "${TEMP_DIR}":"/ansible/data" \
        --rm \
        --interactive \
        --tty \
        joeysbytes/ansible bash -c "${ansible_command}"
}


###########################################################################
# Stop a VirtualBox machine, restore to a snapshot, the start it.
# There are sleep commands here to prevent issues.
###########################################################################
function restore_virtualbox_snapshot {
    echo "Restoring ${VM_NAME} to snapshot ${VM_SNAPSHOT}"
    # Power off virtual machine, if needed
    local vm_status=$(vboxmanage showvminfo "${VM_NAME}"|grep "State:"|cut -f2 -d:|cut -f1 -d\(|tr -d "[:space:]")
    if [ "${vm_status}" == "poweredoff" ]
    then
        echo "${VM_NAME} is already powered off"
    else
        echo "Powering off ${VM_NAME}"
        vboxmanage controlvm "${VM_NAME}" poweroff
        do_sleep 5
    fi
    # Restore snapshot
    echo "Restore ${VM_NAME} to snapshot ${VM_SNAPSHOT}"
    vboxmanage snapshot "${VM_NAME}" restore "${VM_SNAPSHOT}"
    # Power on virtual machine
    if [ "${RESTART_HEADLESS}" == "true" ]
    then
        echo "Power On ${VM_NAME} headless"
        vboxmanage startvm "${VM_NAME}" --type headless
    else
        echo "Power On ${VM_NAME}"
        vboxmanage startvm "${VM_NAME}"
    fi
    do_sleep 20
}

###########################################################################
# Let user know a sleep is occurring
###########################################################################
function do_sleep() {
    local sleep_secs=$1
    echo "Sleeping for ${sleep_secs} seconds..."
    sleep ${sleep_secs}
}


main
