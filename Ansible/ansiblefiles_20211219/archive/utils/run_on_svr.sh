#!/bin/bash
set -e

###########################################################################
# This script will run Ansible from the Ansible server, which is
# Dockerized.
#
# Required Parameters:
#   1) Playbook to run
#   2) Host to run playbook against
###########################################################################


###########################################################################
# GLOBAL VARIABLES / CONSTANTS
###########################################################################

#--------------------------------------------------------------------------
# Variables
#--------------------------------------------------------------------------
# Populated from passed-in parameters
PLAYBOOK=""
PLAYBOOK_HOST=""

# Script will change these values as needed
PARAMETERS_VALIDATED="false"

#--------------------------------------------------------------------------
# Constants
#--------------------------------------------------------------------------
RUN_PARAMETERS_REGEX="^# run-parameters: "
PLAYBOOK_VERBOSITY_FLAG="-v" # set to empty string if not desired
PLAYBOOK_TEMPLATE_FILE="run_playbook.sh"

# This local computer
LOCAL_WORK_DIR="/tmp2/ansible"
# Make sure you are running from the utils directory
LOCAL_UTILS_DIR=$(pwd)
LOCAL_TEMPLATES_DIR="${LOCAL_UTILS_DIR}/templates"
LOCAL_SRC_DIR="${LOCAL_UTILS_DIR}/../src"
LOCAL_PLAYBOOKS_DIR="${LOCAL_SRC_DIR}/playbooks"
COPY_LOCAL_RSA_KEYS_TO_SSH_ROLE="true"

# Remote Ansible server
SERVER_USER_PASSWORD_FILE="/data/credentials/ansible_server.credentials"
SERVER_WORK_DIR="/tmp2/ansible"
SERVER_ADDRESS=$(cat "${SERVER_USER_PASSWORD_FILE}"|cut -f1 -d\ )
SERVER_USER=$(cat "${SERVER_USER_PASSWORD_FILE}"|cut -f2 -d\ )

# Colors
DEFAULT_COLORS='\033[0m'
LOG_TIMESTAMP_COLOR='\033[0;30m\033[46m'
LOG_ERROR_TIMESTAMP_COLOR='\033[0;93m\033[41m'


###########################################################################
# MAIN PROCESSING LOGIC
###########################################################################

#--------------------------------------------------------------------------
# Run Ansible Playbook
#--------------------------------------------------------------------------
function main() {
    log "BEGIN: Run Ansible Playbook"
    parameter_validations "$@"
    if [ "${PARAMETERS_VALIDATED}" == "true" ]
    then
        clean_local_work_dir
        build_ansible_source_folder
        build_ansible_playbook_run_script
        build_ansible_source_file
        get_server_user_password
        clean_server_work_dir
        copy_ansible_source_file_to_server
        run_ansible_playbook
    fi
    log "END: Run Ansible Playbook"
}


###########################################################################
# VALIDATION FUNCTIONS
###########################################################################

#--------------------------------------------------------------------------
# Validate passed-in parameters
#--------------------------------------------------------------------------
function parameter_validations() {
    log "Performing parameter validations"
    local validated="true"
    # Check number of parameters
    if [ $# -eq 0 ]
    then
        logerror "No parameters passed in"
        print_usage "$@"
        validated="false"
    elif [ $# -ne 2 ]
    then
        local parameters=""
        for p in "$@"
        do
            parameters="${parameters}${p} "
        done
        logerror "Invalid number of parameters: $#"
        logerror "  Parameters found: ${parameters}"
        print_usage "$@"
        validated="false"
    fi
    # Check playbook exists
    if [ "${validated}" == "true" ]
    then
        local playbook="${LOCAL_PLAYBOOKS_DIR}/${1}"
        if [ ! -f "${playbook}" ]
        then
            logerror "Playbook not found: ${1}"
            validated="false"
        fi
    fi
    # Everything passed, allow the script to continue
    if [ "${validated}" == "true" ]
    then
        log "  Parameter validations passed"
        PLAYBOOK="${1}"
        PLAYBOOK_HOST="${2}"
        PARAMETERS_VALIDATED="true"
    fi
}


#--------------------------------------------------------------------------
# Print how to call this script
#--------------------------------------------------------------------------
function print_usage() {
    logerror "USAGE: ${0} playbookToRun ansibleHost"
    logerror "  playbookToRun: Ansible playbook file to run,"
    logerror "                 located in playbooks directory"
    logerror "  ansibleHost:   Ansible host to run playbook against"
}


###########################################################################
# ANSIBLE FUNCTIONS
###########################################################################

#--------------------------------------------------------------------------
# Run Ansible playbook on remote server, which is Dockerized
#--------------------------------------------------------------------------
function run_ansible_playbook() {
    log "Running Ansible playbook:"
    log "  Playbook: ${PLAYBOOK}"
    log "  Host:     ${PLAYBOOK_HOST}"
    local cmd=""
    cmd="${cmd}cd \"${SERVER_WORK_DIR}/ansible\"; "
    cmd="${cmd}./run_playbook.sh "
    run_remote_interactive_command "${cmd}"
}


#--------------------------------------------------------------------------
# This takes the Ansible source files and copies them to the local work
# directory in the proper folder structure.
#--------------------------------------------------------------------------
function build_ansible_source_folder() {
    local ansible_dir="${LOCAL_WORK_DIR}/ansible"
    log "Building Ansible source folder: ${ansible_dir}"
    mkdir "${ansible_dir}"
    log "  Copy group_vars"
    cp -R "${LOCAL_SRC_DIR}/group_vars" "${ansible_dir}/"
    log "  Copy host_vars"
    cp -R "${LOCAL_SRC_DIR}/host_vars" "${ansible_dir}/"
    log "  Copy roles"
    cp -R "${LOCAL_SRC_DIR}/roles" "${ansible_dir}/"
    if [ "${COPY_LOCAL_RSA_KEYS_TO_SSH_ROLE}" == "true" ]
    then
        log "  Copy local rsa keys to ssh role"
        cp "${HOME}/.ssh/id_rsa"* "${ansible_dir}/roles/ssh/files/"
    fi
    log "  Copy playbooks"
    cp "${LOCAL_PLAYBOOKS_DIR}/"* "${ansible_dir}/"
}


#--------------------------------------------------------------------------
# This takes the run_playbook.sh script, substitutes the required
# variables needed to run the playbook as requested, and adds it
# to the Ansible source folder.
#--------------------------------------------------------------------------
function build_ansible_playbook_run_script() {
    log "Building Ansible playbook run script"
    local ansible_dir="${LOCAL_WORK_DIR}/ansible"
    log "  Copying template: ${LOCAL_TEMPLATES_DIR}/${PLAYBOOK_TEMPLATE_FILE}"
    cp "${LOCAL_TEMPLATES_DIR}/${PLAYBOOK_TEMPLATE_FILE}" "${ansible_dir}"
    chmod +x "${ansible_dir}/${PLAYBOOK_TEMPLATE_FILE}"

    local server_ansible_dir="\"${SERVER_WORK_DIR}/ansible\""
    log "  Adding Ansible data directory: ${server_ansible_dir}"
    # because variables use /, change sed delimeter to |
    sed -i "s|ANSIBLE_DATA_DIR=|ANSIBLE_DATA_DIR=${server_ansible_dir}|g" "${ansible_dir}/${PLAYBOOK_TEMPLATE_FILE}"

    log "  Building Ansible playbook command"
    local playbook_parameters=$(cat "${ansible_dir}/${PLAYBOOK}"|grep -e "${RUN_PARAMETERS_REGEX}"|cut -f2 -d:)
    local ansible_cmd=""
    ansible_cmd="${ansible_cmd}\""
    ansible_cmd="${ansible_cmd}ansible-playbook "
    ansible_cmd="${ansible_cmd}${PLAYBOOK} "
    ansible_cmd="${ansible_cmd}--inventory=/ansible/hosts.yml "
    # double up the slashes due to sed removing 1 set
    ansible_cmd="${ansible_cmd}--extra-vars=\\\\\"the_host=${PLAYBOOK_HOST}\\\\\" "
    ansible_cmd="${ansible_cmd}${playbook_parameters} "
    ansible_cmd="${ansible_cmd}${PLAYBOOK_VERBOSITY_FLAG}"
    ansible_cmd="${ansible_cmd}\""
    log "  Adding Ansible playbook command"
    # because variables use /, change sed delimeter to |
    sed -i "s|ANSIBLE_COMMAND=|ANSIBLE_COMMAND=${ansible_cmd}|g" "${ansible_dir}/${PLAYBOOK_TEMPLATE_FILE}"
}


#--------------------------------------------------------------------------
# This compresses the ansible source folder in to ansible.tar.gz.
#--------------------------------------------------------------------------
function build_ansible_source_file() {
    log "Building Ansible source file: ${LOCAL_WORK_DIR}/ansible.tar.gz"
    log "  Create tar file"
    cd "${LOCAL_WORK_DIR}"
    tar -c -f ansible.tar ansible
    log "  Gzip tar file"
    gzip -9 ansible.tar
    cd "${LOCAL_UTILS_DIR}"
}


#--------------------------------------------------------------------------
# Copy Ansible source file to server and decompress it.
#--------------------------------------------------------------------------
function copy_ansible_source_file_to_server() {
    log "Copy Ansible source file to server dir: ${SERVER_WORK_DIR}"
    sshpass -e scp "${LOCAL_WORK_DIR}/ansible.tar.gz" "${SERVER_USER}@${SERVER_ADDRESS}:${SERVER_WORK_DIR}"
    local cmd=""
    cmd="${cmd}cd \"${SERVER_WORK_DIR}\"; "
    cmd="${cmd}gunzip ansible.tar.gz; "
    cmd="${cmd}tar -xf ansible.tar; "
    cmd="${cmd}rm ansible.tar; "
    log "  Uncompress Ansible source file"
    run_remote_command "${cmd}"
}


#--------------------------------------------------------------------------
# Get the server password for the server user.
# This is stored in SSHPASS for use with the SSHPASS command.
#--------------------------------------------------------------------------
function get_server_user_password() {
    log "Getting password for server user"
    SSHPASS=$(cat "${SERVER_USER_PASSWORD_FILE}"|cut -f3 -d\ )
    export SSHPASS
}


#--------------------------------------------------------------------------
# Remove all files from *local* temporary directory, if it exists.
# If the directory does not exist, it will be created.
#--------------------------------------------------------------------------
function clean_local_work_dir() {
    log "Cleaning local work directory: ${LOCAL_WORK_DIR}"
    if [ -d "${LOCAL_WORK_DIR}" ]
    then
        log "  Found local working directory"
        cd "${LOCAL_WORK_DIR}"
        rm -rf *
        cd "${LOCAL_UTILS_DIR}"
    else
        log "  Local working directory does not exist, creating"
        mkdir -p "${LOCAL_WORK_DIR}"
    fi
}


#--------------------------------------------------------------------------
# Remove all files from *remote* temporary directory, if it exists.
# If the directory does not exist, it will be created.
#--------------------------------------------------------------------------
function clean_server_work_dir() {
    log "Cleaning server work directory: ${SERVER_WORK_DIR}"
    local cmd=""
    cmd="${cmd}if [ -d \"${SERVER_WORK_DIR}\" ]; then "
    cmd="${cmd}echo \"  Found server working directory\"; "
    cmd="${cmd}cd \"${SERVER_WORK_DIR}\"; "
    cmd="${cmd}rm -rf *; "
    cmd="${cmd}else "
    cmd="${cmd}echo \"  Server working directory does not exist, creating\"; "
    cmd="${cmd}mkdir -p \"${SERVER_WORK_DIR}\"; "
    cmd="${cmd}fi"
    run_remote_command "${cmd}"
}


#--------------------------------------------------------------------------
# Given a command string, run it on the remote server
#--------------------------------------------------------------------------
function run_remote_command() {
    local cmd="${1}"
    sshpass -e ssh ${SERVER_USER}@${SERVER_ADDRESS} "${cmd}"
}


#--------------------------------------------------------------------------
# Given a command string, run it on the remote server in interactive mode.
#--------------------------------------------------------------------------
function run_remote_interactive_command() {
    local cmd="${1}"
    sshpass -e ssh -t ${SERVER_USER}@${SERVER_ADDRESS} "${cmd}"
}


###########################################################################
# UTILITY FUNCTIONS
###########################################################################

#--------------------------------------------------------------------------
# Print text, with some colorful timestamp
#--------------------------------------------------------------------------
function log() {
    local text_msg="${1}"
    local timestamp=$(date +%Y%m%d_%H%M%S)
    echo -e "${LOG_TIMESTAMP_COLOR}${timestamp}${DEFAULT_COLORS} ${text_msg}"
}


#--------------------------------------------------------------------------
# Print text as error, with some colorful timestamp
#--------------------------------------------------------------------------
function logerror() {
    local text_msg="${1}"
    local timestamp=$(date +%Y%m%d_%H%M%S)
    echo -e "${LOG_ERROR_TIMESTAMP_COLOR}${timestamp} ERROR:${DEFAULT_COLORS} ${text_msg}"
}


main "$@"
