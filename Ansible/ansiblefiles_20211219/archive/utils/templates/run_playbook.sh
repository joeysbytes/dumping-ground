#!/bin/bash

ANSIBLE_COMMAND=
ANSIBLE_DATA_DIR=
ANSIBLE_HOSTS_YML_FILE="/data/credentials/ansible/ansible_hosts.yml"

docker run \
    --name ansible \
    --volume "${HOME}/.ssh/known_hosts":"/home/ansibleuser/.ssh/known_hosts" \
    --volume "${HOME}/.ssh/id_rsa":"/home/ansibleuser/.ssh/id_rsa" \
    --volume "${HOME}/.ssh/id_rsa.pub":"/home/ansibleuser/.ssh/id_rsa.pub" \
    --volume "${ANSIBLE_DATA_DIR}":"/ansible/data" \
    --volume "${ANSIBLE_HOSTS_YML_FILE}":"/ansible/hosts.yml" \
    --rm \
    --interactive \
    --tty \
    joeysbytes/ansible bash -c "${ANSIBLE_COMMAND}"
