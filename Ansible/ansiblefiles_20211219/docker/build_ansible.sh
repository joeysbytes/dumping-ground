ansible_image_exists=$(docker images|grep "joeysbytes/ansible"|wc -l)
if [ ! "${ansible_image_exists}" == "0" ]
then
    docker rmi joeysbytes/ansible
fi

docker build \
    --tag joeysbytes/ansible \
    --file ./Dockerfile \
    --pull \
    --rm \
    --force-rm \
    .

docker image prune --force
