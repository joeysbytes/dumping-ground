#!/bin/bash

# Run the Ansible Docker image.
# Parameters:
#   1 - (optional) Entry point script

# NOTE: Docker expects full absolute paths for mounting, not relative paths

# Prefix Variables
ANSIBLE_BASE_DIR="/data/ansiblesvr"
ANSIBLE_SRC_BASE_DIR="${ANSIBLE_BASE_DIR}/ansible-files"
ANSIBLE_HOSTS_DIR="${ANSIBLE_SRC_BASE_DIR}/inventory"

# These are the variables that are used below
ANSIBLE_SRC_DIR="${ANSIBLE_SRC_BASE_DIR}/src"
ANSIBLE_PLAYBOOKS_DIR="${ANSIBLE_SRC_BASE_DIR}/playbooks"
ANSIBLE_SSH_DIR="${ANSIBLE_BASE_DIR}/ssh"
ANSIBLE_HOSTS_FILE="${ANSIBLE_HOSTS_DIR}/ansible-hosts.yml"
ANSIBLE_ENTRY_DIR="${ANSIBLE_SRC_BASE_DIR}/entry_points"

ANSIBLE_ENTRY_FILE="${ANSIBLE_ENTRY_DIR}/entry.sh"
#ANSIBLE_ENTRY_FILE="${ANSIBLE_ENTRY_DIR}/encrypt_string.sh"
#ANSIBLE_ENTRY_FILE="${ANSIBLE_ENTRY_DIR}/goto_bash.sh"

ANSIBLE_ENTRY_CONFIG_DIR="${ANSIBLE_ENTRY_DIR}/config"
ANSIBLE_VAULT_PASSWORD_FILE="${ANSIBLE_BASE_DIR}/credentials/vault_passwords"

# Apply any arguments passed in
if [ "$#" -gt 1 ]
then
    echo "USAGE: run_ansible.sh [entry_point_script]"
    exit 1
elif [ "$#" -eq 1 ]
then
    ANSIBLE_ENTRY_FILE="${ANSIBLE_ENTRY_DIR}/${1}"
fi

# Display values being used
echo ""
echo "Running Ansible from Docker"
echo "  Source Directory:    ${ANSIBLE_SRC_DIR}"
echo "  Playbooks Directory: ${ANSIBLE_PLAYBOOKS_DIR}"
echo "  .ssh Directory:      ${ANSIBLE_SSH_DIR}"
echo "  Hosts File:          ${ANSIBLE_HOSTS_FILE}"
echo "  Entry Point File:    ${ANSIBLE_ENTRY_FILE}"
echo "  Vault Password File: ${ANSIBLE_VAULT_PASSWORD_FILE}"
echo ""

# Run Ansible
docker run \
    --name ansible \
    --volume "${ANSIBLE_SSH_DIR}":"/home/ansibleuser/.ssh" \
    --volume "${ANSIBLE_SRC_DIR}":"/ansible/src" \
    --volume "${ANSIBLE_PLAYBOOKS_DIR}":"/ansible/playbooks" \
    --volume "${ANSIBLE_ENTRY_FILE}":"/ansible/entry_point.sh" \
    --volume "${ANSIBLE_ENTRY_CONFIG_DIR}":"/ansible/entry_points_config" \
    --volume "${ANSIBLE_HOSTS_FILE}":"/ansible/hosts" \
    --volume "${ANSIBLE_VAULT_PASSWORD_FILE}":"/ansible/vault_passwords" \
    --rm \
    --interactive \
    --tty \
    joeysbytes/ansible
