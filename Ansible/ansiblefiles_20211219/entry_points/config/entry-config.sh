# Entry point configuration file

# Required:
PLAYBOOK_DIR="svr01b"
PLAYBOOK="svr01b_1a.yml"

# Optional:
USER="ubuntu"
PASSWORD=1            # 0=n/a, 1=prompt
BECOME_PASSWORD=0     # 0=n/a, 1=prompt
TAGS=""               # comma-separated keywords
SKIP_TAGS=""          # comma-separated keywords
START_AT_TASK=""
VERBOSE=1             # Values 0-4
VAULT_PASSWORD=1      # 0=n/a, 1=use vault file, 2=prompt
