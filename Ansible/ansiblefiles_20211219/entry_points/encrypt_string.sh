#!/bin/bash
set -e

VAULT_PASSWORD_FILE="../vault_passwords"

echo ""
echo "ENCRYPT A STRING"
echo "================"
echo "Using Vault Password File: ${VAULT_PASSWORD_FILE}"
echo ""

ansible-vault encrypt_string --vault-password-file "${VAULT_PASSWORD_FILE}"
