#!/bin/bash
set -e

# This script builds the Ansible playbook command from the variables provided
# and executes it.

# Variables to drive the building of the Ansible Playbook command can be found
# in "initialize_variables". Override these variables in the config folder for
# the playbook you wish to run.
# Leave variables at empty string if you do not wish to use them

CONFIG_SOURCE=entry-config


function main() {
    pwd
    initialize_variables
    source_variables
    copy_playbook
    add_command
    add_playbook
    add_user
    add_password
    add_become_password
    add_vault_password
    add_inventory
    add_tags
    add_skip_tags
    add_start_at_task
    add_verbosity
    set +e
    run_ansible_playbook
    set -e
    uncopy_playbook
}


function initialize_variables() {
    echo "Initializing entry point variables"
    # Required:
    PLAYBOOK_DIR=""
    PLAYBOOK=""
    # Optional:
    USER=""
    PASSWORD=0            # 0=n/a, 1=prompt
    BECOME_PASSWORD=0     # 0=n/a, 1=prompt
    TAGS=""               # comma-separated keywords
    SKIP_TAGS=""          # comma-separated keywords
    START_AT_TASK=""
    VERBOSE=0             # Values 0-4
    VAULT_PASSWORD=0      # 0=n/a, 1=use vault file, 2=prompt
}


function source_variables() {
    echo "Sourcing entry point variables"
    source "../entry_points_config/${CONFIG_SOURCE}.sh"
}


function copy_playbook() {
    echo "Copying playbook to root directory"
    cp -v "../playbooks/${PLAYBOOK_DIR}/${PLAYBOOK}" "."
}


function uncopy_playbook() {
    echo "Removing playbook from root directory"
    rm -v "./${PLAYBOOK}"
}


function add_command() {
    echo "Building Ansible Playbook Command"
    cmd="ansible-playbook"
}


function add_playbook() {
    echo "  Playbook to run: ${PLAYBOOK_DIR}/${PLAYBOOK}"
    cmd="${cmd} ${PLAYBOOK}"
}


function add_inventory() {
    local inventory_file="/ansible/hosts"
    echo "  Inventory file: ${inventory_file}"
    cmd="${cmd} --inventory \"${inventory_file}\""
}


function add_user() {
    if [ ! "${USER}" == "" ]
    then
        echo "  User: ${USER}"
        cmd="${cmd} --extra-vars \"ansible_user=${USER}\""
    else
        echo "  User: [default]"
    fi
}


function add_password() {
    # 0=n/a, 1=prompt
    if [ "${PASSWORD}" -eq 1 ]
    then
        echo "  Password: will be prompted"
        cmd="${cmd} --ask-pass"
    else
        echo "  Password: n/a"
    fi
}


function add_become_password() {
    # 0=n/a, 1=prompt
    if [ "${BECOME_PASSWORD}" -eq 1 ]
    then
        echo "  Become Password: will be prompted"
        cmd="${cmd} --ask-become-pass"
    else
        echo "  Become Password: n/a"
    fi
}


function add_tags() {
    # comma-separated keywords
    if [ ! "${TAGS}" == "" ]
    then
        echo "  Run tags: ${TAGS}"
        cmd="${cmd} --tags \"${TAGS}\""
    fi
}


function add_skip_tags() {
    # comma-separated keywords
    if [ ! "${SKIP_TAGS}" == "" ]
    then
        echo "  Skip tags: ${SKIP_TAGS}"
        cmd="${cmd} --skip-tags \"${SKIP_TAGS}\""
    fi
}


function add_start_at_task() {
    if [ ! "${START_AT_TASK}" == "" ]
    then
        echo "  Start at task: ${START_AT_TASK}"
        cmd="${cmd} --start-at-task \"${START_AT_TASK}\""
    fi
}


function add_vault_password() {
    # 0=n/a, 1=use default vault file, 2=prompt
    if [ "${VAULT_PASSWORD}" -eq 1 ]
    then
        local vault_password_file="/ansible/vault_passwords"
        echo "  Vault password: Using password file: ${vault_password_file}"
        cmd="${cmd} --vault-password-file ${vault_password_file}"
    elif [ "${VAULT_PASSWORD}" -eq 2 ]
    then
        echo "  Vault password: will be prompted"
        cmd="${cmd} --ask-vault-pass"
    else
        echo "  Vault password: n/a"
    fi
}


function add_verbosity() {
    # Values 0-4
    echo "  Verbose level: ${VERBOSE}"
    if [ "${VERBOSE}" -ge 1 ] && [ "${VERBOSE}" -le 4 ]
    then
        cmd="${cmd} -"
        for ((i=0;i<${VERBOSE};i++))
        do
            cmd="${cmd}v"
        done
    fi
}


function run_ansible_playbook() {
    echo "Running Ansible Playbook"
    eval "${cmd}"
}


main
