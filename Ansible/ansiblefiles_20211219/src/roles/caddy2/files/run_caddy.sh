#!/bin/bash

# This starts the Caddy Web Server
# Important Note: Do not treat the caddy data directory as a cache.
#                 Do not delete this folder to "start over".


CADDY_BASE_DIR="/data/caddy"

WEBSITE_DIR="/data/website"
CADDY_WEBSITE_DIR="/usr/share/caddy"

CONFIG_DIR="${CADDY_BASE_DIR}/config"
CADDY_CONFIG_DIR="/config"

DATA_DIR="${CADDY_BASE_DIR}/data"
CADDY_DATA_DIR="/data"

WEBSITE_CONFIG_FILE="${CADDY_BASE_DIR}/caddyfile/Caddyfile"
CADDY_WEBSITE_CONFIG_FILE="/etc/caddy/Caddyfile"


docker run \
--name caddy \
--detach \
--restart=unless-stopped \
--publish 80:80 \
--publish 443:443 \
--volume "${CONFIG_DIR}":"${CADDY_CONFIG_DIR}" \
--volume "${DATA_DIR}":"${CADDY_DATA_DIR}" \
--volume "${WEBSITE_CONFIG_FILE}":"${CADDY_WEBSITE_CONFIG_FILE}" \
--volume "${WEBSITE_DIR}":"${CADDY_WEBSITE_DIR}" \
caddy:2 caddy run --config "${CADDY_WEBSITE_CONFIG_FILE}"
