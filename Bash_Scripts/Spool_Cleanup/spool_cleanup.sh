#!/bin/bash
set -e

# The folders in the spool directory must be a number. This number indicates
# the age in days a file must be before it is deleted.
SPOOL_DIR="/spool"

for dir_name in "${SPOOL_DIR}"/*
do
    dir_age=$(basename "${dir_name}")
    # find "${dir_name}" -type f -mtime +"${dir_age}" -print -delete
    find "${dir_name}" -type f -mtime +"${dir_age}" -print
done
