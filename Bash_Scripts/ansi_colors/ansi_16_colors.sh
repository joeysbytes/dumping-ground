#!/bin/bash

ESC='\033'
CSI="${ESC}["
RESET="${CSI}0m"
line=""


function main() {
    # Setup
    clear
    echo -e "${RESET}"
    echo "ANSI 16 colors"
    echo ""
    # First 8 colors
    for i in {0..7}
    do
        local color_value=$(("${i}" + 40))
        show_color "${i}" "${color_value}"
    done
    # Next 8 colors
    for i in {8..15}
    do
        local color_value=$(("${i}" - 8 + 100))
        show_color "${i}" "${color_value}"
    done
    # Finalize
    echo ""
    echo -e "${RESET}"
}


function show_color() {
    local color_index="${1}"
    local color_value="${2}"
    line="${RESET}"
    append "Color "
    if [ "${color_index}" -lt 10 ]
    then
        append " "
    fi
    append "${color_index}: "
    append "${CSI}37;${color_value}m"
    append "          "
    # Set back to default color
    line="${line}${CSI}0m"
    echo -e "${line}"
}


function append() {
    local text="${1}"
    line="${line}${text}"
}


main