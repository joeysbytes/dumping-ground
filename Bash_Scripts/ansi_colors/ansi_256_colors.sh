#!/bin/bash

ESC='\033'
CSI="${ESC}["
RESET="${CSI}0m"
line=""


function main() {
    # Setup
    clear
    echo -e "${RESET}"
    echo "ANSI 256 colors"
    echo ""
    # First 16 colors
    echo "Colors 0 - 15:"
    echo ""
    print_color_line 0 7
    print_color_line 8 15
    # Colors 16 - 231
    echo ""
    echo "6 x 6 x 6 Color Cube:"
    echo ""
    print_color_line 16 33
    print_color_line 52 69
    print_color_line 88 105
    print_color_line 124 141
    print_color_line 160 177
    print_color_line 196 213
    echo ""
    print_color_line 34 51
    print_color_line 70 87
    print_color_line 106 123
    print_color_line 142 159
    print_color_line 178 195
    print_color_line 214 231
    echo ""
    echo "24 Grayscale Colors:"
    echo ""
    print_color_line 232 243
    print_color_line 244 255
    # Finalize
    echo ""
    echo -e "${RESET}"
}


function print_color_line() {
    local min_idx="${1}"
    local max_idx="${2}"
    line="${RESET}"
    for color in $(seq "${min_idx}" "${max_idx}")
    do
        append_text_color "${color}"
        append "${CSI}48;5;${color}m"
        append " "
        append_num "${color}"
        append " "
    done
    append "${RESET}"
    echo -e "${line}"
}


function append_text_color() {
    local color_idx="${1}"
    local text_color="0"
    if [ "${color_idx}" -eq 0 ] || \
       [ "${color_idx}" -eq 4 ] || \
       (( "${color_idx}" >= 16 && "${color_idx}" <= 21 )) || \
       (( "${color_idx}" >= 52 && "${color_idx}" <= 57 )) || \
       (( "${color_idx}" >= 88 && "${color_idx}" <= 93 )) || \
       (( "${color_idx}" >= 232 && "${color_idx}" <=  240 ))
    then
        text_color="7"
    fi
    append "${CSI}38;5;${text_color}m"
}


function append_num() {
    local num="${1}"
    if [ "${num}" -lt 10 ]
    then
        append "  ${num}"
    elif [ "${num}" -lt 100 ]
    then
        append " ${num}"
    else
        append "${num}"
    fi
}

function append() {
    local text="${1}"
    line="${line}${text}"
}


main