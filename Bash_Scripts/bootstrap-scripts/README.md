# Bootstrap Scripts

These scripts should perform the minimal tasks required to get a new computer set up to be fully configured with another tool, such as Ansible.

## Scripts

| Script | Description |
| --- | --- |
| bootstrap.sh | The bootstrap kickoff script, requires a parameter of the computer to bootstrap<br />__Note:__ The sysinfo and utils module is automatically loaded for you. |
| go/go.sh | Download to any directory on the computer that is being bootstrapped. This script is used when developing / testing.  It will always download the latest code from source control and then execute bootstrap.sh |

## Directories

| Directory | Description |
| --- | --- |
| computers | Computer scripts that will be started by bootstrap.sh |
| go | Holds go.sh separately from rest of scripts. |
| modules | Functions available to load and execute by the scripts. |

## Modules and Functions

| Module | Function | Paramameters | Description |
| --- | --- | --- | --- |
| docker | install_docker | n/a | Installs and activates Docker. |
| swap | add_swap_file | 1) Swap file name<br />2) Swap file size in MB | Creates, mounts, and edits fstab for new swap file. |
| sysinfo | get_architecture | n/a | Sets ARCHITECTURE to processor type. |
| sysinfo | get_os_info | n/a | Sets OS_MAIN, OS_FLAVOR, and OS_VERSION. |
| updates | update_system | n/a | Prompts user if they want to perform a system update, and executes accordingly. If the system was updated, the user will be prompted if they want a reboot. |
| utils | executable_exists | 1) Executable name | Performs a 'which' for the executable, sets TRUE_FALSE |
| utils | yes_no | 1) Prompt text | Prompts user with yes/no question, sets YES_NO |
