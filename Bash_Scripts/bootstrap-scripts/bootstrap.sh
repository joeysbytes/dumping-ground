#!/usr/bin/env bash
set -e
set -u

###########################################################################
# The purpose of this script is to set up a machine just enough so that a
# configuration utility (such as Ansible) can be used.
###########################################################################

###########################################################################
# Global Constants
###########################################################################
# This is the name of the bootstrap file when downloaded from git
BOOTSTRAP_PROJECT="bootstrap-scripts-main"
BOOTSTRAP_PROJECT_TAR_FILE="${BOOTSTRAP_PROJECT}.tar"
BOOTSTRAP_PROJECT_GZIP_FILE="${BOOTSTRAP_PROJECT_TAR_FILE}.gz"
BOOTSTRAP_PROJECT_BASE_URL="https://gitlab.com/joeysbytes/bootstrap-scripts/-/archive/main"
BOOTSTRAP_PROJECT_URL="${BOOTSTRAP_PROJECT_BASE_URL}/${BOOTSTRAP_PROJECT_GZIP_FILE}"
# Where to download / extract the bootstrap scripts
TMP_DIR="/tmp/bootstrap"


###########################################################################
# Main Processing Logic
###########################################################################
function main() {
    heading
    # heading "Bootstrap Script"
    # must_be_root
    # validate_arguments "$@"
}


###########################################################################
# Pass in all arguments here for validation
###########################################################################
# function validate_arguments() {
#     echo "TASK: Validating arguments"

#     # Do we have the correct number of arguments
#     if [ $# -ne 1 ]
#     then
#         show_usage
#         exit 1
#     fi

#     # Is computerName valid
#     COMPUTER_SCRIPT="${COMPUTERS_DIR}/${1}.sh"
#     if [ ! -f "${COMPUTER_SCRIPT}" ]
#     then
#         echo "ERROR: Computer bootstrap script not found:"
#         echo "       ${COMPUTER_SCRIPT}"
#         exit 1
#     fi
# }


###########################################################################
# Headings
###########################################################################

function heading() {
    #----------------------------------------------------------------------
    # Given a text line, print it in a "banner"
    #
    # Parameters:
    #   1: Heading text
    #----------------------------------------------------------------------
    if [ $# -ne 1 ]
    then
        echo "ERROR: heading_text not provided"
        echo "Function Usage: heading heading_text"
        exit 1
    fi
    local heading_text
    heading_text="${1}"
    local heading_text_length
    heading_text_length="${#heading_text}"
    local heading_line_length
    heading_line_length=$((heading_text_length+6))
    local heading_line
    heading_line=""
    for((i=0; i<"${heading_line_length}"; i++))
    do
        heading_line="${heading_line}="
    done
    echo ""
    echo "${heading_line}"
    echo "   ${heading_text}   "
    echo "${heading_line}"
}


###########################################################################
# Logging functions
###########################################################################
# Log an error
function logerr() {
    if [ $# -lt 1 ]
    then
        echo "ERROR: log error text not provided"
        echo "Function Usage: heading heading_text"
        exit 1
    fi

}


# Log a task heading

function log_task() {
    echo ""
}


###########################################################################
# Validations
###########################################################################

function validate_number_of_arguments() {

}


function must_be_root() {
    #----------------------------------------------------------------------
    # Validate we are running as a root user
    #----------------------------------------------------------------------
    local user
    user=$(whoami)
    if [ ! "${user}" == "root" ]
    then
        echo "ERROR: Must run as root user."
        exit 1
    fi
}


###########################################################################
# Begin the script
###########################################################################
main "$@"
