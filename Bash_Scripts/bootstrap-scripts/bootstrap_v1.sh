#!/usr/bin/env bash
set -e
set -u

###########################################################################
# Initialization and kick-off script for the bootstrap process.
#
# Parameters:
#   1 - computerName: The name of the script in the computers sub-directory
#                     (without the .sh)
###########################################################################


###########################################################################
# Global Constants
# If a constant is empty, it will be populated by the scripts down-stream.
###########################################################################
COMPUTERS_DIR="./computers"
MODULES_DIR="./modules"
COMPUTER_SCRIPT=""
ARCHITECTURE=""
OS_MAIN=""
OS_FLAVOR=""
OS_VERSION=""
YES_NO=""
TRUE_FALSE=""


###########################################################################
# Main Processing Logic
###########################################################################
function main() {
    validate_arguments "$@"
    initialize
    echo ""
    source "${COMPUTER_SCRIPT}"
    echo ""
    echo "Bootstrap Script Ended Successfully"
}


###########################################################################
# Initialization Steps
###########################################################################
function initialize() {
    source "${MODULES_DIR}/utils.sh"
    source "${MODULES_DIR}/sysinfo.sh"
    get_architecture
    get_os_info
}





###########################################################################
# Show the proper usage of this script
###########################################################################
function show_usage() {
    echo ""
    echo "USAGE: bootstrap.sh computerName"
    echo ""
    echo "Parameters:"
    echo "  computerName: name of script (without .sh)"
    echo "                in computers directory"
    echo ""
}





###########################################################################
# Begin
###########################################################################
main "$@"
