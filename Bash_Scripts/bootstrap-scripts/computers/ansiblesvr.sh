###########################################################################
# Bootstrap ansiblesvr
###########################################################################
function ansiblesvr_main() {
    echo "Bootstraping: ansiblesvr"
    source "${MODULES_DIR}/updates.sh"
    update_system
    source "${MODULES_DIR}/docker.sh"
    install_docker
}


###########################################################################
# Begin
###########################################################################
ansiblesvr_main
