#!/usr/bin/env bash
set -e
set -u

###########################################################################
# The purpose of this script is to download the latest bootstrap code,
# and execute it.
###########################################################################

BOOTSTRAP_PROJECT="bootstrap-scripts-main"
BOOTSTRAP_PROJECT_TAR_FILE="${BOOTSTRAP_PROJECT}.tar"
BOOTSTRAP_PROJECT_GZIP_FILE="${BOOTSTRAP_PROJECT_TAR_FILE}.gz"
BOOTSTRAP_PROJECT_BASE_URL="https://gitlab.com/joeysbytes/bootstrap-scripts/-/archive/main"
BOOTSTRAP_PROJECT_URL="${BOOTSTRAP_PROJECT_BASE_URL}/${BOOTSTRAP_PROJECT_GZIP_FILE}"
TMP_DIR="/tmp/bootstrap"
COMPUTER_TO_BOOTSTRAP="ansiblesvr"


function main() {
    download_bootstrap_code
    run_bootstrap
}


function download_bootstrap_code() {
    echo "Getting latest version of bootstrap code"
    rm -rf "${TMP_DIR}"
    mkdir -p "${TMP_DIR}"
    curl --silent --show-error --output "${TMP_DIR}/${BOOTSTRAP_PROJECT_GZIP_FILE}" "${BOOTSTRAP_PROJECT_URL}"
    cd "${TMP_DIR}"
    tar -xf "./${BOOTSTRAP_PROJECT_GZIP_FILE}"
}


function run_bootstrap() {
    echo "Running bootstrap process for: ${COMPUTER_TO_BOOTSTRAP}"
    echo ""
    echo ""
    cd "${TMP_DIR}/${BOOTSTRAP_PROJECT}"
    chmod +x ./bootstrap.sh
    ./bootstrap.sh "${COMPUTER_TO_BOOTSTRAP}"
}

main
