###########################################################################
# Install Docker per the official instructions.
# Instructions: https://docs.docker.com/engine/install/
###########################################################################
function install_docker() {
    executable_exists "docker"
    if [ "${TRUE_FALSE}" == "T" ]
    then
        echo "Docker already installed"
    else
        echo "Installing Docker"
        if [ "${OS_MAIN}" == "RHEL" ]
        then
            if [ "${OS_FLAVOR}" == "AlmaLinux" ]
            then
                _install_docker_centos
            fi
        elif [ "${OS_MAIN}" == "DEBIAN" ]
        then
            if [ "${OS_FLAVOR}" == "Debian" ]
            then
                _install_docker_debian
            fi
        fi
    fi
}


###########################################################################
# Install Docker on a CentOS-like operating system.
# Instructions: https://docs.docker.com/engine/install/centos/
# GPG Key Fingerprint: 060A 61C5 1B55 8A7F 742B 77AA C52F EB6B 621E 9F35
###########################################################################
function _install_docker_centos() {
    echo "  Docker install type: CentOS"
    echo "  Installing pre-requisites..."
    yum install -y yum-utils
    echo "  Adding Docker repository..."
    yum-config-manager --add-repo "https://download.docker.com/linux/centos/docker-ce.repo"
    echo "  Installing Docker..."
    yum install docker-ce docker-ce-cli containerd.io
    echo "  Starting Docker..."
    systemctl start docker
    echo "  Enabling Docker..."
    systemctl enable docker.service
    systemctl enable containerd.service
}


###########################################################################
# Install Docker on a Debian-like operating system.
# Instructions: https://docs.docker.com/engine/install/debian/
###########################################################################
function _install_docker_debian() {
    echo "  Docker install type: Debian"
    echo "  Installing pre-requisites..."
    apt install ca-certificates curl gnupg lsb-release
    echo "  Adding Docker repository..."
    curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
    echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null
    echo "  Installing Docker..."
    apt update
    apt install docker-ce docker-ce-cli containerd.io
}
