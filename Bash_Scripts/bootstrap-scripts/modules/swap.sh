###########################################################################
# Create a swap file, activate it, and add it to fstab
# File name must be an absolute path.
#
# Parameters:
#   1 - File name (absolute path)
#   2 - Size in MB
#
# Guide used: https://linuxize.com/post/create-a-linux-swap-file/
###########################################################################
function add_swap_file() {
    # Check for correct usage
    if [ $# -ne 2 ]
    then
        echo "USAGE: add_swap_file fileName sizeInMB"
        exit 1
    fi

    local swap_file_name="${1}"
    local swap_file_size="${2}"

    # Check if swapfile already exists
    if [ ! -f "${swap_file_name}" ]
    then
        echo "Creating swap file:"
        echo "  Stats:"
        echo "    Name: ${swap_file_name}"
        echo "    Size: ${swap_file_size} MiB"

        echo "  Allocating swap file..."
        fallocate -l "${swap_file_size}MiB" "${swap_file_name}"
        
        echo "  Setting swap file permissions..."
        chmod 600 "${swap_file_name}"
        
        echo "  Converting file to swap file..."
        mkswap "${swap_file_name}"
        
        echo "  Activate swap file..."
        swapon "${swap_file_name}"

        echo "  Add swap file to fstab..."
        local swap_fstab_line="${swap_file_name} swap swap defaults,noatime 0 0"
        echo -e "\n${swap_fstab_line}\n">>/etc/fstab

    else
        echo "Swap file already exists:"
        echo "  ${swap_file_name}"
    fi
}
