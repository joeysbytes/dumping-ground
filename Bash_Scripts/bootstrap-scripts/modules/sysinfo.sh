###########################################################################
# Determine the computer architecure type (processor)
###########################################################################
function get_architecture() {
    ARCHITECTURE=$(uname --processor)
    if [ "${ARCHITECTURE}" == "unknown" ]
    then
        ARCHITECTURE=$(uname --machine)
    fi
    echo "Architecture: ${ARCHITECTURE}"
}


###########################################################################
# Determine the operating system being used
###########################################################################
function get_os_info() {
    if [ -f "/etc/os-release" ]
    then
        OS_FLAVOR=$(cat "/etc/os-release"|egrep "^NAME="|cut -f2 -d\")
        OS_VERSION=$(cat "/etc/os-release"|egrep "^VERSION_ID="|cut -f2 -d\")
    fi
    if [ "${OS_FLAVOR}" == "AlmaLinux" ]
    then
        OS_MAIN="RHEL"
    elif [ "${OS_FLAVOR}" == "Debian GNU/Linux" ]
    then
        OS_MAIN="DEBIAN"
        OS_FLAVOR="Debian"
    fi
    echo "OS:         ${OS_MAIN}"
    echo "OS Flavor:  ${OS_FLAVOR}"
    echo "OS Version: ${OS_VERSION}"
}
