###########################################################################
# Update the entire system, if the user agrees to it.
###########################################################################
function update_system() {
    yes_no "Do you wish to perform a system update?"
    if [ "${YES_NO}" == "Y" ]
    then
        _update_system
        yes_no "Do you need to reboot?"
        if [ "${YES_NO}" == "Y" ]
        then
            reboot
        fi
    fi
}


###########################################################################
# Determine how to perform a system update.
###########################################################################
function _update_system() {
    executable_exists "dnf"
    if [ "${TRUE_FALSE}" == "T" ]
    then
        _update_system_dnf
    else
        executable_exists "apt"
        if [ "${TRUE_FALSE}" == "T" ]
        then
            _update_system_apt
        fi
    fi
}


###########################################################################
# Update the system with dnf.
###########################################################################
function _update_system_dnf() {
    echo "Updating system with dnf"
    set +e
    dnf upgrade --refresh
    set -e
}


###########################################################################
# Update the system with apt.
###########################################################################
function _update_system_apt() {
    echo "Updating system with apt"
    set +e
    apt update
    apt upgrade
    set -e
}
