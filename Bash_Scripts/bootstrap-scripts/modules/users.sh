###########################################################################
# Add a given group name to a user.
#
# Parameters:
#  1 - group name
#  2 - user name to add group to
###########################################################################
function add_group_to_user() {
    # Check for correct usage
    if [ $# -ne 2 ]
    then
        echo "USAGE: add_group_to_user group_name user_name"
        exit 1
    fi
    local group_name="${1}"
    local user_name="${2}"
    # Check group exists
    # Check user exists
    # Check user already a group member
}
