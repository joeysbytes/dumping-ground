###########################################################################
# Given an executable name, set TRUE_FALSE if this is found on the path.
#
# Parameters:
#   1 - The executable to search the path for
###########################################################################
function executable_exists() {
    # Check for correct usage
    if [ $# -ne 1 ]
    then
        echo "USAGE: executable_exists executableName"
        exit 1
    fi

    # Search for executable
    set +e
    which "${1}">/dev/null 2>/dev/null
    local which_status=$?
    set -e
    if [ "${which_status}" -eq 0 ]
    then
        TRUE_FALSE="T"
    else
        TRUE_FALSE="F"
    fi
}


###########################################################################
# This function will prompt the user with a yes/no question
# and set a global variable YES_NO to either "Y" or "N".
#
# Parameters:
#   1 - The user prompt text
###########################################################################
function yes_no() {
    # Check for correct usage
    if [ $# -ne 1 ]
    then
        echo "USAGE: yes_no promptText"
        exit 1
    fi

    echo "${1}"
    select choice in "Yes" "No"
    do
        case "${choice}" in
            "Yes" ) YES_NO="Y"; break;;
            "No" ) YES_NO="N"; break;;
        esac
    done
}
