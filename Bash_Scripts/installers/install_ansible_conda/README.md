# Install Ansible

This script installs an Ansible server.  It is useful for freshly installed machines when you do not have another Ansible server for setting Ansible up.

This install will also work on portable medium, such as a USB flash drive.

The tasks performed are:

* Build / Verify the development and download directories
* Install git
* Install MiniConda (Python 3 64-bit version)
* Create an environment with Python 3, and install Ansible in to it
* Clone the git repository of an Ansible project

The script can be completely controlled from the global variables at the top.
