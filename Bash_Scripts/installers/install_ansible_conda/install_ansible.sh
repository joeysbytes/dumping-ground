#!/bin/bash
#==========================================================================
# Install Ansible, assuming a freshly-installed machine
# This installation is portable, and will work on removable media.
#==========================================================================

#--------------------------------------------------------------------------
# stop on errors
#--------------------------------------------------------------------------
set -e


###########################################################################
# User Global Variables
###########################################################################
# Where to put development files
DEVELOPMENT_DIR="/home/joey/JoeyUSB/development"
# Where to put downloaded files
DOWNLOAD_DIR="/home/$USER/Downloads"
# Where to install MiniConda
MINICONDA_INSTALL_PATH="/media/joey/JoeyUSB/bin/miniconda3"
# Name of Ansible environment to create
CONDA_ANSIBLE_ENVIRONMENT="ansible-env"
# The Git Ansible project name
GIT_ANSIBLE_PROJECT_NAME="ansible-files"
# The Git url of the Ansible project
GIT_ANSIBLE_PROJECT_URL="git@gitlab.com:joeysbytes/${GIT_ANSIBLE_PROJECT_NAME}.git"


###########################################################################
# Constant Global Variables
# You should not need to change these values
###########################################################################
# URL to download MiniConda
MINICONDA_DOWNLOAD_URL="https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh"
# Name of MiniConda installer when it is downloaded
MINICONDA_INSTALLER="${DOWNLOAD_DIR}/miniconda3.sh"
# Conda bin path
CONDA_BIN_PATH="${MINICONDA_INSTALL_PATH}/bin"
# Conda executable
CONDA_EXECUTABLE="${CONDA_BIN_PATH}/conda"
# Python version to install for Ansible (pip version)
PYTHON_VERSION="3.7"
# Folder for Ansible project
GIT_ANSIBLE_PROJECT_DIR="${DEVELOPMENT_DIR}/${GIT_ANSIBLE_PROJECT_NAME}"


###########################################################################
# Main
###########################################################################
function main {
    log "Start: Ansible Installation"
    check_user
    build_directory "${DEVELOPMENT_DIR}"
    build_directory "${DOWNLOAD_DIR}"
    install_apt_utils
    install_sshpass
    install_git
    get_miniconda_installer
    install_miniconda
    install_ansible
    install_ansible_project
    delete_miniconda_installer
    log "End: Ansible Installation"
}


###########################################################################
# Install Ansible Project (from git)
###########################################################################
function install_ansible_project {
    if [ ! -e "${GIT_ANSIBLE_PROJECT_DIR}" ]
    then
        log "Cloning Ansible project: ${GIT_ANSIBLE_PROJECT_URL}"
        log "Ansible project location: ${GIT_ANSIBLE_PROJECT_DIR}"
        cd "${DEVELOPMENT_DIR}"
        git clone "${GIT_ANSIBLE_PROJECT_URL}"
    else
        log "Ansible project already cloned: ${GIT_ANSIBLE_PROJECT_DIR}"
    fi
}


###########################################################################
# Install Ansible
###########################################################################
function install_ansible {
    local environment_found=`"${CONDA_EXECUTABLE}" env list|cut -f1 -d\ |grep "${CONDA_ANSIBLE_ENVIRONMENT}"|wc -l`
    if [ "${environment_found}" == "0" ]
    then
        log "Creating Conda Ansible environment: ${CONDA_ANSIBLE_ENVIRONMENT}"
        "${CONDA_EXECUTABLE}" create --yes --name "${CONDA_ANSIBLE_ENVIRONMENT}" python="${PYTHON_VERSION}"
        log "Activating Ansible environment"
        source "${CONDA_BIN_PATH}/activate" "${CONDA_ANSIBLE_ENVIRONMENT}"
        log "Upgrading pip"
        pip install --upgrade pip
        log "Installing Ansible"
        pip install ansible
        log "Deactivating Ansible environment"
        source "${CONDA_BIN_PATH}/deactivate"
    else
        log "Conda Ansible environment already exists: ${CONDA_ANSIBLE_ENVIRONMENT}"
    fi
}


###########################################################################
# Install MiniConda
###########################################################################
function install_miniconda {
    if [ ! -e "${MINICONDA_INSTALL_PATH}" ]
    then
        log "Installing MiniConda to ${MINICONDA_INSTALL_PATH}"
        "${MINICONDA_INSTALLER}" -b -p "${MINICONDA_INSTALL_PATH}"
    else
        log "MiniConda already installed to ${MINICONDA_INSTALL_PATH}"
    fi
}


###########################################################################
# Download MiniConda Installer
###########################################################################
function get_miniconda_installer {
    if [ ! -e "${MINICONDA_INSTALLER}" ]
    then
        log "Downloading MiniConda 3 64-bit to ${MINICONDA_INSTALLER}"
        curl --output "${MINICONDA_INSTALLER}" --url "${MINICONDA_DOWNLOAD_URL}"
    else
        log "MiniConda 3 64-bit already downloaded: ${MINICONDA_INSTALLER}"
    fi
    local permissions=`ls -ld "${MINICONDA_INSTALLER}"|cut -f1 -d" "`
    if [ ! -x "${MINICONDA_INSTALLER}" ]
    then
        log "Making MiniConda installer executable: ${MINICONDA_INSTALLER}"
        chmod +x "${MINICONDA_INSTALLER}"
    fi
}


###########################################################################
# Delete MiniConda Installer
###########################################################################
function delete_miniconda_installer {
    if [ -e "${MINICONDA_INSTALLER}" ]
    then
        log "Deleting MiniConda installer: ${MINICONDA_INSTALLER}"
        rm "${MINICONDA_INSTALLER}"
    else
        log "MiniConda installer already deleted: ${MINICONDA_INSTALLER}"
    fi
}


###########################################################################
# Install apt-utils
###########################################################################
function install_apt_utils {
    local apt_utils_found=`sudo dpkg-query --list apt-utils|grep "no packages found matching"|wc -l`
    if [ "${apt_utils_found}" == "0" ]
    then
        log "apt-utils already installed"
    else
        log "Installing apt-utils"
        sudo apt install apt-utils
    fi
}


###########################################################################
# Install sshpass
###########################################################################
function install_sshpass {
    local sshpass_found=`which sshpass|wc -l`
    if [ "${sshpass_found}" == "0" ]
    then
        log "Installing sshpass"
        sudo apt install sshpass
    else
        log "sshpass already installed"
    fi
}


###########################################################################
# Install git
###########################################################################
function install_git {
    local git_found=`which git|wc -l`
    if [ "${git_found}" == "0" ]
    then
        log "Installing git"
        sudo apt install git
    else
        log "git already installed"
    fi
}


###########################################################################
# Build a directory
#   Parameter 1: Directory to create
###########################################################################
function build_directory {
    local directory_to_build="${1}"
    if [ ! -e "${directory_to_build}" ]
    then
        log "Creating directory: ${directory_to_build}"
        mkdir -p "${directory_to_build}"
    fi
    if [ ! -d "${directory_to_build}" ]
    then
        log "ERROR: Not a directory: ${directory_to_build}"
        exit 1
    fi
    log "Directory ready: ${directory_to_build}"
}


###########################################################################
# Make sure we are not running as root
###########################################################################
function check_user {
    local the_user=`echo "${USER}"`
    if [ "${the_user}" == "root" ]
    then
        echo "DO NOT RUN THIS SCRIPT AS ROOT."
        echo "You will have to provide sudo access for some"
        echo "commands to execute, but Ansible will be"
        echo "installed for the current user running this script."
        exit 1
    else
        log "Running for user: ${USER}"
    fi
}


###########################################################################
# Logging routine
###########################################################################
function log {
    local date_timestamp=`date "+%Y%m%d_%H%M%S"`
    echo "${date_timestamp}: ${1}"
}


###########################################################################
# Begin script
###########################################################################
main
