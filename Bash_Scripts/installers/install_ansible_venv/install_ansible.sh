#!/bin/bash
#==========================================================================
# Install Ansible using the system-installed Python environment.
#==========================================================================

#--------------------------------------------------------------------------
# stop on errors
#--------------------------------------------------------------------------
set -e


###########################################################################
# User Global Variables
###########################################################################
# Where the Ansible environment will be built
BASE_DIR="/work"
# Name of the Ansible environment to create
ANSIBLE_ENV="ansible-env"


###########################################################################
# Constant Global Variables
# You should not need to change these values
###########################################################################
# Folder for Ansible binaries
ANSIBLE_INSTALL_DIR="${BASE_DIR}/${ANSIBLE_ENV}"


###########################################################################
# Main
###########################################################################
function main {
    log "Start: Ansible Installation"
    check_user
    install_apt_utils
    install_sshpass
    install_python3_venv
    install_ansible
    log "End: Ansible Installation"
}


###########################################################################
# Check if Ansible needs installed
###########################################################################
function install_ansible {
    log "Ansible installation directory: ${ANSIBLE_INSTALL_DIR}"
    if [ -d "${ANSIBLE_INSTALL_DIR}" ]
    then
        log "Ansible installation directory found"
        local prompt="Do you wish to re-install Ansible? (Y/N): "
        read -r -p "${prompt}" response
        case "${response}" in
            [Yy])
                _install_ansible
                ;;
            *)
                log "Not re-installing Ansible"
                ;;
        esac
    else
        log "Ansible not found"
        _install_ansible
    fi
}


###########################################################################
# Install Ansible
###########################################################################
function _install_ansible {
    log "Installing Ansible Environment"
    python3 -m venv --symlinks --clear "${ANSIBLE_INSTALL_DIR}"
    log "Activating Ansible Environment"
    source "${ANSIBLE_INSTALL_DIR}/bin/activate"
    log "Updating pip"
    pip install -U pip
    log "Installing Ansible"
    pip install ansible
    log "Deactivating Ansible Environment"
}


###########################################################################
# Install apt-utils
###########################################################################
function install_apt_utils {
    local apt_utils_found=`sudo dpkg-query --list apt-utils|grep "no packages found matching"|wc -l`
    if [ "${apt_utils_found}" == "0" ]
    then
        log "apt-utils already installed"
    else
        log "Installing apt-utils"
        sudo apt install apt-utils
    fi
}


###########################################################################
# Install sshpass
###########################################################################
function install_sshpass {
    local sshpass_found=`which sshpass|wc -l`
    if [ "${sshpass_found}" == "0" ]
    then
        log "Installing sshpass"
        sudo apt install sshpass
    else
        log "sshpass already installed"
    fi
}


###########################################################################
# Install Python 3 venv
###########################################################################
function install_python3_venv {
    local python3_venv_found=`sudo dpkg-query --list python3-venv|grep "no packages found matching"|wc -l`
    if [ "${python3_venv_found}" == "0" ]
    then
        log "python3-venv already installed"
    else
        log "Installing python3-venv"
        sudo apt install python3-venv
    fi
}


###########################################################################
# Make sure we are not running as root
###########################################################################
function check_user {
    local the_user=`echo "${USER}"`
    if [ "${the_user}" == "root" ]
    then
        echo "DO NOT RUN THIS SCRIPT AS ROOT."
        echo "You will have to provide sudo access for some"
        echo "commands to execute, but Ansible will be"
        echo "installed for the current user running this script."
        exit 1
    else
        log "Running for user: ${USER}"
    fi
}


###########################################################################
# Logging routine
###########################################################################
function log {
    local date_timestamp=`date "+%Y%m%d_%H%M%S"`
    echo "${date_timestamp}: ${1}"
}


###########################################################################
# Begin script
###########################################################################
main
