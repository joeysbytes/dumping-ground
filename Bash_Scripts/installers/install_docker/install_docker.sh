#!/bin/bash

###########################################################################
# This script will perform the following:
#   * install docker
#   * install docker-compose
#   * add the docker group
#   * add current user to the docker group
#   * enable docker
#   * start docker
###########################################################################


#==========================================================================
# Global Variables
#==========================================================================
DISTRO_TYPE="unknown"


#==========================================================================
# Main Processing Logic
#==========================================================================
function main() {
    log "BEGIN: Install Docker"
    check_if_root
    detect_distro_type
    install_docker_${DISTRO_TYPE}
    log "END: Install Docker"
}


#--------------------------------------------------------------------------
# Do not run this script if you are root or running as sudo
#--------------------------------------------------------------------------
function check_if_root() {
    log "Check if running as root"
    if [ "$(whoami)" == "root" ]
    then
        log "ERROR: Must run as a regular user without privileges."
        log "       User will be prompted for sudo password if needed by this script."
        exit 1
    fi
}


#--------------------------------------------------------------------------
# Determine the type of distrobution we are running.
#--------------------------------------------------------------------------
function detect_distro_type() {
    log "Detect distribution type"
    if [ -e "/etc/arch-release" ]
    then
        DISTRO_TYPE="arch"
    fi
    if [ "${DISTRO_TYPE}" == "unknown" ]
    then
        log "ERROR: Unknown Distribution"
        exit 1
    fi
}


#--------------------------------------------------------------------------
# Install Docker on Arch linux
#--------------------------------------------------------------------------
function install_docker_arch() {
    log "Install Docker for Arch"
    install_docker_from_pacman
    add_docker_group
    add_user_to_docker_group
    enable_docker_systemd
    start_docker_systemd
}


#--------------------------------------------------------------------------
# Install Docker from pacman repositories
#--------------------------------------------------------------------------
function install_docker_from_pacman() {
    log "Install Docker and Docker-Compose from Pacman"
    sudo pacman -Syu docker docker-compose
}


#--------------------------------------------------------------------------
# Add docker group
#--------------------------------------------------------------------------
function add_docker_group() {
    log "Add 'docker' group"
    local group_exists=$(cat /etc/group|grep docker|cut -f1 -d:)
    if [ ! "${group_exists}" == "docker" ]
    then
        sudo groupadd docker
    else
        log "  'docker' group already exists"
    fi
}


#--------------------------------------------------------------------------
# Add the docker group to the user
#--------------------------------------------------------------------------
function add_user_to_docker_group() {
    log "Add 'docker' group to user ${USER}"
    local in_group=$(groups "${USER}"|grep docker|wc -l)
    if [ "${in_group}" == "0" ]
    then
        sudo usermod -aG docker "${USER}"
    else
        log "  User ${USER} already in group 'docker'"
    fi
}


#--------------------------------------------------------------------------
# Enable docker with systemd
#--------------------------------------------------------------------------
function enable_docker_systemd() {
    log "Enable docker with systemd"
    local is_enabled=$(sudo systemctl is-enabled docker)
    if [ ${is_enabled} == "disabled" ]
    then
        sudo systemctl enable docker
    else
        log "  Docker already enabled"
    fi
}


#--------------------------------------------------------------------------
# Start docker with systemd
#--------------------------------------------------------------------------
function start_docker_systemd() {
    log "Start docker with systemd"
    local is_active=$(sudo systemctl is-active docker)
    if [ ${is_active} == "inactive" ]
    then
        sudo systemctl start docker
    else
        log "  Docker alread started"
    fi
}


#--------------------------------------------------------------------------
# I like timestamps in front of my echos
#--------------------------------------------------------------------------
function log() {
    local datetimestamp=$(date +%Y%m%d_%H%M%S)
    echo "${datetimestamp}: ${1}"
}


main

