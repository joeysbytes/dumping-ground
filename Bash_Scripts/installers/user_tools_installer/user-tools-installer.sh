#!/bin/bash
set -e

# This script installs custom utilities on to the system.


###########################################################################
# Global Variables / Constants
###########################################################################
# Set to "true" if you want utilities reinstalled / updated.
#   Otherwise, only missing utilities will be installed.
FORCE_REINSTALLS="true"
BINARY_DIR="/usr/local/bin"
# These are used by the functions, no need to tamper with
_USE_SUDO="false"


###########################################################################
# Main Processing Logic
###########################################################################
function main() {
    cd "${BINARY_DIR}"
    install_as_command "key-value" \
        "https://gitlab.com/joeysbytes/key-value-cli/-/raw/master/key-value/key-value.py"
    install_as_command "fix-audio" \
        "https://gitlab.com/joeysbytes/scripts/-/raw/master/fix_audio_linux/fix-audio.sh"
    install_as_command "updme" \
        "https://gitlab.com/joeysbytes/scripts/-/raw/master/linux_updates/updates.sh"
    install_as_command "filesvr" \
        "https://gitlab.com/joeysbytes/scripts/-/raw/master/mount_fileserver/mount_fileserver_v2.sh"
    install_user_file "${HOME}/.bash_aliases" \
        "https://gitlab.com/joeysbytes/dotfiles/-/raw/master/bash/.bash_aliases"
    install_user_file "${HOME}/.nanorc" \
        "https://gitlab.com/joeysbytes/dotfiles/-/raw/master/nano/.nanorc"
    draw_separater
}


###########################################################################
# Given a single file, install it as a command.
#   1 - name of command to create
#   2 - file to download / install /update
###########################################################################
function install_as_command() {
    draw_separater
    _USE_SUDO="true"
    local command="${1}"
    local script_url="${2}"
    local script_ext="${script_url##*.}"
    local script="${command}.${script_ext}"
    local full_command="${BINARY_DIR}/${command}"
    local full_script="${BINARY_DIR}/${script}"
    echo "Install command: ${command}"
    echo "  Download from: ${script_url}"
    remove_file_if_forced "${full_command}"
    remove_file_if_forced "${full_script}"
    if [ ! -e "${full_script}" ]
    then
        download_file_if_missing "${full_script}" "${script_url}"
        # Create command
        echo "Creating command: ${full_command}"
        case "${script_ext}" in
        "py")
            local pycmd="python3 ${full_script} \$@"
            echo "${pycmd}" | sudo tee "${full_command}" > /dev/null
            sudo chmod +x "${full_command}"
            ;;
        "sh")
            sudo chmod +x "${full_script}"
            sudo ln -s "./${script}" "${full_command}"
            ;;
        *)
            echo "ERROR: Unhandled extension: ${script_ext}"
            exit 1
            ;;
        esac
    else
        echo "Script already exists: ${full_script}"
    fi
}


###########################################################################
# Download a file and save it
#   1 - File name to save as (full path)
#   2 - URL of file to download
###########################################################################
function install_user_file() {
    _USE_SUDO="false"
    draw_separater
    local file_name="${1}"
    local file_url="${2}"
    echo "Downloding to file: ${file_name}"
    echo "          From URL: ${file_url}"
    remove_file_if_forced "${file_name}"
    download_file_if_missing "${file_name}" "${file_url}"
}


###########################################################################
# Delete a file if the FORCE_REINSTALLS is true.
#   1 - File to delete (full path)
###########################################################################
function remove_file_if_forced() {
    local file_name="${1}"
    if [ "${FORCE_REINSTALLS}" == "true" ]
    then
        echo "Removing file: ${file_name}"
        if [ "${_USE_SUDO}" == "true" ]
        then
            sudo rm -f "${file_name}"
        else
            rm -f "${file_name}"
        fi
    fi
}


###########################################################################
# Download a file if it does not exist.
#   1 - File name to save as (full path)
#   2 - File url to download
###########################################################################
function download_file_if_missing() {
    local file_name="${1}"
    local file_url="${2}"
    if [ ! -e "${file_name}" ]
    then
        echo "Downloading file: ${file_name}"
        if [ "${_USE_SUDO}" == "true" ]
        then
            sudo curl --output "${file_name}" "${file_url}"
        else
            curl --output "${file_name}" "${file_url}"
        fi
    else
        echo "File already exists: ${file_name}"
    fi
}


###########################################################################
# Draw a line
###########################################################################
function draw_separater() {
    echo "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -"
}


main
