# Linux Updater Script

This script will perform the following actions:

* Update main system packages with the appropriate package manager
  * pkcon
  * apt
  * zypper
  * pacman
* Update specially installed software.  If they do not exist, no action is taken.
  * conda, conda environments
  * snap packages
  * flatpak packages
  * VirtualBox extensions, to make it match the currently installed VirtualBox
  * Visual Studio Code extensions
* Refresh dotfiles (always)
  * .bashrc
  * .nanorc
