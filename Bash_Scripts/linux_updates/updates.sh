# My Update Script

package_count="0"


function main {
    # Update main system packages
    package_exists zypper
    if [ ! "${package_count}" == "0" ]
    then
        update_zypper
    else
        package_exists pacman
        if [ ! "${package_count}" == "0" ]
        then
            update_pacman
        else
            package_exists pkcon
            if [ ! "${package_count}" == "0" ]
            then
                update_pkcon
            else
                package_exists apt
                if [ ! "${package_count}" == "0" ]
                then
                    update_apt
                fi
            fi
        fi
    fi

    # Update specific software packages
    package_exists snap; if [ ! "${package_count}" == "0" ]; then update_snap; fi
    package_exists flatpak; if [ ! "${package_count}" == "0" ]; then update_flatpak; fi
#    package_exists vboxmanage; if [ ! "${package_count}" == "0" ]; then update_virtualbox_extensions; fi
#    package_exists code; if [ ! "${package_count}" == "0" ]; then update_visual_studio_code_extensions; fi

    # Refresh dotfiles
    echo ""
#    refresh_dotfiles

    # Completed
    echo ""
    echo "Update process finished."
}


##############################################################################
# Update main packages with apt
##############################################################################
function update_apt {
    print_heading "Updating packages with apt"
    sudo apt update
    sudo apt upgrade
}


##############################################################################
# Update main packages with pkcon
##############################################################################
function update_pkcon {
    print_heading "Updating with pkcon"
    sudo pkcon refresh
    sudo pkcon update
}


##############################################################################
# Update main packages with zypper
##############################################################################
function update_zypper {
    print_heading "Updating with zypper"
    sudo zypper refresh
    sudo zypper update
}


##############################################################################
# Update main packages with pacman
##############################################################################
function update_pacman {
    print_heading "Updating with pacman"
    sudo pacman -Syu
}


##############################################################################
# Update any packages installed with snap
##############################################################################
function update_snap {
    print_heading "Updating snap packages"
    sudo snap refresh
}


##############################################################################
# Update any packages installed with flatpak
##############################################################################
function update_flatpak {
    print_heading "Updating flatpak packages"
    sudo flatpak update
}


##############################################################################
# Update the VirtualBox Extensions to match the version
# of the installed VirtualBox
##############################################################################
function update_virtualbox_extensions {
    print_heading "Updating VirtualBox extensions"
    local virtualbox_version=$(vboxmanage --version|cut -f1 -dr|cut -f1 -d_)
    local virtualbox_extpack_version=$(vboxmanage list extpacks|grep Version|cut -f2 -d:|tr -d [:space:])
    echo "VirtualBox Version:                ${virtualbox_version}"
    echo "VirtualBox Extension Pack Version: ${virtualbox_extpack_version}"
    if [ ! "${virtualbox_version}" == "${virtualbox_extpack_version}" ]
    then
        echo "Updating VirtualBox Extension Pack"
        local virtualbox_base_url="https://download.virtualbox.org/virtualbox/${virtualbox_version}"
        local virtualbox_sha256sums_file="/tmp/virtualbox_${virtualbox_version}.SHA256SUMS"
        local virtualbox_extpack_file="Oracle_VM_VirtualBox_Extension_Pack-${virtualbox_version}.vbox-extpack"
        local virtualbox_extpack_full_file="/tmp/${virtualbox_extpack_file}"
        if [ ! -e "${virtualbox_sha256sums_file}" ]
        then
            wget -O - "${virtualbox_base_url}/SHA256SUMS">"${virtualbox_sha256sums_file}"
        fi
        local virtualbox_extpack_sha256sum=$(cat "${virtualbox_sha256sums_file}"|grep "${virtualbox_extpack_file}"|cut -d' ' -f1)
        if [ ! -e "${virtualbox_extpack_full_file}" ]
        then
            wget -O - "${virtualbox_base_url}/${virtualbox_extpack_file}">"${virtualbox_extpack_full_file}"
        fi
        local virtualbox_extpack_file_sha256sum=$(sha256sum "${virtualbox_extpack_full_file}"|cut -d' ' -f1)
        if [ "${virtualbox_extpack_file_sha256sum}" == "${virtualbox_extpack_sha256sum}" ]
        then
            sudo echo y|vboxmanage extpack install --replace "${virtualbox_extpack_full_file}"
        else
            echo "ERROR: VirtualBox Extension Pack checksum did not equal"
            echo "  Expected: ${virtualbox_extpack_sha256sum}"
            echo "  Result:   ${virtualbox_extpack_file_sha256sum}"
        fi
    fi
}


##############################################################################
# Update all installed Visual Studio Code Extensions
##############################################################################
function update_visual_studio_code_extensions {
    print_heading "Updating Visual Studio Code Extensions"
    local vs_code_count=`which code|wc -l`
    if [ "${vs_code_count}" == "1" ]
    then
        local vs_code_extension_list=`code --list-extensions`
        while read -r vs_code_extension;
        do
            echo "Updating Visual Studio Code extension: ${vs_code_extension}"
            code --install-extension "${vs_code_extension}" --force
        done <<< "${vs_code_extension_list}"
    else
        echo "Visual Studio Code not found"
    fi
}


##############################################################################
# Refresh the dotfiles, in case they have been updated
##############################################################################
function refresh_dotfiles {
    print_heading "Refresh Dotfiles"
    wget -O - "https://gitlab.com/joeysbytes/dotfiles/raw/master/bash/.bash_aliases">"${HOME}/.bash_aliases"
    wget -O - "https://gitlab.com/joeysbytes/dotfiles/raw/master/nano/.nanorc">"${HOME}/.nanorc"
}

##############################################################################
# Using the which command combined with line count, check if a package exists
# Sets package_count to number of lines from which command
# Parameter 1: package name to check
##############################################################################
function package_exists {
    echo ""
    local package_name="${1}"
    package_count=$(which "${package_name}"|wc -l)
    if [ "${package_count}" == "0" ]
    then
        echo "Package does not exist: ${package_name}"
    else
        echo "Package exists: ${package_name}"
    fi
}


##############################################################################
# Given a text string, print a heading for the current task.
# Parameter 1: Text string to surround with heading
##############################################################################
function print_heading {
    local heading_text="${1}"
    local heading_length=$(("${#heading_text}" + 0))
    for i in $(seq 1 $heading_length); do echo -n "="; done; echo ""
    echo "${heading_text}"
    for i in $(seq 1 $heading_length); do echo -n "="; done; echo ""
}


main
