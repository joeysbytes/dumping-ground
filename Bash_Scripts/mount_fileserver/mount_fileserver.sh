#!/bin/bash

###########################################################################
# This script will mount the file server directories.
# NOTE: server needs to be in ~./ssh/known_hosts for sshfs
###########################################################################

#----------------
# sshfs Variables
#----------------
sshfs_password_file="/data/credentials/fileserver.password"
declare -a sshfs_mounts
# For each sshfs mount: server_mount local_mount sshfs_options
sshfs_mounts+=("pi@svr01:/mnt/data_3tb_mirror" "/mnt/file_server_1" "follow_symlinks,uid=1000,gid=1000,password_stdin,reconnect")
sshfs_mounts+=("pi@svr01:/mnt/data_1tb_mirror" "/mnt/file_server_2" "follow_symlinks,uid=1000,gid=1000,password_stdin,reconnect")


###########################################################################
# Main Processing Logic
###########################################################################
function main() {
    mount_all_sshfs
    pause_for_dramatic_effect
}


###########################################################################
# sshfs Mounting
###########################################################################

#-----------------------------------------
# This function loops through sshfs_mounts
#-----------------------------------------
function mount_all_sshfs() {
    local sshfs_mounts_count=${#sshfs_mounts[@]}
    if [[ ! "${sshfs_mounts_count}" == "0" ]]
    then
        install_sshfs
        check_for_sshfs_password_file
        for ((i=0; i<${sshfs_mounts_count}; i+=3));
        do
            mount_sshfs "${sshfs_mounts[i]}" "${sshfs_mounts[i+1]}" "${sshfs_mounts[i+2]}"
        done
    else
        echo "No sshfs mounts found"
    fi
}

#---------------------------
# This function mounts sshfs
# Parameters:
#   1: sshfs server mount
#   2: sshfs local mount
#   3: sshfs mount options
#---------------------------
function mount_sshfs() {
    local sshfs_server_mount="${1}"
    local sshfs_local_mount="${2}"
    local sshfs_mount_options="${3}"
    echo ""
    echo "sshfs Mount:"
    echo "  Server:  ${sshfs_server_mount}"
    echo "  Local:   ${sshfs_local_mount}"
    echo "  Options: ${sshfs_mount_options}"
    check_if_local_mnt_dir_exists "${sshfs_local_mount}"
    check_if_sshfs_already_mounted "${sshfs_server_mount}"
    if [[ $? == 0 ]]
    then
        echo "Mounting sshfs"
        cat "${sshfs_password_file}"|sshfs "${sshfs_server_mount}" "${sshfs_local_mount}" -o "${sshfs_mount_options}"
        check_if_sshfs_already_mounted "${sshfs_server_mount}"
        if [[ $? == 0 ]]
        then
            echo "ERROR: Failed to mount: ${sshfs_server_mount}"
            pause_for_dramatic_effect
            exit 1
        fi
    else
        echo "Not performing sshfs mount"
    fi
}

#--------------------------------------------------------------
# This function will check if the local mount directory exists.
# Parameters:
#   1: sshfs local mount point
#--------------------------------------------------------------
function check_if_local_mnt_dir_exists() {
    local sshfs_local_mount="${1}"
    if [ ! -d "${sshfs_local_mount}" ]
    then
        echo "ERROR: Local mount point does not exist: ${sshfs_local_mount}"
        pause_for_dramatic_effect
        exit 1
    else
        echo "Local mount point exists: ${sshfs_local_mount}"
    fi
}

#------------------------------------------------------------------
# This function checks if the requested sshfs mount already exists.
# Return values: 0=not mounted  1=mounted
# Parameters:
#   1: sshfs server mount
#------------------------------------------------------------------
function check_if_sshfs_already_mounted() {
    local sshfs_server_mount="${1}"
    echo "Checking if mount point already exists: ${sshfs_server_mount}"
    local found_sshfs_mount=$(mount -l -t fuse.sshfs|grep "${sshfs_server_mount} on ")
    if [[ "${found_sshfs_mount}" == "" ]]
    then
        echo "Mount point not found"
        return 0
    else
        echo "Mount point found: ${found_sshfs_mount}"
        return 1
    fi
}

#----------------------------------------------------------
# If sshfs is not installed, this function will install it.
#----------------------------------------------------------
function install_sshfs() {
    local sshfs_found=$(which sshfs)
    if [[ sshfs_found == "" ]]
    then
        echo "sshfs not found, installing"
        sudo apt install sshfs
    else
        echo "sshfs found"
    fi
}

#-----------------------------------------------------------------
# This function verifies the password file exists and is readable.
#-----------------------------------------------------------------
function check_for_sshfs_password_file() {
    if [ ! -f "${sshfs_password_file}" ]
    then
        echo "ERROR: sshfs password file not found: ${sshfs_password_file}"
        pause_for_dramatic_effect
        exit 1
    elif [ ! -r "${sshfs_password_file}" ]
    then
        echo "ERROR: sshfs password file not readable: ${sshfs_password_file}"
        pause_for_dramatic_effect
        exit 1
    else
        echo "sshfs password file found: ${sshfs_password_file}"
    fi
}


###########################################################################
# This function is here for the user to review the output.
###########################################################################
function pause_for_dramatic_effect() {
    echo ""
    read -rsp $'Press any key to continue...\n\n' -n1 key
}


main
