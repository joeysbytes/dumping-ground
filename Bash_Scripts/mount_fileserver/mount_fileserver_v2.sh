#!/bin/bash
set -e

KEY_VALUE_CREDENTIALS_FILE="/data/credentials/file-server.json"
TEMP_DIR="/tmp"
FILE_SERVER_ADDR="//svr01"
MOUNT_BASE_DIR="/mnt"


function main() {
    if [ "${USER}" == "joey" ]
    then
        mount_samba "${FILE_SERVER_ADDR}/Data_Share" "${MOUNT_BASE_DIR}/data_share" "joey" "joey"
        mount_samba "${FILE_SERVER_ADDR}/Joey_Share" "${MOUNT_BASE_DIR}/joey_share" "joey" "joey"
        # mount_samba "${FILE_SERVER_ADDR}/Margaret_Share" "${MOUNT_BASE_DIR}/margaret_share" "margaret" "joey"
    else
        echo "ERROR: Unknown User"
    fi
    show_mounts
    draw_separator
}


###########################################################################
# Mount a Samba Share
#   1 - Samba Network Address
#   2 - Mount Path
#   3 - User for Credentials
#   4 - User for uid/gid
###########################################################################
function mount_samba() {
    draw_separator
    local samba_netwk_addr="${1}"
    local local_mount_path="${2}"
    local credentials_user="${3}"
    local uid_user="${4}"
    echo "Mount Samba: ${samba_netwk_addr}"
    echo "    To path: ${local_mount_path}"

    # Make mount directory if it does not exist
    if [ ! -d "${local_mount_path}" ]
    then
        echo "Creating mount directory"
        sudo mkdir -p "${local_mount_path}"
    fi

    # Check if mount directory is already mounted
    set +e
    mountpoint -q "${local_mount_path}"
    local is_mounted=$?
    set -e

    # Mount samba address to local path
    if [ "${is_mounted}" -eq 0 ]
    then
        echo "Already mounted"
    else
        # Samba Credentials
        local samba_cred_file="${TEMP_DIR}/samba_credentials.txt"
        local samba_user=$(key-value -f "${KEY_VALUE_CREDENTIALS_FILE}" get "SAMBA_USERID_${credentials_user}")
        local samba_password=$(key-value -f "${KEY_VALUE_CREDENTIALS_FILE}" get "SAMBA_PASSWORD_${credentials_user}")
        rm -f "${samba_cred_file}"
        touch "${samba_cred_file}"
        echo "username=${samba_user}">>"${samba_cred_file}"
        echo "password=${samba_password}">>"${samba_cred_file}"

        # Mount Options
        local gid_user=$(id -gn "${uid_user}")
        local samba_mount_options="rw,seal,vers=3,uid=${uid_user},gid=${gid_user},credentials=${samba_cred_file}"

        # Mount Samba
        sudo mount.cifs -o "${samba_mount_options}" "${samba_netwk_addr}" "${local_mount_path}"
        rm -f "${samba_cred_file}"
    fi
}


function show_mounts() {
    draw_separator
    set +e
    df -Th -x tmpfs -x devtmpfs -x squashfs
    set -e
}


function draw_separator() {
    echo "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - "
}


main
