source ~/.bash_functions

######################################################################
# Utility aliases
######################################################################
alias ll='ls -l'
alias psg='ps -ef|grep -i'
alias up='cd ..'
alias dfh='fn_dfh'

######################################################################
# Folder aliases
######################################################################
if [ -e ~/Downloads ]; then alias cddl='cd ~/Downloads'; fi;
if [ -e ~/Documents ]; then alias cddoc='cd ~/Documents'; fi;
if [ -e ~/Development ]; then alias cddev='cd ~/Development'; fi;
if [ -e ~/Music ]; then alias cdmus='cd ~/Music'; fi;
if [ -e ~/Pictures ]; then alias cdpic='cd ~/Pictures'; fi;
if [ -e ~/Videos ]; then alias cdvid='cd ~/Videos'; fi;
if [ -e ~/bin ]; then alias cdbin='cd ~/bin'; fi;
if [ -e ~/Dropbox ]; then alias cddrop='cd ~/Dropbox'; fi;
