# Default (no color)
RESET_COLOR="\033[0m"

COLOR[0]="\033[0;30m"
COLOR[1]="\033[0;31m"
COLOR[2]="\033[0;32m"
COLOR[3]="\033[0;33m"
COLOR[4]="\033[0;34m"
COLOR[5]="\033[0;35m"
COLOR[6]="\033[0;36m"
COLOR[7]="\033[0;37m"
COLOR[8]="\033[1;30m"
COLOR[9]="\033[1;31m"
COLOR[10]="\033[1;32m"
COLOR[11]="\033[1;33m"
COLOR[12]="\033[1;34m"
COLOR[13]="\033[1;35m"
COLOR[14]="\033[1;36m"
COLOR[15]="\033[1;37m"

# Normal
BLACK="\033[0;30m"
RED="\033[0;31m"
GREEN="\033[0;32m"
YELLOW="\033[0;33m"
BLUE="\033[0;34m"
PURPLE="\033[0;35m"
CYAN="\033[0;36m"
WHITE="\033[0;37m"

# Bright
BRIGHT_BLACK="\033[1;30m"
BRIGHT_RED="\033[1;31m"
BRIGHT_GREEN="\033[1;32m"
BRIGHT_YELLOW="\033[1;33m"
BRIGHT_BLUE="\033[1;34m"
BRIGHT_PURPLE="\033[1;35m"
BRIGHT_CYAN="\033[1;36m"
BRIGHT_WHITE="\033[1;37m"

# Background
# BG_BLACK="\033[40m"
# BG_RED="\033[41m"
# BG_GREEN="\033[42m"
# BG_YELLOW="\033[43m"
# BG_BLUE="\033[44m"
# BG_PURPLE="\033[45m"
# BG_CYAN="\033[46m"
# BG_WHITE="\033[47m"

# Underline
# UNDLN_BLACK="\033[4;30m"
# UNDLN_RED="\033[4;31m"
# UNDLN_GREEN="\033[4;32m"
# UNDLN_YELLOW="\033[4;33m"
# UNDLN_BLUE="\033[4;34m"
# UNDLN_PURPLE="\033[4;35m"
# UNDLN_CYAN="\033[4;36m"
# UNDLN_WHITE="\033[4;37m"

# High Intensity
# HI_BLACK="\033[0;90m"
# HI_RED="\033[0;91m"
# HI_GREEN="\033[0;92m"
# HI_YELLOW="\033[0;93m"
# HI_BLUE="\033[0;94m"
# HI_PURPLE="\033[0;95m"
# HI_CYAN="\033[0;96m"
# HI_WHITE="\033[0;97m"

# Bold High Intensity
# BOLD_HI_BLACK="\033[1;90m"
# BOLD_HI_RED="\033[1;91m"
# BOLD_HI_GREEN="\033[1;92m"
# BOLD_HI_YELLOW="\033[1;93m"
# BOLD_HI_BLUE="\033[1;94m"
# BOLD_HI_PURPLE="\033[1;95m"
# BOLD_HI_CYAN="\033[1;96m"
# BOLD_HI_WHITE="\033[1;97m"

# High Intensity Background
# BG_HI_BLACK="\033[0;100m"
# BG_HI_RED="\033[0;101m"
# BG_HI_GREEN="\033[0;102m"
# BG_HI_YELLOW="\033[0;103m"
# BG_HI_BLUE="\033[0;104m"
# BG_HI_PURPLE="\033[0;105m"
# BG_HI_CYAN="\033[0;106m"
# BG_HI_WHITE="\033[0;107m"
