function fn_dfh() {
    local heading_color="\033[1;33m" # bright yellow
    local color_reset="\033[0m"
    local df_data=`df -h --output=source,fstype,size,used,avail,pcent,target --exclude-type=tmpfs --exclude-type=squashfs --exclude-type=devtmpfs`
    local df_heading=`echo "${df_data}" | head -1`
    local df_line_cnt=$(( $(echo "${df_data}"|wc -l) - 1 ))
    echo ""
    echo -e "${heading_color}${df_heading}${color_reset}"
    echo "${df_data}" | tail -"${df_line_cnt}" | sort
    echo ""
}
