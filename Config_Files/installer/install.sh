#!/bin/bash
set -e

###############################################################################
# Installation script to install dotfiles.
# NOTE: Existing files will be overwritten if "--force" is passed as parameter 1.
#
# This script uses convention over configuration
#   Parent folder name = name of application that must be installed for dotfile usage
#     The "which" command will be run against this name
#   Child folder names are relative to user home folder
###############################################################################

# empty string = false
FORCE=""

#------------------------------------------------------------------------------
# Find directory of install.sh, and make sure we are running from it
#------------------------------------------------------------------------------
function set_directory() {
    THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
    echo "Using directory: ${THIS_DIR}"
    cd "${THIS_DIR}"
}

#------------------------------------------------------------------------------
# Get list of application directories for dotfiles
#------------------------------------------------------------------------------
function get_application_dirs() {
    APPLICATION_DIRS=`find -mindepth 0 -maxdepth 1 -type d -not -path "./.git" -not -path "." -printf "%f\n"|sort`
}

#------------------------------------------------------------------------------
# Process dotfiles for an application
#------------------------------------------------------------------------------
function process_application_dotfiles() {
    local application="${1}"
    echo "Processing dotfiles for: ${application}"
    local application_installed=`which "${application}"`
    if [ "${application_installed}" ]
    then
        install_application_dotfiles "${application}"
    else
        echo "    Not installed: ${application}"
    fi
}

#------------------------------------------------------------------------------
# Install dotfiles for an application
#------------------------------------------------------------------------------
function install_application_dotfiles() {
    local app="${1}"
    local app_dir="${THIS_DIR}/${app}"
    cd "${app_dir}"
    local app_dotfiles=`find -type f -printf "%f\n"|sort`
    while read -r app_dotfile
    do
        echo "Checking dotfile: ${app_dotfile}"
        if [ -e "${HOME}/${app_dotfile}" ] && [ ! "${FORCE}" ]
        then
            echo "    Dotfile exists, not forcing: ${app_dotfile}"
        else
            echo "    Copying dotfile: ${app_dotfile}"
            cp --force "${app_dir}/${app_dotfile}" "${HOME}/${app_dotfile}"
        fi
    done <<< "${app_dotfiles}"
    cd "${THIS_DIR}"
}

#------------------------------------------------------------------------------
# Main processing logic
#------------------------------------------------------------------------------
function main() {
    echo "BEGIN: Install dotfiles"
    set_directory

    # Process dotfiles
    get_application_dirs
    while read -r application_dir;
    do
        process_application_dotfiles "${application_dir}"
    done <<< "${APPLICATION_DIRS}"

    # End of script
    echo "END: Install dotfiles"
}

main "${@}"
