# EmuParadise Scripts (GoodTools Parsing)

## About

I wrote these scripts after downloading full rom sets from [EmuParadise](http://www.emuparadise.me/Complete_ROM_Sets_(Full_Sets_in_One_File)_ROMs/List-All-Titles/37).
Their purpose is mainly to sort out English speaking/country ROMS, for loading on to a [RetroPie](https://retropie.org.uk/).
This is an interactive script, it is not meant to run headless.

## Requirements

* Linux
* Bash
* Python 3.5 or greater
* [Python-Utilities](https://github.com/joeysbytes/Python-Utilities)

## Instructions

1. Modify emuparadise.sh
   1. Set PY\_UTILS\_DIR to the path for the Python-Utilities project
1. Run emuparadise.sh
