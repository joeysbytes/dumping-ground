#!/bin/bash

EMU_DIR=`pwd`
INPUT_DIR="/data/tmp/gba"
OUTPUT_DIR="/tmp2/emuout/gba"
TEMP_DIR="/tmp2/emutmp/gameboyadvance"
PY_UTILS_DIR="/home/joey/Development/Python-Utilities"
export PYTHONPATH="${PY_UTILS_DIR}"
#echo ""
#echo "${PYTHONPATH}"

clear
python3 "${EMU_DIR}/scripts/emuparadise.py" "${INPUT_DIR}" "${OUTPUT_DIR}" "${TEMP_DIR}"
#python3 "${EMU_DIR}/scripts/emuparadise.py"
