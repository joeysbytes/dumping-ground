Activision Decathlon, The (1984) (Activision).a52
Activision Decathlon, The (1984) (Activision)
  Activision Decathlon, The (1984) (Activision).a52
  Activision Decathlon, The (1984) (Activision) [b1].bin

Adventure 2 Demo (2002) (Ron Lloyd).bin
Adventure 2 Demo (2002) (Ron Lloyd)
  Adventure 2 Demo (2002) (Ron Lloyd).bin

Astro Chase (1982) (Parker Bros-First Star Software).bin
Astro Chase (1982) (Parker Bros-First Star Software)
  Astro Chase (1982) (Parker Bros-First Star Software).bin

Ballblazer (1984) (Atari-Lucasfilm Games).bin
Ballblazer (1984) (Atari-Lucasfilm Games)
  Ballblazer (1984) (Atari-Lucasfilm Games).bin
  Ballblazer (1984) (Atari-Lucasfilm Games) [a1].bin

Beamrider (1984) (Activision).a52
Beamrider (1984) (Activision)
  Beamrider (1984) (Activision).a52
  Beamrider (1984) (Activision) [b1].bin
  Beamrider (1984) (Activision) [o1].bin

Berzerk (1983) (Atari).bin
Berzerk (1983) (Atari)
  Berzerk (1983) (Atari).bin
  Berzerk (1983) (Atari) [b1].bin
  Berzerk (1983) (Atari) [o1].bin

Blueprint (1982) (CBS).a52
Blueprint (1982) (CBS)
  Blueprint (1982) (CBS).a52
  Blueprint (1982) (CBS) [b3].a52
  Blueprint (1982) (CBS) [b1].bin
  Blueprint (1982) (CBS) [b2].bin
  Blueprint (1982) (CBS) [b3][o1].bin
  Blueprint (1982) (CBS) [b3][o2].bin
  Blueprint (1982) (CBS) [o1].bin

* Bounty Bob Strikes Back ($4000) (1984) (Big Five Software).bin
* Bounty Bob Strikes Back ($5000) (1984) (Big Five Software).bin
* Bounty Bob Strikes Back ($A000) (1984) (Big Five Software).bin
* Bounty Bob Strikes Back (Merged) (Big Five Software).bin
Bounty Bob Strikes Back (1984) (Big Five Software)
  Bounty Bob Strikes Back ($4000) (1984) (Big Five Software).bin
  Bounty Bob Strikes Back ($5000) (1984) (Big Five Software).bin
  Bounty Bob Strikes Back ($A000) (1984) (Big Five Software).bin
  Bounty Bob Strikes Back (Merged) (Big Five Software).bin

Buck Rogers - Planet of Zoom (1983) (Sega).bin
Buck Rogers - Planet of Zoom (1983) (Sega)
  Buck Rogers - Planet of Zoom (1983) (Sega).bin
  Buck Rogers - Planet of Zoom (1983) (Sega) [a1].bin
  Buck Rogers - Planet of Zoom (1983) (Sega) [a1][b1].bin
  Buck Rogers - Planet of Zoom (1983) (Sega) [b1].bin

Captain Beeble 5200 Conversion by Kenfuzed (2004) (InHome Software).bin
Captain Beeble 5200 Conversion by Kenfuzed (2004) (InHome Software)
  Captain Beeble 5200 Conversion by Kenfuzed (2004) (InHome Software).bin

Castle Blast (Philly Classic 4) (2002) (Ronen Habot).bin
Castle Blast (2002) (Ronen Habot)
  Castle Blast (Demo V.03) (2002) (Ronen Habot).bin
  Castle Blast (NAVA Demo) (2002) (Ronen Habot).bin
  Castle Blast (Philly Classic 4) (2002) (Ronen Habot).bin

Centipede (1982) (Atari).bin
Centipede (1982) (Atari)
  Centipede (1982) (Atari).bin

Choplifter (1984) (Atari).a52
Choplifter (1984) (Atari)
  Choplifter (1984) (Atari).a52
  Choplifter (1984) (Atari) [o1].bin

Combat II Demo (2002) (Mean Hamster Software).bin
Combat II Demo (2002) (Mean Hamster Software)
  Combat II Demo (2002) (Mean Hamster Software).bin

Congo Bongo (1983) (Sega).bin
Congo Bongo (1983) (Sega)
  Congo Bongo (1983) (Sega).bin
  Congo Bongo (1983) (Sega) [a1].bin
  Congo Bongo (1983) (Sega) [a1][b1].bin
  Congo Bongo (1983) (Sega) [b1].bin

Countermeasure (1983) (Atari).bin
Countermeasure (1983) (Atari)
  Countermeasure (1983) (Atari).bin
  Countermeasure (1983) (Atari) [o1].bin

Defender (1982) (Atari).a52
Defender (1982) (Atari)
  Defender (1982) (Atari).a52
  Defender (1982) (Atari) [b1].a52
  Defender (1982) (Atari) [b2].bin

PAM Diagnostics V2.0 (1983) (Atari).a52
Diagnostics (1983) (Atari)
  PAM Diagnostics V2.0 (1983) (Atari).a52
  Atari Diagnostics V2.3 (1983) (Atari).bin

Atari Diagnostics V2.3 (1983) (Atari).bin
Diagnostics (1983) (Atari)
  PAM Diagnostics V2.0 (1983) (Atari).a52
  Atari Diagnostics V2.3 (1983) (Atari).bin

Dig Dug (1983) (Atari).bin
Dig Dug (1983) (Atari)
  Dig Dug (1983) (Atari) [b2].a52
  Dig Dug (1983) (Atari).bin
  Dig Dug (1983) (Atari) [b1].bin

Dreadnaught Factor, The (1983) (Atari).a52
Dreadnaught Factor, The (1983) (Atari)
  Dreadnaught Factor, The (1983) (Atari).a52
  Dreadnaught Factor, The (1983) (Atari) [o1].bin

Frogger (1983) (Parker Bros).a52
Frogger (1983) (Parker Bros)
  Frogger (1983) (Parker Bros).a52
  Frogger (1983) (Parker Bros) [a1].a52
  Frogger (1983) (Parker Bros) [b1].bin
  Frogger (1983) (Parker Bros) [b1][o1].bin
  Frogger (1983) (Parker Bros) [o1].bin
  Frogger (1983) (Parker Bros) [o2].bin

Frogger 2 - Threedeep! (1984) (Parker Bros).bin
Frogger 2 - Threedeep! (1984) (Parker Bros)
  Frogger 2 - Threedeep! (1984) (Parker Bros) [a1].a52
  Frogger 2 - Threedeep! (1984) (Parker Bros).bin
  Frogger 2 - Threedeep! (1984) (Parker Bros) [a1][o1].bin
  Frogger 2 - Threedeep! (1984) (Parker Bros) [a1][o2].bin
  Frogger 2 - Threedeep! (1984) (Parker Bros) [b1].bin

Galaxian (1982) (Atari).a52
Galaxian (1982) (Atari)
  Galaxian (1982) (Atari).a52
  Galaxian (1982) (Atari) [b1].bin
  Galaxian (1982) (Atari) [o1].bin
  Galaxian (1982) (Atari) [o2].bin

Gorf (1982) (CBS).a52
Gorf (1982) (CBS)
  Gorf (1982) (CBS).a52
  Gorf (1982) (CBS) [o1].bin

Gremlins (1984) (Atari).bin
Gremlins (1984) (Atari)
  Gremlins (1984) (Atari).bin
  Gremlins (1984) (Atari) [b1].bin
  Gremlins (1984) (Atari) [b2].bin

Gyruss (1982) (Parker Bros).bin
Gyruss (1982) (Parker Bros)
  Gyruss (1982) (Parker Bros).bin

H.E.R.O. (1984) (Activision).a52
H.E.R.O. (1984) (Activision)
  H.E.R.O. (1984) (Activision).a52
  H.E.R.O. (1984) (Activision) [b1].bin
  H.E.R.O. (1984) (Activision) [o1].bin

James Bond 007 (1983) (Parker Bros).bin
James Bond 007 (1983) (Parker Bros)
  James Bond 007 (1983) (Parker Bros).bin
  James Bond 007 (1983) (Parker Bros) [a1][b1].bin
  James Bond 007 (1983) (Parker Bros) [b1].bin

Joust (1984) (Atari).bin
Joust (1983-84) (Atari)
  Joust (1983) (Atari) [b1].bin
  Joust (1984) (Atari).bin

Jr Pac-Man (1984) (Atari).a52
Jr Pac-Man (1984) (Atari)
  Jr Pac-Man (1984) (Atari).a52
  Jr Pac-Man (1984) (Atari) [a1].bin
  Jr Pac-Man (1984) (Atari) [b1].bin
  Jr Pac-Man (1984) (Atari) [b2].bin

Jungle Hunt (1983) (Atari).bin
Jungle Hunt (1983) (Atari)
  Jungle Hunt (1983) (Atari).bin
  Jungle Hunt (1983) (Atari) [b1].bin

K-razy Shoot-Out (1982) (CBS).a52
K-razy Shoot-Out (1982) (CBS)
  K-razy Shoot-Out (1982) (CBS).a52
  K-razy Shoot-Out (1982) (CBS) [h1] (Two Port).bin
  K-razy Shoot-Out (1982) (CBS) [o1].bin

KABOOM! (1983) (Activision).a52
KABOOM! (1983) (Activision)
  KABOOM! (1983) (Activision).a52
  KABOOM! (1983) (Activision) [o1].bin

Kangaroo (1982) (Atari).bin
Kangaroo (1982) (Atari)
  Kangaroo (1982) (Atari).bin
  Kangaroo (1982) (Atari) [b1].bin
  Kangaroo (1982) (Atari) [b2].bin
  Kangaroo (1982) (Atari) [o1].bin

Keystone Kapers (1984) (Activision).a52
Keystone Kapers (1984) (Activision)
  Keystone Kapers (1984) (Activision).a52
  Keystone Kapers (1984) (Activision) [b1].bin
  Keystone Kapers (1984) (Activision) [o1].bin

Last Starfighter, The (1984) (Atari).a52
Last Starfighter, The (1984) (Atari)
  Last Starfighter, The (1984) (Atari).a52
  Last Starfighter, The (1984) (Atari) [b1].bin

Mario Brothers (1983) (Atari).bin
Mario Brothers (1983) (Atari)
  Mario Brothers (1983) (Atari).bin
  Mario Brothers (1983) (Atari) [a1].bin
  Mario Brothers (1983) (Atari) [b1].bin

Meebzork (1983) (Atari).a52
Meebzork (1983) (Atari)
  Meebzork (1983) (Atari).a52

Megamania (1983) (Activision).a52
Megamania (1983) (Activision)
  Megamania (1983) (Activision).a52
  Megamania (1983) (Activision) [o1].bin

Meteorites (1983) (Electra Concepts).bin
Meteorites (1983) (Electra Concepts)
  Meteorites (1983) (Electra Concepts) [o1].a52
  Meteorites (1983) (Electra Concepts).bin

Millipede (1984) (Atari).a52
Millipede (1984) (Atari)
  Millipede (1984) (Atari).a52
  Millipede (1984) (Atari) [o1].bin

Miner 2049 (1983) (Big Five Software).a52
Miner 2049 (1983) (Big Five Software)
  Miner 2049 (1983) (Big Five Software).a52
  Miner 2049 (1983) (Big Five Software) [o1].bin

Miniature Golf (1983) (Atari).a52
Miniature Golf (1983) (Atari)
  Miniature Golf (1983) (Atari).a52
  Miniature Golf (1983) (Atari) [b1].a52

Missile Command (1983) (Atari).a52
Missile Command (1983) (Atari)
  Missile Command (1983) (Atari).a52
  Missile Command (1983) (Atari) [b1].a52
  Missile Command (1983) (Atari) [b2].bin
  Missile Command (1983) (Atari) [b3].bin

Montezuma's Revenge (1984) (Parker Bros).a52
Montezuma's Revenge (1984) (Parker Bros)
  Montezuma's Revenge (1984) (Parker Bros).a52
  Montezuma's Revenge (1984) (Parker Bros) [b1].bin

Moon Patrol (1983) (Atari).bin
Moon Patrol (1983) (Atari)
  Moon Patrol (1983) (Atari).bin
  Moon Patrol (1983) (Atari) [o1].bin

Mountain King (1984) (Sunrise Software).a52
Mountain King (1984) (Sunrise Software)
  Mountain King (1984) (Sunrise Software).a52
  Mountain King (1984) (Sunrise Software) [o2].a52
  Mountain King (1984) (Sunrise Software) [h1] (Two Port).bin
  Mountain King (1984) (Sunrise Software) [o1].bin

Mr. Do's Castle (1984) (Parker Bros).a52
Mr. Do's Castle (1984) (Parker Bros)
  Mr. Do's Castle (1984) (Parker Bros).a52
  Mr. Do's Castle (1984) (Parker Bros) [b1].a52
  Mr. Do's Castle (1984) (Parker Bros) [b1][o1].bin
  Mr. Do's Castle (1984) (Parker Bros) [o1].bin

Ms. Pac-Man (1982) (Atari).bin
Ms. Pac-Man (1982) (Atari)
  Ms. Pac-Man (1982) (Atari).bin
  Ms. Pac-Man (1982) (Atari) [b1].bin
  Ms. Pac-Man (1982) (Atari) [b2].bin

PAM SALT V1.02 (1982) (Atari).a52
PAM SALT V1.02 (1982) (Atari)
  PAM SALT V1.02 (1982) (Atari).a52
  PAM SALT V1.02 (1982) (Atari) [o1].a52

Pac-Man (1982) (Atari).bin
Pac-Man (1982) (Atari)
  Pac-Man (1982) (Atari).bin
  Pac-Man (1982) (Atari) [b1].bin
  Pac-Man (1982) (Atari) [b2].bin
  Pac-Man (1982) (Atari) [b3].bin
  Pac-Man (1982) (Atari) [b4].bin

Pengo (1983) (Atari).bin
Pengo (1983) (Atari)
  Pengo (1983) (Atari).bin
  Pengo (1983) (Atari) [b1].bin

Pete's Diagnostics (1982) (Atari).a52
Pete's Diagnostics (1982) (Atari)
  Pete's Diagnostics (1982) (Atari).a52
  Pete's Diagnostics (1982) (Atari) [o1].a52

Pitfall II - The Lost Caverns (1984) (Activision).a52
Pitfall II - The Lost Caverns (1984) (Activision)
  Pitfall II - The Lost Caverns (1984) (Activision).a52
  Pitfall II - The Lost Caverns (1984) (Activision) [o1].bin

Pitfall! (1982) (Activision).a52
Pitfall! (1982) (Activision)
  Pitfall! (1982) (Activision).a52
  Pitfall! (1982) (Activision) [h1] (Two Port).bin
  Pitfall! (1982) (Activision) [o1].bin

Pole Position (1983) (Atari).bin
Pole Position (1983) (Atari)
  Pole Position (1983) (Atari).bin

Popeye (1983) (Parker Bros).bin
Popeye (1983) (Parker Bros)
  Popeye (1983) (Parker Bros).bin
  Popeye (1983) (Parker Bros) [b1].bin

Q-bert (1983) (Parker Bros).a52
Q-bert (1983) (Parker Bros)
  Q-bert (1983) (Parker Bros).a52
  Q-bert (1983) (Parker Bros) [b1].bin
  Q-bert (1983) (Parker Bros) [o1].bin

QIX (1982) (Atari).a52
QIX (1982) (Atari)
  QIX (1982) (Atari).a52
  QIX (1982) (Atari) [b1].bin
  QIX (1982) (Atari) [b2].bin

Quest for Quintana Roo (1984) (Sunrise Software).a52
Quest for Quintana Roo (1984) (Sunrise Software)
  Quest for Quintana Roo (1984) (Sunrise Software).a52
  Quest for Quintana Roo (1984) (Sunrise Software) [o1].bin

Realsports Baseball (1983) (Atari).a52
Realsports Baseball (1983) (Atari)
  Barroom Baseball (1983) (Atari) (Prototype).a52
  Realsports Baseball (1983) (Atari).a52
  Realsports Baseball (1983) (Atari) [o1].bin

Realsports Basketball (1983) (Atari).bin
Realsports Basketball (1983) (Atari)
  Realsports Basketball (1983) (Atari) [a1].a52
  Realsports Basketball (1983) (Atari).bin

Realsports Football (1982) (Atari).bin
Realsports Football (1982) (Atari)
  Realsports Football (1982) (Atari) [b1].a52
  Realsports Football (1982) (Atari).bin

Realsports Soccer (1982) (Atari).bin
Realsports Soccer (1982) (Atari)
  Realsports Soccer (1982) (Atari) [a1].a52
  Realsports Soccer (1982) (Atari) [b2].a52
  Realsports Soccer (1982) (Atari).bin
  Realsports Soccer (1982) (Atari) [b1].bin

Realsports Tennis (1982) (Atari).bin
Realsports Tennis (1982) (Atari)
  Realsports Tennis (1982) (Atari).bin

Rescue on Fractalus (1984) (Atari-Lucasfilm Games).bin
Rescue on Fractalus (1984) (Atari-Lucasfilm Games)
  Rescue on Fractalus (1984) (Atari-Lucasfilm Games).bin

River Raid (1983) (Activision).a52
River Raid (1983) (Activision)
  River Raid (1983) (Activision).a52
  River Raid (1983) (Activision) [b1].bin
  River Raid (1983) (Activision) [b2].bin
  River Raid (1983) (Activision) [o1].bin

Roadrunner (1982) (Atari).a52
Roadrunner (1982) (Atari)
  Roadrunner (1982) (Atari).a52
  Roadrunner (1982) (Atari) [b1].bin

Robotron 2084 (1983) (Atari).a52
Robotron 2084 (1983) (Atari)
  Robotron 2084 (1983) (Atari).a52
  Robotron 2084 (1983) (Atari) [b1].bin
  Robotron 2084 (1983) (Atari) [o1].bin
  Robotron 2084 (1983) (Atari) [o2].bin

Space Dungeon (1983) (Atari).bin
Space Dungeon (1983) (Atari)
  Space Dungeon (1983) (Atari).bin

Space Invaders (1982) (Atari).a52
Space Invaders (1982) (Atari)
  Space Invaders (1982) (Atari).a52
  Space Invaders (1982) (Atari) [b1].bin
  Space Invaders (1982) (Atari) [o1].bin
  Space Invaders (1982) (Atari) [o2].bin

Space Shuttle - A Journey Into Space (1983) (Activision).bin
Space Shuttle - A Journey Into Space (1983) (Activision)
  Space Shuttle - A Journey Into Space (1983) (Activision).bin
  Space Shuttle - A Journey Into Space (1983) (Activision) [o1].bin

Star Raiders (1982) (Atari).bin
Star Raiders (1982) (Atari)
  Star Raiders (1982) (Atari).bin
  Star Raiders (1982) (Atari) [b1].bin
  Star Raiders (1982) (Atari) [b2].bin
  Star Raiders (1982) (Atari) [b3].bin

Star Trek - Strategic Operations Simulator (1983) (Sega).a52
Star Trek - Strategic Operations Simulator (1983) (Sega)
  Star Trek - Strategic Operations Simulator (1983) (Sega).a52
  Star Trek - Strategic Operations Simulator (1983) (Sega) [b1].bin

Star Wars - ROTJ - Death Star Battle (1983) (Parker Bros).a52
Star Wars - ROTJ - Death Star Battle (1983) (Parker Bros)
  Star Wars - ROTJ - Death Star Battle (1983) (Parker Bros).a52
  Star Wars - ROTJ - Death Star Battle (1983) (Parker Bros) [b1].bin

Star Wars - The Arcade Game (1983) (Parker Bros).bin
Star Wars - The Arcade Game (1983) (Parker Bros)
  Star Wars - The Arcade Game (1983) (Parker Bros).bin

Stargate (1984) (Atari).a52
Stargate (1984) (Atari)
  Stargate (1984) (Atari).a52
  Stargate (1984) (Atari) [b1].a52

Super Breakout (1982) (Atari).a52
Super Breakout (1982) (Atari)
  Super Breakout (1982) (Atari).a52
  Super Breakout (1982) (Atari) [b2].bin
  Super Breakout (1982) (Atari) [o1].bin
  Super Breakout (1982) (Atari) [o2].bin

Super Cobra (1983) (Parker Bros).a52
Super Cobra (1983) (Parker Bros)
  Super Cobra (1983) (Parker Bros).a52
  Super Cobra (1983) (Parker Bros) [b1].bin
  Super Cobra (1983) (Parker Bros) [o1].bin
  Super Cobra (1983) (Parker Bros) [o2].bin

Track and Field (1984) (Atari).a52
Track and Field (1984) (Atari)
  Track and Field (1984) (Atari).a52
  Track and Field (1984) (Atari) [o1].bin

Up N' Down 5200 Conversion by Kenfuzed (2004) (Sega).bin
Up N' Down 5200 Conversion by Kenfuzed (2004) (Sega)
  Up N' Down 5200 Conversion by Kenfuzed (2004) (Sega).bin

Vanguard (1983) (Atari).bin
Vanguard (1983) (Atari)
  Vanguard (1983) (Atari).bin
  Vanguard (1983) (Atari) [b1].bin

Wizard of Wor (1982) (CBS).a52
Wizard of Wor (1982) (CBS)
  Wizard of Wor (1982) (CBS).a52
  Wizard of Wor (1982) (CBS) [b1].bin
  Wizard of Wor (1982) (CBS) [o1].bin

Zaxxon (1984) (Sega).bin
Zaxxon (1984) (Sega)
  Zaxxon (1984) (Sega).bin
  Zaxxon (1984) (Sega) [a1].bin

Zenji (1984) (Atari).a52
Zenji (1984) (Atari)
  Zenji (1984) (Atari).a52
  Zenji (1984) (Atari) [o1].a52
  Zenji (1984) (Atari) [b1].bin

Zone Ranger (1984) (Activision).a52
Zone Ranger (1984) (Activision)
  Zone Ranger (1984) (Activision).a52
  Zone Ranger (1984) (Activision) [o1].bin

