"""
This module contains the class for individual rom files.
"""

from pathlib import PurePath
import RomFileConfig as config


def get_flags_from_file_name(file_name):
    """
    Given a file name, return an array of all flags () []
    """
    flags = []
    file_name_len = len(file_name)
    start_pos = 0
    while start_pos < file_name_len:
        if file_name[start_pos] == "(":
            end_pos = start_pos + 1
            while end_pos < file_name_len:
                if file_name[end_pos] == ")":
                    flags.append(file_name[start_pos:end_pos + 1])
                    start_pos = end_pos + 1
                    end_pos = file_name_len
                else:
                    end_pos += 1
        else:
            start_pos += 1
    start_pos = 0
    while start_pos < file_name_len:
        if file_name[start_pos] == "[":
            end_pos = start_pos + 1
            while end_pos < file_name_len:
                if file_name[end_pos] == "]":
                    flags.append(file_name[start_pos:end_pos + 1])
                    start_pos = end_pos + 1
                    end_pos = file_name_len
                else:
                    end_pos += 1
        else:
            start_pos += 1
    return flags


class RomFile():
    """
    This class takes a ROM file name and tries to extract all
    the useful information about it from the file name.
    """


    def __init__(self, rom_file_name):
        # Items derived directly from file name
        self.__full_file_name = rom_file_name
        pure_path = PurePath(self.__full_file_name)
        self.__extension = str(pure_path.suffix)[1:]
        self.__directory = str(pure_path.parent)
        self.__file_name = str(pure_path.name)
        pure_path = PurePath(self.__directory)
        self.__game_name = str(pure_path.name)
        pure_path = PurePath(self.__file_name)
        self.__file_name_no_ext = str(pure_path.stem)
        # Calculated items
        self.__group_key = self.__build_group_key()
        self.__flags = self.__get_flags(self.__file_name_no_ext)
        self.__name_flags = self.__get_flags(self.__game_name)
        self.__skip_rom = False
        self.__sort_key = self.__build_sort_key(self.__flags)
        self.__name_sort_key = self.__build_sort_key(self.__name_flags)


    def get_file_name(self):
        """
        Return the file name without the full path
        """
        return self.__file_name


    def get_full_file_name(self):
        """
        Return full file name.
        """
        return self.__full_file_name


    def get_game_name(self):
        """
        Return name of game.
        """
        return self.__game_name


    def is_skippable(self):
        """
        Return if this ROM has been marked for skipping.
        """
        return self.__skip_rom


    def get_grouping_key(self):
        """
        Return the key to sort files by groupings.
        """
        return self.__group_key


    def get_sorting_key(self):
        """
        Return the sorting key for use to determine best rom choice
        """
        return self.__sort_key


    def get_flags(self):
        """
        Return text version of the flags in the file name
        """
        flags_text = ""
        for flag in self.__flags:
            flags_text += (flag + " ")
        return flags_text.strip()


    def __build_sort_key(self, flags):
        """
        Build the key for sorting the roms within the same group.
        Lower values are sorted to the top.
        While building the key, if any conditions warrant it, the skip_rom
        indicator will be marked as True.
        format: skip|attributes|country|special|translation
        """
        attribute_order = 99
        country_order = 99
        special_order = 99
        translation_order = 99
        version_order = 99999.99999
        attribute_found = False
        country_found = False
        special_found = False
        translation_found = False
        version_found = False

        for flag in flags:
            srt_ord = None
            if srt_ord is None:
                srt_ord = config.get_attribute_code_sort_order(flag)
                if srt_ord is not None:
                    attribute_found = True
                    if srt_ord == -1:
                        self.__skip_rom = True
                    else:
                        if srt_ord < attribute_order:
                            attribute_order = srt_ord
            if srt_ord is None:
                srt_ord = config.get_country_code_sort_order(flag)
                if srt_ord is not None:
                    country_found = True
                    if srt_ord == -1:
                        self.__skip_rom = True
                    else:
                        if srt_ord < country_order:
                            country_order = srt_ord
            if srt_ord is None:
                srt_ord = config.get_special_code_sort_order(flag)
                if srt_ord is not None:
                    special_found = True
                    if srt_ord == -1:
                        self.__skip_rom = True
                    else:
                        if srt_ord < special_order:
                            special_order = srt_ord
            if srt_ord is None:
                srt_ord = config.get_translation_code_sort_order(flag)
                if srt_ord is not None:
                    translation_found = True
                    if srt_ord == -1:
                        self.__skip_rom = True
                    else:
                        if srt_ord < translation_order:
                            translation_order = srt_ord
            if srt_ord is None:
                srt_ord = config.get_version_code_sort_order(flag)
                if srt_ord is not None:
                    version_found = True
                    if srt_ord == -1:
                        self.__skip_rom = True
                    else:
                        if srt_ord < version_order:
                            version_order = srt_ord

        if not attribute_found:
            attribute_order = config.MISSING_ATTRIBUTES_SORT_ORDER
        if not country_found:
            country_order = config.MISSING_COUNTRY_SORT_ORDER
        if not special_found:
            special_order = config.MISSING_SPECIAL_CODES_SORT_ORDER
        if not translation_found:
            translation_order = config.MISSING_TRANSLATION_CODE_SORT_ORDER
        if not version_found:
            version_order = config.MISSING_VERSION_CODE_SORT_ORDER

        sort_key = ""
        if self.__skip_rom:
            sort_key += "Skip:01"
        else:
            sort_key += "Skip:00"
        sort_key += "|"
        sort_key += ("Attr:" + "{0:0>2}".format(attribute_order))
        sort_key += "|"
        sort_key += ("Cntry:" + "{0:0>2}".format(country_order))
        sort_key += "|"
        sort_key += ("Spec:" + "{0:0>2}".format(special_order))
        sort_key += "|"
        sort_key += ("Trns:" + "{0:0>2}".format(translation_order))
        sort_key += "|"
        sort_key += ("Ver:" + "{:11.5f}".format(version_order))

        return sort_key


    def __get_flags(self, name):
        """
        Read game file name and return a list of [] and () text items.
        """
        flags = get_flags_from_file_name(name)
        # Special Super Nintendo Case
        if (name.startswith("BS ")) and (self.__extension == "smc"):
            flags.append("BS ")
        return flags


    def __build_group_key(self):
        """
        Builds the key used to group rom files.
        format: extension|file name
        """
        group_key = (self.__extension + "|" +
                     self.__file_name_no_ext)
        return group_key


    def info(self):
        """
        Return a string with information about this ROM.
        Useful for debugging
        """
        info = (self.__game_name + "/" + self.__file_name)
        info += "\n  Flags: "
        for flag in self.__flags:
            info += (flag + " ")
        info += ("\n  Skippable: " + str(self.__skip_rom))
        info += ("\n  Sort Key: " + self.__sort_key)
        return info
