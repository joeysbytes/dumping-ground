"""
This module contains the variables which control RomFile behavior.
Sort order minimum = 0, maximum = 99
This also contains methods for convenient searching.
"""

#================================================================================
# Attributes
#================================================================================

"""
Full list of known attributes
"""
# AttributeName: [[Set of codes], SortOrder]
ATTRIBUTE_CODE_MAP = {"Alternate": [["[a"], 20],
                      "Bad Dump": [["[b"], 99],
                      "Checksum Bad": [["[x"], 99],
                      "Checksum Faulty": [["[c"], 99],
                      "Fixed": [["[f"], 30],
                      "Hacked": [["[h"], 99],
                      "Overdump": [["[o"], 10],
                      "Pending Dump": [["[!p"], 99],
                      "Pirate": [["[p", "(Pirate"], 99],
                      "Trained": [["[t"], 40],
                      "Verified": [["[!]"], 0]
                     }

# Attributes considered valid for rom
VALID_ATTRIBUTES = {"Verified"}

# If no known attributes are found, should the rom be considered valid?
MISSING_ATTRIBUTES_SORT_ORDER = ATTRIBUTE_CODE_MAP["Verified"][1] + 1


def get_attribute_code_sort_order(flag):
    """
    Return the sort order of the given flag, if it is an attribute.
    If the flag does not match an attribute, None is returned.
    If the flag has a match but is not valid, -1 is returned.
    """
    sort_order = None
    for attribute_key, attribute_value in ATTRIBUTE_CODE_MAP.items():
        for attribute_code in attribute_value[0]:
            if attribute_code in flag:
                if attribute_key in VALID_ATTRIBUTES:
                    sort_order = attribute_value[1]
                else:
                    sort_order = -1
                break
        if sort_order is not None:
            break
    return sort_order


def is_attribute_code(flag):
    """
    Return True or False if the flag matches an attribute.
    """
    sort_order = get_attribute_code_sort_order(flag)
    if sort_order is None:
        return False
    else:
        return True


#================================================================================
# Countries
#================================================================================

# Full list of known country codes
# CountryName: [[Set of codes], SortOrder]
COUNTRY_CODE_MAP = {"Asia": [["(AS)", "(As)"], 99],
                    "Australia": [["(A)"], 4],
                    "Brazil": [["(4)", "(B)"], 99],
                    "Canada": [["(C)"], 5],
                    "China": [["(CH)", "(Ch)"], 99],
                    "Dutch": [["(D)"], 99],
                    "England": [["(UK)"], 2],
                    "Europe": [["(E)", "(EU)"], 3],
                    "Finland": [["(FN)", "(Fn)"], 99],
                    "France": [["(F)"], 99],
                    "French Canada": [["(FC)"], 99],
                    "Germany": [["(G)"], 99],
                    "Greece": [["(GR)", "(Gr)"], 99],
                    "Holland": [["(H)"], 99],
                    "Hong Kong": [["(HK)"], 99],
                    "Italy": [["(I)"], 99],
                    "Japan": [["(1)", "(J)"], 50],
                    "Korea": [["(1)", "(K)"], 99],
                    # "Multi-Language": [["(M"], 20],
                    "Netherlands": [["(NL)", "(Nl)"], 99],
                    "Norway": [["(NO)", "(No)"], 99],
                    "Russia": [["(R)"], 99],
                    "Spain": [["(S)"], 99],
                    "Sweden": [["(SW)", "(Sw)"], 99],
                    "Tawain": [["(Tw)"], 99],
                    "Unknown": [["(Unk)"], 6],
                    "USA": [["(4)", "(U)", "(EU)", "(JU)", "(UE)", "(US)"], 0],
                    "World": [["(F)"], 1],
                   }

# Country codes considered valid for rom
VALID_COUNTRIES = {"Australia",
                   "England",
                   "Europe",
                   # "Multi-Language",
                   "USA",
                   "World",
                   "Canada",
                   "Unknown"}

# If no known country is found, should the rom be considered valid?
MISSING_COUNTRY_SORT_ORDER = COUNTRY_CODE_MAP["Unknown"][1] + 1


def get_country_code_sort_order(flag):
    """
    Return the sort order of the given flag, if it is a country code.
    If the flag does not match a country code, None is returned.
    If the flag has a match but is not valid, -1 is returned.
    """
    sort_order = None
    for country_code_key, country_code_value in COUNTRY_CODE_MAP.items():
        for country_code in country_code_value[0]:
            if country_code in flag:
                if country_code_key in VALID_COUNTRIES:
                    sort_order = country_code_value[1]
                else:
                    sort_order = -1
                break
        if sort_order is not None:
            break
    return sort_order


def is_country_code(flag):
    """
    Return True or False if the flag matches a country code.
    """
    sort_order = get_country_code_sort_order(flag)
    if sort_order is None:
        return False
    else:
        return True


#================================================================================
# Special Codes
#================================================================================

"""
Full list of known special codes
"""
# SpecialCode: [[Set of codes], SortOrder]
SPECIAL_CODE_MAP = {"Beta": [["(Beta", "Beta)"], 99],
                    "BS Rom": [["(BS", "BS "], 99], # super nintendo
                    "Bung Fix": [["[BF"], 90], # game boy
                    "Color": [["[C]"], 0], # game boy
                    "Demo": [["(Demo", "Demo)"], 99],
                    "GameCube": [["(GameCube"], 0],
                    "Hack": [["(Hack", "Hack)"], 99],
                    "Monochrome": [["[M]"], 0], # black and white
                    "Multiboot": [["(MB)", "(MB2GBA)"], 0], # game boy advance
                    "Nintendo Power": [["(NP"], 80], # super nintendo
                    "NTSC": [["(NTSC)"], 0], # tv output
                    "PAL": [["(PAL)"], 5], # tv output
                    "PlayChoice-10": [["(PC10)"], 99], # nintendo
                    "Public Domain": [["(PD"], 99],
                    "Prototype": [["(Prototype"], 99],
                    "Sample": [["(Sample"], 99],
                    "Sachen": [["(Sachen"], 0], # unlicensed by Sachen
                    "Sufami Turbo": [["(ST"], 99], # super nintendo
                    "Super": [["[S]"], 99], # game boy
                    "Universal NES": [["[U]"], 0], # nintendo
                    "Unlicensed": [["(Unl)"], 0],
                    "Versus": [["(VS)"], 70]
                   }

# Special codes considered valid for rom
VALID_SPECIAL_CODES = {"Unlicensed",
                       "Color",
                       "Monochrome",
                       "Sachen",
                       "GameCube",
                       "Multiboot",
                       "Universal NES"}

# If no known special codes are found, should the rom be considered valid?
MISSING_SPECIAL_CODES_SORT_ORDER = 0


def get_special_code_sort_order(flag):
    """
    Return the sort order of the given flag, if it is a special code.
    If the flag does not match a special code, None is returned.
    If the flag has a match but is not valid, -1 is returned.
    """
    sort_order = None
    for special_code_key, special_code_value in SPECIAL_CODE_MAP.items():
        for special_code in special_code_value[0]:
            if special_code in flag:
                if special_code_key in VALID_SPECIAL_CODES:
                    sort_order = special_code_value[1]
                else:
                    sort_order = -1
                break
        if sort_order is not None:
            break
    return sort_order


def is_special_code(flag):
    """
    Return True or False if the flag matches a special code.
    """
    sort_order = get_special_code_sort_order(flag)
    if sort_order is None:
        return False
    else:
        return True


#================================================================================
# Translation Codes
#================================================================================

"""
Full list of known translation codes
"""
# TranslationCode: [[Set of codes], SortOrder]
TRANSLATION_CODE_MAP = {"Albanian": [["Alb"], 99],
                        "Arabic": [["Ara"], 99],
                        "Brazilian": [["Bra"], 99],
                        "Chinese": [["Chi"], 99],
                        "Simplified Chinese": [["ChS", "SChi"], 99],
                        "Traditional Chinese": [["ChT", "TChi"], 99],
                        "Croatia": [["Cro"], 99],
                        "Danish": [["Dan"], 99],
                        "Dutch": [["Dut"], 99],
                        "English": [["Eng"], 10],
                        "Finnish": [["Fin"], 99],
                        "French": [["Fre"], 99],
                        "German": [["Ger"], 99],
                        "Greek": [["Gre"], 99],
                        "Hebrew": [["Heb"], 99],
                        "Italian": [["Ita"], 99],
                        "Japanese": [["Jap"], 99],
                        "Korean": [["Kor"], 99],
                        "Latvian": [["Lat"], 99],
                        "Lithuanian": [["Lit"], 99],
                        "Norwegian": [["Nor"], 99],
                        "Polish": [["Pol"], 99],
                        "Portuguese": [["Por"], 99],
                        "Romanian": [["Rom"], 99],
                        "Rumanian": [["Rum"], 99],
                        "Russian": [["Rus"], 99],
                        "Serbian": [["Ser"], 99],
                        "Spanish": [["Spa", "Esp"], 99],
                        "Swedish": [["Swe"], 99],
                        "Thai": [["Tai", "Thai"], 99],
                        "Turkish": [["Tur"], 99],
                        "Uruguay": [["Uru"], 99],
                        "Unknown": [["Hun", "Cat"], 99],
                        "Vietnamese": [["Viet"], 99]
                       }

# Translation codes considered valid for rom
VALID_TRANSLATION_CODES = {"English"}

# If no known translation codes are found, should the rom be considered valid?
MISSING_TRANSLATION_CODE_SORT_ORDER = 0


def get_translation_code_sort_order(flag):
    """
    Return the sort order of the given flag, if it is a translation code.
    If the flag does not match a translation code, None is returned.
    If the flag has a match but is not valid, -1 is returned.
    """
    sort_order = None
    for translation_code_key, translation_code_value in TRANSLATION_CODE_MAP.items():
        for translation_code in translation_code_value[0]:
            # Translation code examples: (Eng) (Ger-Spa-Eng) [T+Eng] [T-Eng]
            translation_codes_modified = []
            translation_codes_modified.append("(" + translation_code)
            translation_codes_modified.append("-" + translation_code)
            translation_codes_modified.append("[T+" + translation_code)
            translation_codes_modified.append("[T-" + translation_code)
            for translation_code_modified in translation_codes_modified:
                if translation_code_modified in flag:
                    if translation_code_key in VALID_TRANSLATION_CODES:
                        sort_order = translation_code_value[1]
                    else:
                        sort_order = -1
                    break
            if sort_order is not None:
                break
        if sort_order is not None:
            break
    return sort_order


def is_translation_code(flag):
    """
    Return True or False if the flag matches a translation code.
    """
    sort_order = get_translation_code_sort_order(flag)
    if sort_order is None:
        return False
    else:
        return True


#================================================================================
# Version Codes
#================================================================================

"""
Full list of known version codes
"""
# VersionCode: [[Set of codes], SortOrder]
VERSION_CODE_MAP = {"Revision": [["(REV", "(Revision "], 0],
                    "Release": [["(Release "], 0],
                    "Program": [["(PRG"], 0],
                    "Version": [["(V", "(v"], 0]
                   }

# Version codes considered valid for rom
VALID_VERSION_CODES = {"Revision",
                       "Release",
                       "Program",
                       "Version"
                      }

# If no known translation codes are found, should the rom be considered valid?
MISSING_VERSION_CODE_SORT_ORDER = 99999.99999


def __get_version(version_flag):
    """
    Return the version number from a specified type of flag(s).
    """
    version = 99.999
    # We could use regular expressions, but there is a wide variety (free-form)
    # examples of versions, easier to manually extract the number.
    # For simplicity, if it is non-numeric, we are treating it as version 0.
    start_pos = None
    index = 0
    while (start_pos is None) and (index < len(version_flag)):
        if (version_flag[index] >= "0") and (version_flag[index] <= "9"):
            start_pos = index
        index += 1
    if start_pos is not None:
        ver = version_flag[start_pos:-1]
        try:
            ver_num = float(ver)
            if ver_num < 100.000:
                version = version - ver_num
        except ValueError:
            pass
    return version


def get_version_code_sort_order(flag):
    """
    Return the sort order of the given flag, if it is a version code.
    If the flag does not match a special code, None is returned.
    If the flag has a match but is not valid, -1 is returned.
    """
    sort_order = None
    for version_code_key, version_code_value in VERSION_CODE_MAP.items():
        for version_code in version_code_value[0]:
            if version_code in flag:
                if version_code_key in VALID_VERSION_CODES:
                    sort_order = __get_version(flag)
                else:
                    sort_order = -1
                break
        if sort_order is not None:
            break
    return sort_order


def is_version_code(flag):
    """
    Return True or False if the flag matches a version code.
    """
    sort_order = get_version_code_sort_order(flag)
    if sort_order is None:
        return False
    else:
        return True
