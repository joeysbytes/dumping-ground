"""
This module holds the class for dealing with groupings
of rom files under the same directory.
"""

from utilities import file_utils as file_utils
from RomFile import RomFile as RomFile

class RomGroups():
    """
    This class creates and loads RomFile objects from a given directory.
    The directory is assumed to contain files for the same game only.
    """

    def __init__(self, game_directory):
        self.__all_roms = self.__get_all_roms(game_directory)
        self.__filtered_roms = self.__get_filtered_roms()
        self.__rom_groups = self.__get_rom_groups()
        self.__groups_index = 0


    def get_best_roms(self):
        """
        Take the current grouping and return a list of the best rom choice(s).
        The top roms of the list with the same matching sort keys are selected.
        If we are at the end of the list, None is returned.
        """
        best_roms = []
        index = self.__groups_index
        if index >= len(self.__rom_groups):
            return None
        rom_group = self.__rom_groups[index]
        hold_sort_key = rom_group[0].get_sorting_key()
        for rom_file in rom_group:
            rom_sort_key = rom_file.get_sorting_key()
            if rom_sort_key == hold_sort_key:
                best_roms.append(rom_file)
            else:
                break
        index += 1
        self.__groups_index = index
        return best_roms


    def get_all_roms_formatted(self):
        """
        Return a string of all the roms in a formatted manner.
        """
        rom_text = ""
        all_roms = self.__all_roms
        rom_text += (all_roms[0].get_game_name() + "\n")
        for rom_file in all_roms:
            rom_text += ("  " + rom_file.get_file_name() + "\n")
        return rom_text


    def __get_all_roms(self, game_directory):
        """
        Go through each file in the directory and build a RomFile.
        Sort by grouping key, but do not remove skippable roms.
        """
        rom_list = []
        file_list = file_utils.get_list_of_files(game_directory,
                                                 include_files=True,
                                                 include_directories=False)
        for file_item in file_list:
            rom_file = RomFile(file_item)
            rom_list.append(rom_file)
        rom_list.sort(key=lambda r: r.get_grouping_key())
        return rom_list


    def __get_filtered_roms(self):
        """
        Take the all roms list, remove skippable roms, then
        """
        all_roms = self.__all_roms
        filtered_roms = []
        for rom_file in all_roms:
            if not rom_file.is_skippable():
                filtered_roms.append(rom_file)
        # filtered_roms.sort(key=lambda r: r.get_grouping_key())  # shouldn't be needed
        return filtered_roms


    def __get_rom_groups(self):
        """
        Take the filtered roms list, split the list in to groupings.
        """
        rom_list = self.__filtered_roms
        rom_groups = []
        rom_group = []
        previous_extension = ""
        current_extension = ""
        for rom_item in rom_list:
            current_extension = rom_item.get_grouping_key().split("|")[0]
            if current_extension != previous_extension:
                if len(rom_group) > 0:
                    rom_groups.append(rom_group)
                rom_group = []
                previous_extension = current_extension
            rom_group.append(rom_item)
        if len(rom_group) > 0:
            rom_groups.append(rom_group)
        # Sort each grouping
        for rom_group in rom_groups:
            rom_group.sort(key=lambda r: r.get_sorting_key())
        return rom_groups
