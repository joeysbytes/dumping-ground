"""
This program is designed to help the user with the steps
necessary to extract specific ROMs from EmuParadise fullset
ROMs.
"""

import sys
from pathlib import PurePath
from utilities.Menu import Menu as Menu
from utilities import string_utils as string_utils
from utilities import dir_utils as dir_utils
from utilities import file_utils as file_utils
from utilities import archive_utils as archive_utils
from utilities import prompt_utils as prompt_utils
from RomGroups import RomGroups as RomGroups
import RomFile as RomFile
import RomFileConfig as RomFileConfig


_CONFIG = {}

def _build_pre_menu_text():
    """
    Build the dynamically generated pre menu text
    """
    menu = _CONFIG["main_menu"]
    pre_menu_text = ("Input Dir : " + str(_CONFIG["input_dir"]) + "\n" +
                     "Output Dir: " + str(_CONFIG["output_dir"]) + "\n" +
                     "Temp Dir  : " + str(_CONFIG["temp_dir"]))
    menu.set_pre_menu_text(pre_menu_text)


def _get_directory_from_user(directory_title):
    """
    Prompt user for a directory to set (or create)
    """
    print((string_utils.underline("Setting " + directory_title + " Directory") + "\n"))
    user_directory = dir_utils.prompt_user_for_directory(creation_allowed=True)
    return user_directory


def _list_files():
    """
    Print file list to screen
    """
    print(string_utils.underline("Files in: " + _CONFIG["input_dir"]))
    file_utils.print_list_of_files(_CONFIG["input_dir"])
    print()


def _show_unknown_regions_and_attributes():
    """
    Go through all files in input directory, and list unique region and attribute codes
    that are not already defined in RomFileConfig.
    """
    print(string_utils.underline("Show File Region And Attribute Codes"))
    print()
    codes = set()
    dir_list = dir_utils.get_list_of_directories(_CONFIG["input_dir"])
    for dir_name in dir_list:
        file_list = file_utils.get_list_of_files(dir_name,
                                                 include_directories=False,
                                                 include_files=True)
        for file_name in file_list:
            flags = RomFile.get_flags_from_file_name(file_name)
            for flag in flags:
                codes.add(flag)
    unknown_codes = set()
    for code in codes:
        if not (RomFileConfig.is_attribute_code(code) or
                RomFileConfig.is_country_code(code) or
                RomFileConfig.is_special_code(code) or
                RomFileConfig.is_translation_code(code) or
                RomFileConfig.is_version_code(code)):
            unknown_codes.add(code)
    for code in sorted(unknown_codes):
        print(code)
    print()


def _uncompress_single_file(file_name=None):
    """
    Prompt user for a file (if not passed in) and uncompress it.
    """
    print(string_utils.underline("Uncompress a single file"))
    print()
    msg = "Do you want to delete the original archive file afterwards"
    delete_original_file = prompt_utils.prompt_user_for_yes_or_no(msg)
    while file_name is None:
        user_file_name = file_utils.prompt_for_existing_file(_CONFIG["input_dir"])
        if archive_utils.is_compressed_file(user_file_name):
            file_name = user_file_name
        else:
            print("*** Not a compressed file\n")
    archive_utils.uncompress_file(file_name, _CONFIG["output_dir"])
    if delete_original_file:
        print("\nDeleting file: " + file_name)
        file_utils.delete_file(file_name)
    print()


def _uncompress_all_files():
    """
    Uncompress all archive files in the input directory to their own game folders
    """
    print(string_utils.underline("Uncompress all files"))
    print()
    msg = "Do you want to delete each original archive file afterwards"
    delete_original_file = prompt_utils.prompt_user_for_yes_or_no(msg)
    file_list = file_utils.get_list_of_files(_CONFIG["input_dir"], include_directories=False)
    for file_name in file_list:
        if archive_utils.is_compressed_file(file_name):
            archive_name = PurePath(file_name).stem
            output_directory = (_CONFIG["output_dir"] + "/" + archive_name)
            print("\nCreating directory: " + output_directory)
            dir_utils.create_directory(output_directory)
            archive_utils.uncompress_file(file_name, output_directory)
            if delete_original_file:
                print("\nDeleting file: " + file_name)
                file_utils.delete_file(file_name)
    print()


def _rename_directory():
    """
    Rename the input directory
    """
    print(string_utils.underline("Rename directory"))
    input_directory = _CONFIG["input_dir"]
    base_directory = dir_utils.get_parent(input_directory)
    new_directory = dir_utils.prompt_user_for_new_directory(base_directory)
    print("\nMoving " + input_directory + " to " + new_directory + "\n")
    dir_utils.rename_directory(_CONFIG["input_dir"], new_directory)
    _CONFIG["input_dir"] = new_directory
    if _CONFIG["output_dir"] == input_directory:
        _CONFIG["output_dir"] = new_directory
    if _CONFIG["temp_dir"] == input_directory:
        _CONFIG["temp_dir"] = new_directory


def _build_rom_extraction_list():
    """
    Go through all game directories and make list of the best roms to use.
    """


    def prompt_user_for_roms(rom_list):
        """
        Program calculated multiple possibilities, ask user to select ROMs to choose.
        """
        # set up menu
        title = rom_list[0].get_game_name()
        post_menu_text = ("Use commas to separate multiple choices")
        prompt = "Select ROM(s)"
        rom_menu = Menu(title=title, post_menu_text=post_menu_text, prompt=prompt)
        index = 1
        for rom_item in rom_list:
            key = str(index)
            description = (rom_item.get_file_name() + " : " +
                           rom_item.get_sorting_key())
            rom_menu.add_menu_option(key, description)
            index += 1
        rom_menu.add_menu_option("z", "No Selection")
        choices = rom_menu.prompt_user_for_choices()
        user_rom_list = []
        for choice in choices:
            if choice != "Z":
                index = int(choice) - 1
                user_rom_list.append(rom_list[index])
        return user_rom_list


    print(string_utils.underline("Build ROM extraction list"))
    print()
    # set up log files
    no_roms_file_name = (_CONFIG["temp_dir"] + "no_roms_found.txt")
    no_roms_file = open(no_roms_file_name, 'w')
    selected_roms_file_name = (_CONFIG["temp_dir"] + "selected_roms.txt")
    selected_roms_file = open(selected_roms_file_name, 'w')
    # process games
    dir_list = dir_utils.get_list_of_directories(_CONFIG["input_dir"])
    for dir_item in dir_list:
        #print("Processing: " + dir_item)
        rom_groups = RomGroups(dir_item)
        rom_list = rom_groups.get_best_roms()
        if rom_list is None:
            no_roms_file.write(rom_groups.get_all_roms_formatted() + "\n")
            no_roms_file.flush()
        else:
            while rom_list is not None:
                rom_list_length = len(rom_list)
                # Best rom found
                if rom_list_length == 1:
                    selected_roms_file.write(rom_list[0].get_file_name() + "\n")
                    selected_roms_file.write(rom_groups.get_all_roms_formatted() + "\n")
                    selected_roms_file.flush()
                    file_utils.copy_file(rom_list[0].get_full_file_name(),
                                         _CONFIG["output_dir"] + rom_list[0].get_file_name())
                # Prompt user for best rom
                else:
                    user_rom_list = prompt_user_for_roms(rom_list)
                    if len(user_rom_list) == 0:
                        no_roms_file.write("* " + rom_groups.get_all_roms_formatted() + "\n")
                        no_roms_file.flush()
                    else:
                        for user_rom_item in user_rom_list:
                            selected_roms_file.write("* " + user_rom_item.get_file_name() + "\n")
                            file_utils.copy_file(user_rom_item.get_full_file_name(),
                                                 _CONFIG["output_dir"] + user_rom_item.get_file_name())
                        selected_roms_file.write(rom_groups.get_all_roms_formatted() + "\n")
                        selected_roms_file.flush()
                rom_list = rom_groups.get_best_roms()
    # close log files
    selected_roms_file.close()
    no_roms_file.close()
    print()


def _process_choice(choice):
    """
    Given the selection made by the user on the main menu, perform the next action.
    Return True or False if the main menu needs rebuilt
    """
    if choice == "1":
        _uncompress_single_file()
    elif choice == "2":
        _uncompress_all_files()
    elif choice == "3":
        _build_rom_extraction_list()
    elif choice == "L":
        _list_files()
    elif choice == "R":
        _rename_directory()
        _build_pre_menu_text()
    elif choice == "=":
        _CONFIG["output_dir"] = _CONFIG["input_dir"]
        _build_pre_menu_text()
    elif choice == "I":
        _CONFIG["input_dir"] = _get_directory_from_user("Input")
        _build_pre_menu_text()
    elif choice == "O":
        _CONFIG["output_dir"] = _get_directory_from_user("Output")
        _build_pre_menu_text()
    elif choice == "T":
        _CONFIG["temp_dir"] = _get_directory_from_user("Temp")
        _build_pre_menu_text()
    elif choice == "U":
        _show_unknown_regions_and_attributes()
    else:
        print("\n*** Undefined Option\n")


def main():
    """
    The main method that runs everything
    """
    main_menu = _CONFIG["main_menu"]
    print()
    choice = main_menu.prompt_user_for_choice()
    while choice != "Q":
        _process_choice(choice)
        choice = main_menu.prompt_user_for_choice()
    print("Exiting\n")
    quit()


def init():
    """
    Initial items to set up before starting the main program
    """
    print()
    # build main menu
    menu = _CONFIG["main_menu"] = Menu(title="EmuParadise ROM Helper Main Menu")
    menu.add_menu_option("1", "Uncompress Single File from Input to Output Directory")
    menu.add_menu_option("2", "Uncompress All Files from Input to Output Directory")
    menu.add_menu_option("3", "Build ROM Extraction List from Input to Temp Directory")
    menu.add_menu_option("L", "List Input Directory Files")
    menu.add_menu_option("R", "Rename Input Directory")
    menu.add_menu_option("I", "Set/Create Input Directory")
    menu.add_menu_option("O", "Set/Create Ouput Directory")
    menu.add_menu_option("T", "Set/Create Temp Directory")
    menu.add_menu_option("=", "Set Output Directory = Input Directory")
    menu.add_menu_option("U", "Show Unknown Regions/Attributes On File Names from Input Directory")
    menu.add_menu_option("Q", "Quit/Exit")
    # set required directories
    _CONFIG["input_dir"] = None
    _CONFIG["output_dir"] = None
    _CONFIG["temp_dir"] = None
    if len(sys.argv) >= 2:
        if dir_utils.directory_exists(sys.argv[1]):
            _CONFIG["input_dir"] = dir_utils.append_separator(sys.argv[1])
        else:
            print("Input directory not found: " + sys.argv[1])
            _process_choice("I")
    else:
        _process_choice("I")
    if len(sys.argv) >= 3:
        if dir_utils.directory_exists(sys.argv[2]):
            _CONFIG["output_dir"] = dir_utils.append_separator(sys.argv[2])
        else:
            print("Ouput directory not found: " + sys.argv[2])
            _process_choice("O")
    else:
        _process_choice("O")
    if len(sys.argv) >= 4:
        if dir_utils.directory_exists(sys.argv[3]):
            _CONFIG["temp_dir"] = dir_utils.append_separator(sys.argv[3])
        else:
            print("Temp directory not found: " + sys.argv[3])
            _process_choice("T")
    else:
        _process_choice("T")
    _build_pre_menu_text() # make sure latest values are accounted for


init()
main()

