# Python Utilities

## About

This is a collection of Python 3 utilities that can be incorporated in to other projects.

## Requirements

* Linux
* Python 3.5 or greater

## The Utilities

* archive_utils - methods that work with archive files
* prompt_utils - methods that prompt the user for input
