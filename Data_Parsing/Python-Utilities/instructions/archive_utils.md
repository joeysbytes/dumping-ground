# Archive Utilities (archive_utils.py)

The Archive Utilities contain methods that work with archive files.

## Requirements

* Linux
* Python 3.5 or greater
* Archive utilities
  * 7z
  * unrar
  * unzip

## Notes

* These utilities make system calls to linux command line tools.  The newer syntax is used, and this forces the minimum Python version to be 3.5.

## Methods

* is\_compressed\_file(string file\_name)
  * determines if the file is a supported archive type by its extension.
* uncompress\_file(string file\_name, string output\_directory)
  * determines the type of compressed file by its extension, and outputs the files to the output directory.
