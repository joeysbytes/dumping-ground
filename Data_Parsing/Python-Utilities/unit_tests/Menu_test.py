import sys
sys.path.append("../")
import unittest
from utilities.Menu import Menu as Menu

class TestMenu(unittest.TestCase):


    def test_build_menu_text_all_parts(self):
        expected_result = ("Main Menu" + "\n" +
                           "---------" + "\n" +
                           "\n" +
                           "The weather looks sunny today." + "\n" +
                           "\n" +
                           "C) Change City" + "\n" +
                           "R) Refresh" + "\n" +
                           "0) Administration" + "\n" +
                           "Quit) Exit / Quit" + "\n" +
                           "\n" +
                           "CPU Percentage = 10%" + "\n" +
                           "\n")
        title = "Main Menu"
        pre_menu_text = "The weather looks sunny today."
        post_menu_text = "CPU Percentage = 10%"
        menu = Menu(title=title, pre_menu_text=pre_menu_text, post_menu_text=post_menu_text)
        menu.add_menu_option("C", "Change City")
        menu.add_menu_option("R", "Refresh")
        menu.add_menu_option("0", "Administration")
        menu.add_menu_option("Quit", "Exit / Quit")
        menu._build_menu_text()
        result = menu._menu_text
        # print(result)
        msg = "Menu text did not build correctly."
        self.assertEqual(expected_result, result, msg=msg)


    def test_build_menu_text_no_parts(self):
        expected_result = ""
        title = None
        pre_menu_text = None
        post_menu_text = None
        menu = Menu(title=title, pre_menu_text=pre_menu_text, post_menu_text=post_menu_text)
        menu._build_menu_text()
        result = menu._menu_text
        # print(result)
        msg = "Empty menu text did not build correctly."
        self.assertEqual(expected_result, result, msg=msg)


    def test_build_menu_text_no_pre_no_post(self):
        expected_result = ("Main Menu" + "\n" +
                           "---------" + "\n" +
                           "\n" +
                           "C) Change City" + "\n" +
                           "R) Refresh" + "\n" +
                           "0) Administration" + "\n" +
                           "Quit) Exit / Quit" + "\n" +
                           "\n")
        title = "Main Menu"
        pre_menu_text = None
        post_menu_text = None
        menu = Menu(title=title, pre_menu_text=pre_menu_text, post_menu_text=post_menu_text)
        menu.add_menu_option("C", "Change City")
        menu.add_menu_option("R", "Refresh")
        menu.add_menu_option("0", "Administration")
        menu.add_menu_option("Quit", "Exit / Quit")
        menu._build_menu_text()
        result = menu._menu_text
        # print(result)
        msg = "Menu text did not build correctly."
        self.assertEqual(expected_result, result, msg=msg)


if __name__ == '__main__':
    unittest.main()
