import sys
sys.path.append("../")
import unittest
from utilities import prompt_utils as prompt_utils

class TestPromptUtils(unittest.TestCase):


    def test_is_valid_choice_array_scenarios(self):
        # choice, valid_choices, case_sensitive, is_valid
                         # empty everything
        test_scenarios = ((None, None, False, False),
                          (None, None, True, False),
                          (None, [], False, False),
                          (None, [], True, False),
                          ("", None, False, False),
                          ("", None, True, False),
                          ("", [], False, False),
                          ("", [], True, False),
                          # empty valid choices
                          ("a", None, False, False),
                          ("b", None, True, False),
                          ("c", [], False, False),
                          ("d", [], True, False),
                          # empty choice
                          (None, ["a", "b", "c"], False, False),
                          (None, ["a", "b", "c"], True, False),
                          ("", ["a", "b", "c"], False, False),
                          ("", ["a", "b", "c"], True, False),
                          ("", ["a", "", "c"], False, True),
                          ("", ["a", "", "c"], True, True),
                          # case sensitive
                          ("a", ["a", "b", "3", ""], True, True),
                          ("b", ["a", "b", "3", ""], True, True),
                          ("3", ["a", "b", "3", ""], True, True),
                          ("A", ["a", "b", "3", ""], True, False),
                          ("B", ["a", "b", "3", ""], True, False),
                          # case insensitive
                          ("a", ["a", "b", "3", ""], False, True),
                          ("b", ["a", "b", "3", ""], False, True),
                          ("3", ["a", "b", "3", ""], False, True),
                          ("A", ["a", "b", "3", ""], False, True),
                          ("B", ["a", "b", "3", ""], False, True))
        for test_scenario in test_scenarios:
            choice = test_scenario[0]
            valid_choices = test_scenario[1]
            case_sensitive = test_scenario[2]
            is_valid = test_scenario[3]
            msg = ("Choice: " + str(choice) +
                   ", Valid Choices: " + str(valid_choices) +
                   ", Case Sensitive: " + str(case_sensitive))
            # print(msg)
            result = prompt_utils.is_valid_choice(
                choice, valid_choices, case_sensitive=case_sensitive)
            self.assertEqual(is_valid, result, msg=msg)


    def test_is_valid_text_array_scenarios(self):
        # text, empty text allowed, expected result
        test_scenarios = ((None, False, False),
                          ("", False, False),
                          ("", True, True),
                          ("abc", False, True),
                          ("abc", True, True))
        for test_scenario in test_scenarios:
            text = test_scenario[0]
            empty_text_allowed = test_scenario[1]
            is_valid = test_scenario[2]
            msg = ("Text: " + str(text) +
                   ", Empty Text Allowed: " + str(empty_text_allowed))
            # print(msg)
            result = prompt_utils.is_valid_text(text, empty_text_allowed=empty_text_allowed)
            self.assertEqual(is_valid, result, msg=msg)


    def test_build_prompt_msg_array_scenarios(self):
        # prompt, prompt suffix, append newline, expected result
                         # empty prompt and prompt suffix
        test_scenarios = ((None, None, False, ""),
                          (None, None, True, "\n"),
                          (None, "", False, ""),
                          (None, "", True, "\n"),
                          ("", None, False, ""),
                          ("", None, True, "\n"),
                          ("", "", False, ""),
                          ("", "", True, "\n"),
                          # prompt only
                          ("Prompt Msg ", None, False, "Prompt Msg "),
                          ("Prompt Msg ", None, True, "Prompt Msg\n"),
                          ("Prompt Msg ", "", False, "Prompt Msg "),
                          ("Prompt Msg ", "", True, "Prompt Msg\n"),
                          # prompt suffix only
                          (None, "> ", False, "> "),
                          (None, "> ", True, ">\n"),
                          ("", "> ", False, "> "),
                          ("", "> ", True, ">\n"),
                          # prompt and prompt suffix
                          ("Prompt Here ", "] ", False, "Prompt Here ] "),
                          ("Prompt Here ", "] ", True, "Prompt Here ]\n"))
        for test_scenario in test_scenarios:
            prompt = test_scenario[0]
            prompt_suffix = test_scenario[1]
            prompt_on_next_line = test_scenario[2]
            expected_result = test_scenario[3]
            msg = ("Prompt: " + str(prompt) +
                   ", Prompt Suffix: " + str(prompt_suffix) +
                   ", Prompt on Next Line: " + str(prompt_on_next_line))
            # print(msg)
            result = prompt_utils._build_prompt_msg(prompt, prompt_suffix=prompt_suffix,
                                                    prompt_on_next_line=prompt_on_next_line)
            self.assertEqual(expected_result, result, msg=msg)


if __name__ == '__main__':
    unittest.main()
