import sys
sys.path.append("../")
import unittest
from utilities import string_utils as string_utils

class TestStringUtils(unittest.TestCase):


    def test_line_scenarios(self):
        # length, characters to build line, expected result
        test_scenarios = ((None, None, None),
                          (None, "", None),
                          (None, 5, None),
                          (None, "-", None),
                          ("", None, None),
                          ("", "", None),
                          ("", 5, None),
                          ("", "-", None),
                          ("a", None, None),
                          ("a", "", None),
                          ("a", 5, None),
                          ("a", "-", None),
                          ("1", None, None),
                          ("1", "", None),
                          ("1", 5, None),
                          ("1", "-", None),
                          (-5, None, None),
                          (-5, "", None),
                          (-5, 5, None),
                          (-5, "-", None),
                          (0, None, None),
                          (0, "", None),
                          (0, 5, None),
                          (0, "-", None),
                          (11, None, None),
                          (11, "", None),
                          (11, 5, None),
                          (11, "-", "-----------"),
                          (11, "-_", "-_-_-_-_-_-"),
                          (11, "abc", "abcabcabcab"))
        for test_scenario in test_scenarios:
            length = test_scenario[0]
            chars = test_scenario[1]
            expected_result = test_scenario[2]
            msg = ("Length: " + str(length) +
                   " Chars: " + str(chars))
            result = string_utils.line(length, chars)
            self.assertEqual(expected_result, result, msg)


    def test_underline_scenarios(self):
        # text, chars, expected result
        test_scenarios = ((None, None, None),
                          (None, "", None),
                          (None, 5, None),
                          (None, "-", None),
                          ("", None, ""),
                          ("", "", ""),
                          ("", 5, ""),
                          ("", "-", ""),
                          (5, None, 5),
                          (5, "", 5),
                          (5, 5, 5),
                          (5, "-", 5),
                          ("text message", None, "text message"),
                          ("text message", "", "text message"),
                          ("text message", 5, "text message"),
                          ("text message", "-", "text message\n------------"))
        for test_scenario in test_scenarios:
            text = test_scenario[0]
            chars = test_scenario[1]
            expected_result = test_scenario[2]
            msg = ("Text: " + str(text) +
                   " Chars: " + str(chars))
            result = string_utils.underline(text, chars)
            self.assertEqual(expected_result, result, msg)


if __name__ == '__main__':
    unittest.main()
