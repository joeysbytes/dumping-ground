"""
This module contains a Menu class for use in terminal programs.
"""

import collections
from utilities import prompt_utils as prompt_utils
from utilities import string_utils as string_utils

class Menu():
    """
    This class is a flexible menu system which contains a title, pre-menu display,
    menu, post-menu display, and a prompt.  An example of a menu is shown below:

    ================================================================================

    MAIN TITLE, if any
    ------------------

    This is where the pre-menu text is displayed, if any.

    1) Option 1
    2) Option 2
    A) Option A
    Quit) Quit / Exit

    This is where the post-menu text is displayed, if any.

    Enter choice (prompt text): 1

    ================================================================================

    The user is guaranteed that only valid choices are returned.  Choices can be
    case-insensitive (default).
    """


    def __init__(self, title=None, prompt="Enter Choice", prompt_suffix=": ",
                 case_sensitive=False, pre_menu_text=None, post_menu_text=None):
        if case_sensitive:
            self.set_case_sensitive()
        else:
            self.set_case_insensitive()
        self.set_title(title)
        self.set_pre_menu_text(pre_menu_text)
        self.set_post_menu_text(post_menu_text)
        self.set_prompt(prompt)
        self.set_prompt_suffix(prompt_suffix)
        self._menu_options = collections.OrderedDict()
        self._menu_text = ""
        self._rebuild_menu = True


    def prompt_user_for_choice(self):
        """
        Display the menu and prompt for the user's choice
        """
        if self._rebuild_menu:
            self._build_menu_text()
        print(self._menu_text, end="")
        choice = prompt_utils.prompt_user_for_choice(self._prompt,
                                                     self._menu_options.keys(),
                                                     case_sensitive=self._case_sensitive,
                                                     prompt_suffix=self._prompt_suffix)
        return choice


    def prompt_user_for_choices(self, separator=","):
        """
        Display the menu, and take a user's comma-separated choices
        """
        if self._rebuild_menu:
            self._build_menu_text()
        print(self._menu_text, end="")
        choices = prompt_utils.prompt_user_for_choices(self._prompt,
                                                       self._menu_options.keys(),
                                                       separator=separator,
                                                       case_sensitive=self._case_sensitive,
                                                       prompt_suffix=self._prompt_suffix)
        return choices


    def _build_menu_text(self):
        """
        Method to pre-build the menu text.
        """
        self._menu_text = ""
        if self._title is not None:
            self._menu_text = (self._menu_text + string_utils.underline(self._title) + "\n\n")
        if self._pre_menu_text is not None:
            self._menu_text = (self._menu_text + self._pre_menu_text + "\n\n")
        for key, description in self._menu_options.items():
            self._menu_text = (self._menu_text + key + ") " + description + "\n")
        if len(self._menu_options) > 0:
            self._menu_text = (self._menu_text + "\n")
        if self._post_menu_text is not None:
            self._menu_text = (self._menu_text + self._post_menu_text + "\n\n")
        self._rebuild_menu = False


    def add_menu_option(self, key, description):
        """
        Add a menu option by key, and the description to display.
        """
        self._menu_options[key] = description
        self._rebuild_menu = True


    def set_title(self, title):
        """
        Set the title of the menu.  Set to None if this is not needed.
        """
        self._title = title
        self._rebuild_menu = True


    def set_pre_menu_text(self, pre_menu_text):
        """
        Set the pre-menu text.  Set to None if this is not needed.
        """
        self._pre_menu_text = pre_menu_text
        self._rebuild_menu = True


    def set_post_menu_text(self, post_menu_text):
        """
        Set the post-menu text.  Set to None if this is not needed.
        """
        self._post_menu_text = post_menu_text
        self._rebuild_menu = True


    def set_case_insensitive(self):
        """
        Set the menu options to be case-insensitive.
        """
        self._case_sensitive = False


    def set_case_sensitive(self):
        """
        Set the menu options to be case-insensitive.
        """
        self._case_sensitive = True


    def set_prompt(self, prompt):
        """
        Set the prompt text.
        """
        self._prompt = prompt


    def set_prompt_suffix(self, prompt_suffix):
        """
        Set the prompt suffix.
        """
        self._prompt_suffix = prompt_suffix
