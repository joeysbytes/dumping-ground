"""
This module holds utilities for working with archive files.
"""

from pathlib import PurePath
import subprocess

_VALID_COMPRESSION_EXTENSIONS = (
    ".rar",
    ".7z",
    ".zip"
)


def is_compressed_file(file_name):
    """
    Return true/false if file name has a valid archive extension
    """
    path_name = PurePath(file_name)
    extension = path_name.suffix
    if extension in _VALID_COMPRESSION_EXTENSIONS:
        return True
    return False


def _uncompress_rar(file_name, output_directory):
    """
    Given a rar file and output directory, uncompress the file.
    Both values are strings.
    """
    # will create missing directories if output directory ends in /, otherwise errors out
    cmd = ["unrar", "x", file_name, output_directory]
    subprocess.run(cmd, shell=False)


def _uncompress_7z(file_name, output_directory):
    """
    Given a 7z file and output directory, uncompress the file.
    Both values are strings.
    """
    # -o flag will create missing directories automatically, does not matter if ending in /
    cmd = ["7z", "x", "-o" + output_directory, file_name]
    subprocess.run(cmd, shell=False)


def _uncompress_zip(file_name, output_directory):
    """
    Given a zip file and output directory, uncompress the file.
    Both values are strings.
    """
    # does not create missing directories, errors instead
    cmd = ["unzip", file_name, "-d", output_directory]
    subprocess.run(cmd, shell=False)


def uncompress_file(file_name, output_directory):
    """
    Given a file, uncompress it to the given output directory
    Both values are strings.
    """
    path_name = PurePath(file_name)
    extension = path_name.suffix
    if extension == ".rar":
        _uncompress_rar(file_name, output_directory)
    elif extension == ".7z":
        _uncompress_7z(file_name, output_directory)
    elif extension == ".zip":
        _uncompress_zip(file_name, output_directory)
