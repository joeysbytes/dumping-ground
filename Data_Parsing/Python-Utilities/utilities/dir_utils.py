"""
This module contains methods for working with directories.
All input parameters are considered to be strings, not Path objects.
"""

from pathlib import Path
from pathlib import PurePath
from utilities import prompt_utils as prompt_utils
from utilities import file_utils as file_utils

def directory_exists(directory):
    """
    Check if a directory exists.
    """
    path_name = Path(directory)
    return (path_name.exists()) and (path_name.is_dir())


def create_directory(directory):
    """
    Create a directory, including all parents
    """
    path_name = Path(directory)
    path_name.mkdir(parents=True)


def rename_directory(original_directory, new_directory, overwrite_file=False):
    """
    Given a directory, rename it.
    """
    if (not directory_exists(original_directory)) or (directory_exists(new_directory)):
        return
    if not overwrite_file:
        if file_utils.file_exists(new_directory):
            return
    orig_path_name = Path(original_directory)
    new_path_name = Path(new_directory)
    orig_path_name.rename(new_path_name)


def append_separator(directory):
    """
    Given a directory string, add the file path separator to the end if it is not there.
    """
    if not directory.endswith("/"):
        return directory + "/"
    return directory


def get_parent(directory):
    """
    Given a directory, return the parent directory
    """
    directory_path = PurePath(directory)
    return str(directory_path.parent)

def get_list_of_directories(base_directory):
    """
    Return a list of directories within the base directory.
    """
    return file_utils.get_list_of_files(base_directory,
                                        include_files=False,
                                        include_directories=True)


def prompt_user_for_directory(creation_allowed=False):
    """
    Prompt a user for a directory, return the string for it.abs
    If the directory does not exist, and creation is allowed, create the directory
    if the user chooses so.
    The returned directory always ends with a /
    """
    verified_directory = None
    while verified_directory is None:
        user_directory = prompt_utils.prompt_user_for_text("Enter directory path",
                                                           prompt_on_next_line=True)
        if not directory_exists(user_directory):
            if creation_allowed:
                msg = "Directory not found, do you wish to create it"
                create_dir = prompt_utils.prompt_user_for_yes_or_no(msg)
                if create_dir:
                    print("Creating directory: " + user_directory + "\n")
                    create_directory(user_directory)
                    verified_directory = user_directory
            else:
                print("\n*** Directory not found\n")
        else:
            verified_directory = user_directory
    verified_directory = append_separator(verified_directory)
    return verified_directory


def prompt_user_for_new_directory(base_directory=".", overwrite_file=False):
    """
    Prompt the user for a directory which does not already exist under the
    base directory.
    """
    base_directory = append_separator(base_directory)
    verified_directory = None
    while verified_directory is None:
        user_directory = prompt_utils.prompt_user_for_text("Enter new directory path",
                                                           prompt_on_next_line=True)
        full_user_directory = (base_directory + user_directory)
        if directory_exists(full_user_directory):
            print("\n*** Directory already exists\n")
        elif (not overwrite_file) and (file_utils.file_exists(full_user_directory)):
            print("\n*** File exists with that name\n")
        else:
            verified_directory = append_separator(full_user_directory)
    return verified_directory
