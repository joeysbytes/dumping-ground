"""
This module contains methods for working with files.
All input parameters are considered to be strings, not Path objects.
"""

from shutil import copy2
from pathlib import Path
from utilities import dir_utils as dir_utils
from utilities import prompt_utils as prompt_utils

def file_exists(file_name):
    """
    Return true/false if a given file exists
    """
    path_name = Path(file_name)
    return (path_name.exists()) and (path_name.is_file())


def copy_file(source_file, destination_file):
    """
    Copy source file name to destination file name
    """
    copy2(source_file, destination_file)


def delete_file(file_name):
    """
    Deletes a file.
    """
    path_name = Path(file_name)
    path_name.unlink()


def get_list_of_files(directory, include_files=True, include_directories=True):
    """
    Return an array of files.  If directories are included, they will be at
    the top of the list, ending in a /
    """
    path_name = Path(directory)
    file_list = sorted(path_name.glob("*"))
    file_array = []
    # get directories
    if include_directories:
        for file_item in file_list:
            dir_name = str(file_item)
            if dir_utils.directory_exists(dir_name):
                file_array.append(dir_name + "/")
    # get files
    if include_files:
        for file_item in file_list:
            file_name = str(file_item)
            if file_exists(file_name):
                file_array.append(file_name)
    return file_array


def print_list_of_files(directory, include_directories=True):
    """
    Gets a list of files and prints it to the screen.
    """
    file_list = get_list_of_files(directory, include_directories=include_directories)
    for file_item in file_list:
        print(file_item)


def prompt_for_existing_file(directory="."):
    """
    Prompt user for an existing file within a given directory.
    If any error occurs, None is returned
    """
    try:
        directory = dir_utils.append_separator(directory)
        user_file_name = None
        while user_file_name is None:
            file_name = prompt_utils.prompt_user_for_text("Enter file name")
            full_file_name = (directory + file_name)
            if file_exists(full_file_name):
                user_file_name = full_file_name
            else:
                print("*** File not found\n")
        return user_file_name
    except:
        return None

