"""
This module holds methods for prompting the user for input.
"""


def is_valid_choice(choice, valid_choices, case_sensitive=False):
    """
    Given a choice, an array of valid choices, and optionally whether the choice
    has to be case-sensitive, check if the choice is valid (returns True or False).
    An empty valid choices list will return False.
    An empty choice is treated as a regular choice, assuming an empty valid choice exists.
    """
    # If any inputs are None or empty, return False
    if ((choice is None) or
            (valid_choices is None) or
            (len(valid_choices) == 0)):
        return False
    # More efficient if case_sensitive search
    if case_sensitive:
        return choice in valid_choices
    # Compare upper-case version of choice to valid choices list in upper-case
    upper_case_choice = choice.upper()
    for valid_choice in valid_choices:
        if upper_case_choice == valid_choice.upper():
            return True
    return False


def is_valid_text(text, empty_text_allowed=False):
    """
    Given text, see if it is empty, and whether it is allowed to be.
    A value of None will return False.
    """
    if text is None:
        return False
    if empty_text_allowed:
        return True
    elif len(text) == 0:
        return False
    else:
        return True


def _build_prompt_msg(prompt, prompt_suffix=": ", prompt_on_next_line=False):
    """
    Build the prompt to display to the user.
    """
    if prompt is None:
        prompt = ""
    prompt_msg = prompt
    if prompt_suffix is not None:
        prompt_msg = (prompt_msg + prompt_suffix)
    if prompt_on_next_line:
        prompt_msg = (prompt_msg.strip() + "\n")
    return prompt_msg


def prompt_user_for_choice(prompt, valid_choices, case_sensitive=False, prompt_suffix=": "):
    """
    Given a prompt message, get user input and compare it against a list of valid choices.
    This method is guaranteed to only return valid values.
    If the choice is not case_sensitive, it will be returned in upper-case.
    """
    prompt_msg = _build_prompt_msg(prompt, prompt_suffix=prompt_suffix, prompt_on_next_line=False)
    valid_choice = False
    user_choice = None
    while not valid_choice:
        choice = None
        choice_input = input(prompt_msg)
        print()
        if choice_input is not None:
            choice = choice_input.strip()
        valid_choice = is_valid_choice(choice,
                                       valid_choices,
                                       case_sensitive=case_sensitive)
        if not valid_choice:
            if len(choice) == 0:
                print("*** ERROR: NOTHING ENTERED ***\n")
            else:
                print("*** ERROR: INVALID CHOICE: " + valid_choice + "\n")
        else:
            if case_sensitive:
                user_choice = choice
            else:
                user_choice = choice.upper()
    return user_choice


def prompt_user_for_choices(prompt, valid_choices, separator=",",
                            case_sensitive=False, prompt_suffix=": "):
    """
    Given a prompt message, get user input and compare it against a list of valid choices.
    This method is guaranteed to only return valid values in an array.
    Multiple values can be selected by using the separator.
    If the choice is not case_sensitive, it will be returned in upper-case.
    """
    prompt_msg = _build_prompt_msg(prompt, prompt_suffix=prompt_suffix, prompt_on_next_line=False)
    valid_choice = False
    user_choices = []
    while not valid_choice:
        seen = set()
        choices = None
        choices_input = input(prompt_msg)
        print()
        if choices_input is not None:
            choices = choices_input.split(separator)
        for choice in choices:
            choice = choice.strip()
            if not case_sensitive:
                choice = choice.upper()
            valid_choice = is_valid_choice(choice,
                                           valid_choices,
                                           case_sensitive=case_sensitive)
            if not valid_choice:
                if len(choice) == 0:
                    print("*** ERROR: NOTHING ENTERED ***\n")
                else:
                    print("*** ERROR: INVALID CHOICE: " + choice + "\n")
                break
            elif choice in seen:
                # TODO: pass flag to only send back unique choices rather than error.
                print("*** ERROR: DUPLICATE ENTRY: " + choice + "\n")
                valid_choice = False
            else:
                seen.add(choice)
    for choice in choices:
        if case_sensitive:
            user_choices.append(choice.strip())
        else:
            user_choices.append(choice.strip().upper())
    return user_choices


def prompt_user_for_text(prompt, prompt_on_next_line=False, empty_text_allowed=False):
    """
    Get free-form text from a user.
    """
    valid_text = False
    text_input = None
    prompt_msg = _build_prompt_msg(prompt, prompt_on_next_line=prompt_on_next_line)
    while not valid_text:
        text_input = input(prompt_msg)
        print()
        valid_text = is_valid_text(text_input, empty_text_allowed=empty_text_allowed)
        if not valid_text:
            if len(text_input) == 0:
                print("*** ERROR: NOTHING ENTERED ***\n")
            else:
                print("*** ERROR: INVALID TEXT ***\n")
    return text_input


def prompt_user_for_yes_or_no(prompt):
    """
    This is a shortcut method to get the user choice for a Yes/No question.
    Yes will be converted to True, No will be converted to False.
    """
    valid_choices = ("Y", "N", "Yes", "No")
    user_choice = prompt_user_for_choice(prompt, valid_choices, prompt_suffix=(" (Yes/No)? "))
    if (user_choice == "Y") or (user_choice == "YES"):
        return True
    return False
