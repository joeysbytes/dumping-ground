"""
This module holds methods for working with strings.
"""


def line(length, chars="-"):
    """
    Returns a line of a certain length built from the provided chars.
    If the input is bad, None is returned.
    """
    try:
        if (length < 1) or (len(chars) < 1):
            return None
        line_of_chars = chars * length
        return line_of_chars[:length]
    except: # any exception will be invalid input
        return None


def underline(text, chars="-"):
    """
    Returns text with a newline, and an underline of equal length of characters
    If the input is bad, the original text is returned.
    """
    try:
        line_of_chars = line(len(text), chars=chars)
        return text + "\n" + line_of_chars
    except: # any exception will be invalid input
        return text
