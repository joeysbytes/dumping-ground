#!/bin/bash

PAUSE_IN_SECONDS=7

BGG_DATA_DIR="/data/bgg_data"

BGG_TSV_DIR="${BGG_DATA_DIR}/tsv"
BGG_OWN_TSV="${BGG_TSV_DIR}/bgg_own.tsv"
BGG_ANDROID_NETRUNNER_TSV="${BGG_TSV_DIR}/bgg_android_netrunner.tsv"
BGG_ID_LIST="${BGG_TSV_DIR}/bgg_id_list.txt"

BGG_XML_DIR="${BGG_DATA_DIR}/xml"
BGG_DB_DIR="${BGG_DATA_DIR}/sqlite3"
BGG_DB="${BGG_DB_DIR}/bgg.db"


function main {
    echo "START: BGG Download Script"
    build_data_directories
    download_tsv_files
    build_id_list
    sort_id_list
    download_bgg_thing_ids
    build_expansion_id_list
    sort_id_list
    download_bgg_thing_ids
    load_bgg_database
    echo "END: BGG Download Script"
}


function download_bgg_thing_ids {
    echo "STEP: Download BGG IDs"
    local bgg_url_base="https://www.boardgamegeek.com/xmlapi2/thing?id="
    while read bgg_id
    do
        local bgg_xml_file="${BGG_XML_DIR}/${bgg_id}.xml"
        if [ ! -e "${bgg_xml_file}" ]
        then
            pause
            wget --output-document="${BGG_XML_DIR}/${bgg_id}.xml" "${bgg_url_base}${bgg_id}"
        else
            echo "INFO: Already downloaded: ${bgg_xml_file}"
        fi
    done<"${BGG_ID_LIST}"
}


function pause {
    local date_time=`date +"%Y-%m-%d %H:%M:%S"`
    echo "[ ${date_time} ] Pausing for ${PAUSE_IN_SECONDS} seconds..."
    sleep "${PAUSE_IN_SECONDS}"
}


function build_id_list {
    echo "STEP: Building BGG ID List"
    touch "${BGG_ID_LIST}"
    cp /dev/null "${BGG_ID_LIST}"
    cat "${BGG_OWN_TSV}"|cut -f1 -d$'\t'>>"${BGG_ID_LIST}"
    cat "${BGG_ANDROID_NETRUNNER_TSV}"|cut -f1 -d$'\t'>>"${BGG_ID_LIST}"
}


function build_expansion_id_list {
    echo "STEP: Building BGG Expansion ID List"
    touch "${BGG_ID_LIST}"
    cp /dev/null "${BGG_ID_LIST}"
    python3 ./get_bgg_expansion_ids.py "${BGG_XML_DIR}">>"${BGG_ID_LIST}"
}


function load_bgg_database {
    echo "STEP: Loading BGG data in to database"
    cp /dev/null "${BGG_DB}"
    python3 ./load_bgg_database.py "${BGG_TSV_DIR}" "${BGG_XML_DIR}" "${BGG_DB}"
}


function sort_id_list {
    echo "STEP: Sorting BGG ID List"
    mv "${BGG_ID_LIST}" "${BGG_ID_LIST}.tmp"
    cat "${BGG_ID_LIST}.tmp"|grep "^[0-9]"|sort --numeric-sort --unique>"${BGG_ID_LIST}"
    rm "${BGG_ID_LIST}.tmp"
}


function build_data_directories {
    echo "STEP: Building data directories"
    mkdir -pv "${BGG_TSV_DIR}"
    mkdir -pv "${BGG_XML_DIR}"
    mkdir -pv "${BGG_DB_DIR}"
}


function download_tsv_files {
    echo "STEP: Download board game TSV files"
    local google_base_url="https://docs.google.com/spreadsheets/d/e/2PACX-1vTzLJTon7mDgy3FW-sdbXtIwPazwyl_leMbTrKsvWp_mLJkG2RNelBpXeoMMI7sZOrp669V15ZY0neA"
    if [ ! -e "${BGG_OWN_TSV}" ]
    then
        wget --output-document="${BGG_OWN_TSV}" "${google_base_url}/pub?gid=0&single=true&output=tsv"
    else
        echo "INFO: Already downloaded: ${BGG_OWN_TSV}"
    fi
    if [ ! -e "${BGG_ANDROID_NETRUNNER_TSV}" ]
    then
        wget --output-document="${BGG_ANDROID_NETRUNNER_TSV}" "${google_base_url}/pub?gid=1625125561&single=true&output=tsv"
    else
        echo "INFO: Already downloaded: ${BGG_ANDROID_NETRUNNER_TSV}"
    fi
}


main
