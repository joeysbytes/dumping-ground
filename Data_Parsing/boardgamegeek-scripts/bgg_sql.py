CREATE_USER_THING_TABLE = """
    CREATE TABLE USER_THING
    ( USER_THING_ID INTEGER,
      BGG_THING_ID INTEGER,
      NAME TEXT,
      EXPANSION_CODE TEXT,
      RATING INTEGER,
      NOTES TEXT,
      STATUS TEXT,
      PRIMARY KEY (USER_THING_ID) )
"""

INSERT_USER_THING_RECORD = """
    INSERT INTO USER_THING
        ( BGG_THING_ID, NAME, EXPANSION_CODE, RATING, NOTES, STATUS )
    VALUES
        ( :bgg_thing_id, :name, :expansion_code, :rating, :notes, :status )
"""

CREATE_BGG_THING_TABLE = """
    CREATE TABLE BGG_THING
    ( BGG_THING_ID INTEGER,
      NAME TEXT,
      TYPE TEXT,
      YEAR_PUBLISHED INTEGER,
      MIN_PLAYERS INTEGER,
      MAX_PLAYERS INTEGER,
      PLAY_TIME INTEGER,
      MIN_PLAY_TIME INTEGER,
      MAX_PLAY_TIME INTEGER,
      AGE INTEGER,
      THUMBNAIL_URL TEXT,
      IMAGE_URL TEXT,
      DESCRIPTION TEXT,
      PRIMARY KEY (BGG_THING_ID) )
"""

INSERT_BGG_THING_RECORD = """
    INSERT INTO BGG_THING
        ( BGG_THING_ID, NAME, TYPE, YEAR_PUBLISHED, MIN_PLAYERS,
          MAX_PLAYERS, PLAY_TIME, MIN_PLAY_TIME, MAX_PLAY_TIME, AGE, THUMBNAIL_URL,
          IMAGE_URL, DESCRIPTION )
    VALUES
        ( :thing_id, :name, :type, :year_published, :min_players,
          :max_players, :play_time, :min_play_time, :max_play_time, :age, :thumbnail_url,
          :image_url, :description )
"""

CREATE_BGG_THING_ALTERNATE_NAME_TABLE = """
    CREATE TABLE BGG_THING_ALTERNATE_NAME
    ( BGG_THING_ID INTEGER,
      ALTERNATE_NAME TEXT,
      TYPE TEXT,
      SORT_INDEX INTEGER,
      PRIMARY KEY (BGG_THING_ID, ALTERNATE_NAME) );
"""

# Sometimes the XML has duplicate rows (100423, for example)
INSERT_BGG_THING_ALTERNATE_NAME_RECORD = """
    INSERT OR IGNORE INTO BGG_THING_ALTERNATE_NAME
        ( BGG_THING_ID, ALTERNATE_NAME, TYPE, SORT_INDEX )
    VALUES
        ( :thing_id, :alternate_name, :type, :sort_index )
"""

CREATE_BGG_THING_LINK_TABLE = """
    CREATE TABLE BGG_THING_LINK
    ( BGG_THING_ID INTEGER,
      LINK_ID INTEGER,
      TYPE TEXT,
      VALUE TEXT,
      PRIMARY KEY (BGG_THING_ID, LINK_ID, TYPE) );
"""

# Sometimes the XML has duplicate rows (158957, for example)
INSERT_BGG_THING_LINK_RECORD = """
    INSERT OR IGNORE INTO BGG_THING_LINK
        ( BGG_THING_ID, LINK_ID, TYPE, VALUE )
    VALUES
        ( :thing_id, :link_id, :type, :value )
"""