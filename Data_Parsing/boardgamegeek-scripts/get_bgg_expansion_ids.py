import sys
import argparse
from pathlib import Path
import xml.etree.ElementTree as ET

bgg_expansion_ids = list()


def main():
    bgg_things_dir = init()
    bgg_things_files = sorted(Path(bgg_things_dir).glob('*.xml'))
    for bgg_thing_file in bgg_things_files:
        process_bgg_thing_xml(bgg_thing_file)
    for bgg_expansion_id in bgg_expansion_ids:
        print(bgg_expansion_id)


def init():
    parser = argparse.ArgumentParser(description="Extract BGG Expansion IDs from a directory.")
    parser.add_argument('bgg_things_dir', help="Directory of BGG Thing XML files")
    args = parser.parse_args()
    bgg_things_dir = Path(args.bgg_things_dir)
    if not (bgg_things_dir.exists() and bgg_things_dir.is_dir()):
        print("BGG Things directory not found: " + args.bgg_things_dir, file=sys.stderr)
        quit(1)
    return args.bgg_things_dir


def process_bgg_thing_xml(bgg_thing_file):
    tree = ET.parse(bgg_thing_file)
    root = tree.getroot()
    bgg_expansions = root.findall("./item//link[@type='boardgameexpansion']")
    for bgg_expansion in bgg_expansions:
        bgg_expansion_ids.append(bgg_expansion.get("id"))
    bgg_integrations = root.findall("./item//link[@type='boardgameintegration']")
    for bgg_integration in bgg_integrations:
        bgg_expansion_ids.append(bgg_integration.get("id"))


main()