import sys
import argparse
from pathlib import Path
import xml.etree.ElementTree as ET
import sqlite3
import bgg_sql


commit_on_count = 50
db_connection = None
user_things_dir = None
bgg_things_dir = None
bgg_db_filename = None


def main():
    global db_connection, user_things_dir, bgg_things_dir, bgg_db_filename
    # Initialize
    user_things_dir, bgg_things_dir, bgg_db_filename = init()
    db_connection = sqlite3.connect(bgg_db_filename)
    init_database()
    # Load User Things
    load_user_things(user_things_dir + '/bgg_own.tsv', 'own')
    load_user_things(user_things_dir + '/bgg_android_netrunner.tsv', 'own', android_netrunner=True)
    # Load BGG Things
    load_bgg_things()
    # Finalize
    db_connection.close()


################################################################################
# Initialization Steps
################################################################################

def init():
    parser = argparse.ArgumentParser(description="Load BGG Database")
    parser.add_argument('user_things_dir', help="Directory of User Thing TSV files")
    parser.add_argument('bgg_things_dir', help="Directory of BGG Thing XML files")
    parser.add_argument('bgg_db_filename', help="Database file for BGG data")
    args = parser.parse_args()
    user_things_dir = Path(args.user_things_dir)
    if not (user_things_dir.exists() and user_things_dir.is_dir()):
        print("User Things directory not found: " + args.user_things_dir, file=sys.stderr)
        quit(1)
    bgg_things_dir = Path(args.bgg_things_dir)
    if not (bgg_things_dir.exists() and bgg_things_dir.is_dir()):
        print("BGG Things directory not found: " + args.bgg_things_dir, file=sys.stderr)
        quit(1)
    bgg_db_filename = Path(args.bgg_db_filename)
    if not (bgg_db_filename.exists() and bgg_db_filename.is_file()):
        print("BGG database file not found: " + args.bgg_db_filename, file=sys.stderr)
        quit(1)
    return args.user_things_dir, args.bgg_things_dir, args.bgg_db_filename


def init_database():
    db_connection.execute(bgg_sql.CREATE_USER_THING_TABLE)
    db_connection.execute(bgg_sql.CREATE_BGG_THING_TABLE)
    db_connection.execute(bgg_sql.CREATE_BGG_THING_ALTERNATE_NAME_TABLE)
    db_connection.execute(bgg_sql.CREATE_BGG_THING_LINK_TABLE)
    db_connection.commit()


################################################################################
# Load BGG Data To Database
################################################################################

def load_bgg_things():
    bgg_things_files = sorted(Path(bgg_things_dir).glob('*.xml'))
    commit_count = 0
    for bgg_thing_file in bgg_things_files:
        _load_bgg_thing_file(bgg_thing_file)
        commit_count += 1
        if commit_count % commit_on_count == 0:
            db_connection.commit()
    db_connection.commit()


def _load_bgg_thing_file(bgg_thing_file):
    tree = ET.parse(bgg_thing_file)
    root = tree.getroot()
    bgg_thing_id = _insert_bgg_thing_record(root)
    print("Loading BGG Thing Id: " + bgg_thing_id)
    _insert_bgg_thing_alternate_names(root, bgg_thing_id)
    _insert_bgg_links(root, bgg_thing_id)


def _insert_bgg_thing_record(root):
    thing_type = _get_xml_value(root, "./item", attribute="type")
    thing_id = _get_xml_value(root, "./item", attribute="id")
    thing_name = _get_xml_value(root, "./item/name[@type='primary']", attribute="value")
    thing_thumbnail_url = _get_xml_value(root, "./item/thumbnail")
    thing_image_url = _get_xml_value(root, "./item/image")
    thing_description = _get_xml_value(root, "./item/description")
    thing_min_players = _get_xml_value(root, "./item/minplayers", attribute="value")
    thing_max_players = _get_xml_value(root, "./item/maxplayers", attribute="value")
    thing_playing_time = _get_xml_value(root, "./item/playingtime", attribute="value")
    thing_min_playing_time = _get_xml_value(root, "./item/minplaytime", attribute="value")
    thing_max_playing_time = _get_xml_value(root, "./item/maxplaytime", attribute="value")
    thing_age = _get_xml_value(root, "./item/minage", attribute="value")
    thing_year_published = _get_xml_value(root, "./item/yearpublished", attribute="value")
    db_connection.execute(bgg_sql.INSERT_BGG_THING_RECORD,
                          {"thing_id": thing_id, "name": thing_name, "type": thing_type,
                           "year_published": thing_year_published, "min_players": thing_min_players,
                           "max_players": thing_max_players, "play_time": thing_playing_time,
                           "min_play_time": thing_min_playing_time, "max_play_time": thing_max_playing_time,
                           "age": thing_age, "thumbnail_url": thing_thumbnail_url,
                           "image_url": thing_image_url, "description": thing_description})
    return thing_id


def _insert_bgg_thing_alternate_names(root, bgg_thing_id):
    names = root.findall("./item/name")
    for name in names:
        alt_name_type = name.get("type")
        alt_name_sort_index = name.get("sortindex")
        alt_name = name.get("value")
        db_connection.execute(bgg_sql.INSERT_BGG_THING_ALTERNATE_NAME_RECORD,
                              {"thing_id": bgg_thing_id, "alternate_name": alt_name,
                               "type": alt_name_type, "sort_index": alt_name_sort_index})


def _insert_bgg_links(root, bgg_thing_id):
    links = root.findall("./item/link")
    for link in links:
        link_type = link.get("type")
        link_id = link.get("id")
        link_value = link.get("value")
        db_connection.execute(bgg_sql.INSERT_BGG_THING_LINK_RECORD,
                              {"thing_id": bgg_thing_id, "link_id": link_id,
                               "type": link_type, "value": link_value})


def _get_xml_value(root, xpath, attribute=None):
    value = None
    element = root.find(xpath)
    if element is not None:
        if attribute == None:
            value = element.text
        else:
            value = element.get(attribute)
    return value


################################################################################
# Load User Data To Database
################################################################################

def load_user_things(user_things_file, status, android_netrunner=False):
    commit_count = 0
    first_record = True
    with open(user_things_file) as f:
        for data_line in f:
            if data_line == "":
                break
            elif first_record:
                first_record = False
            elif not _is_user_data_line_empty(data_line):
                 _load_user_thing(data_line, status, android_netrunner)
                 commit_count += 1
                 if commit_count % commit_on_count == 0:
                     db_connection.commit()
    db_connection.commit()


def _load_user_thing(data_line, status, android_netrunner):
    data_line_parts = data_line.split('\t')
    bgg_thing_id = data_line_parts[0].strip()
    print("Loading User BGG Thing Id: " + bgg_thing_id)
    user_name = None
    expansion_code = None
    user_rating = None
    user_notes = None
    if android_netrunner:
        cycle = data_line_parts[2].strip()
        if cycle == "Main Game":
            user_name = "Android: Netrunner"
        else:
            user_name = "Android: Netrunner: " + data_line_parts[1].strip()
            expansion_code = "x"
            if cycle == "Deluxe Expansion":
                user_notes = cycle
            else:
                user_notes = data_line_parts[2].strip() + ": " + data_line_parts[3].strip() + " - " + data_line_parts[4].strip()
    else:
        user_name = data_line_parts[1].strip()
        expansion_code = data_line_parts[2].strip()
        user_rating = data_line_parts[3].strip()
        user_notes = data_line_parts[4].strip()
    db_connection.execute(bgg_sql.INSERT_USER_THING_RECORD,
                          {"bgg_thing_id": bgg_thing_id, "name": user_name, "expansion_code": expansion_code,
                           "rating": user_rating, "notes": user_notes, "status": status})


def _is_user_data_line_empty(data_line):
    data_line_empty = True
    trimmed_data_line = data_line.strip()
    if len(trimmed_data_line) == 0:
        return data_line_empty
    data_line_parts = trimmed_data_line.split('\t')
    for data_line_part in data_line_parts:
        if len(data_line_part.strip()) != 0:
            data_line_empty = False
            break
    return data_line_empty


main()
