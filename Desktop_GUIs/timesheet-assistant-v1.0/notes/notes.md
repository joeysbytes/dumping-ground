Startup for dev:
* cd /data/joey/development/timesheet-assistant/src
* conda activate timesheet-assistant
* wxglade.sh

* How to replace methods in a class, or an instance:
  * https://filippo.io/instance-monkey-patching-in-python/

wxGlade gotchas:
* When generating multiple files instead of 1 inside a folder, it gets the import wrong
* If you generate multiple files, the app file name is taken from the name field instead of the class field

LibreOffice Calc 6.0 Functions Help: https://help.libreoffice.org/6.0/en-US/text/scalc/01/04060100.html

