import wx
from gui.TimesheetAssistantGuiApp import TimesheetAssistantGuiApp
from form_controls.TimesheetAssistantFrame import TimesheetAssistantFrame

class TimesheetAssistant(TimesheetAssistantGuiApp):

    def __init__(self, *args, **kwds):
        super().__init__(*args, **kwds)


    def OnInit(self):
        self.timesheet_assistant_frame = TimesheetAssistantFrame(None, wx.ID_ANY, "")
        self.SetTopWindow(self.timesheet_assistant_frame)
        self.timesheet_assistant_frame.Show()
        return True


if __name__ == "__main__":
    timesheet_assistant = TimesheetAssistant(0)
    timesheet_assistant.MainLoop()
