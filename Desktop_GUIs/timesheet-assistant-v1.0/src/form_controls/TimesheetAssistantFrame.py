import wx
from gui.TimesheetAssistantGuiApp import TimesheetAssistantGuiFrame
from pathlib import Path
from form_controls.utils import *

class TimesheetAssistantFrame(TimesheetAssistantGuiFrame):

    def __init__(self, *args, **kwds):
        super().__init__(*args, **kwds)


    def browse_files_button_clicked(self, event):
        file_name = wx.FileSelector(message="Select a Timesheet Excel File", wildcard="Excel Files (*.xlsx)|*.xlsx")
        if file_name.strip():
            self.file_name_text_ctrl.SetValue(file_name)
        event.Skip()


    def load_file_button_clicked(self, event):
        file_name = self.file_name_text_ctrl.GetValue()
        path = Path(file_name)
        if file_name.strip() and path.exists():
            self.file_not_found_static_text.Hide()
        else:
            self.file_not_found_static_text.Show()
        self.file_not_found_sizer.RecalcSizes()
        event.Skip()


    def view_date_changed(self, event):  # wxGlade: TimesheetAssistantGuiFrame.<event_handler>
        view_date_time = wxdatetime_to_pydatetime(self.view_datepicker_ctrl.GetValue())
        view_start_date = str(view_date_time.month) + "/" + str(view_date_time.day) + "/" + str(view_date_time.year)
        self.view_start_date_label.SetLabel(view_start_date)
        self.view_dates_sizer.RecalcSizes
        self.Layout()
        event.Skip()

