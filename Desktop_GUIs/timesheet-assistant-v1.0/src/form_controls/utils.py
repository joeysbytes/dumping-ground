from datetime import datetime
import wx

def wxdatetime_to_pydatetime(wx_datetime):
    py_datetime = datetime(wx_datetime.GetYear(),
                           wx_datetime.GetMonth() + 1,
                           wx_datetime.GetDay(),
                           hour=wx_datetime.GetHour(),
                           minute=wx_datetime.GetMinute(),
                           second=wx_datetime.GetSecond(),
                           microsecond=wx_datetime.GetMillisecond() * 1000)
    return py_datetime
