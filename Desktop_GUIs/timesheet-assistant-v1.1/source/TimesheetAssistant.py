import wx
from wxglade.TimesheetAssistant import TimesheetAssistantApp
from gui.Frame import Frame

class TimesheetAssistant(TimesheetAssistantApp):

    def __init__(self, *args, **kwds):
        super().__init__(*args, **kwds)


    def OnInit(self):
        self.frame = Frame(None, wx.ID_ANY, "")
        self.SetTopWindow(self.frame)
        self.frame.Show()
        return True


if __name__ == "__main__":
    timesheet_assistant = TimesheetAssistant(0)
    timesheet_assistant.MainLoop()
