import wx
from wxglade.TimesheetAssistant import AboutTimesheetAssistantFrame
from config.version import *

class AboutFrame(AboutTimesheetAssistantFrame):

    def __init__(self, *args, **kwds):
        super().__init__(*args, **kwds)
        self.version_label.SetLabelText(VERSION)
        self.build_date_label.SetLabelText(BUILD_DATE)


    def about_ok_button_clicked(self, event):
        self.Hide()
        self.GetParent().Enable()
        event.Skip()
