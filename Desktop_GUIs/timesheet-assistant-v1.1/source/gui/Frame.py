import wx
from wxglade.TimesheetAssistant import TimesheetAssistantFrame
from gui.AboutFrame import AboutFrame

class Frame(TimesheetAssistantFrame):

    def __init__(self, *args, **kwds):
        super().__init__(*args, **kwds)
        self.about_frame = AboutFrame(self, wx.ID_ANY, "")


    def file_quit_menu_item_selected(self, event):
        self.Close()



    def help_about_menu_item_selected(self, event):
        self.Disable()
        self.about_frame.Show()
        event.Skip()
