# Image to download and extract Caddy to
FROM debian:10 as caddy_build
RUN apt-get update && \
    apt-get --yes install wget && \
    mkdir /caddy && \
    mkdir /caddy/bin && \
    cd /caddy/bin && \
    wget --no-verbose -O caddy.tar.gz "https://caddyserver.com/download/linux/amd64?license=personal&telemetry=off" && \
    gunzip caddy.tar.gz && \
    tar -xf caddy.tar && \
    rm caddy.tar


# Runnable Caddy Image
FROM debian:10

LABEL maintainer="joey@joeysbytes.net"
LABEL description="Caddy Web Server"

RUN mkdir /caddy && \
    mkdir /caddy/bin && \
    mkdir /caddy/config && \
    mkdir /caddy/caddypath && \
    mkdir /caddy/logs && \
    mkdir /caddy/website

COPY --from=caddy_build "/caddy/bin/" "/caddy/bin/"

RUN setcap cap_net_bind_service=+ep /caddy/bin/caddy

WORKDIR /caddy/website

ENV CADDYPATH /caddy/caddypath
ENV CASE_SENSITIVE_PATH true

EXPOSE 80 443

VOLUME /caddy/config
VOLUME /caddy/caddypath
VOLUME /caddy/logs
VOLUME /caddy/website

# For production
CMD [ "/caddy/bin/caddy", "-agree", "-conf", "/caddy/config/Caddyfile", "-log", "/caddy/logs/caddy_process.log", "-log-roll-mb", "20", "-pidfile", "/caddy/logs/caddy.pid" ]

