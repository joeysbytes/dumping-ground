caddy_image_exists=$(docker images|grep "joeysbytes/caddy"|wc -l)
if [ ! "${caddy_image_exists}" == "0" ]
then
    docker rmi joeysbytes/caddy
fi

docker build \
    --tag joeysbytes/caddy \
    --file ./Dockerfile \
    --pull \
    --rm \
    --force-rm \
    .

docker rmi $(docker images -f "dangling=true" -q)

