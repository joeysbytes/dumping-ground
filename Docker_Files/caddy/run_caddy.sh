# TODO: add healthcheck

docker run \
    --name caddy \
    --publish 80:80 \
    --publish 443:443 \
    --volume /work/caddy/logs:/caddy/logs \
    --volume /data/caddy/config:/caddy/config \
    --volume /data/caddy/caddypath:/caddy/caddypath \
    --volume /data/website:/caddy/website \
    --detach --restart unless-stopped joeysbytes/caddy:latest

#    --interactive --tty --restart no --rm joeysbytes/caddy:latest /bin/bash
