jekyll_image_exists=$(docker images|grep "joeysbytes/jekyll"|wc -l)
if [ ! "${jekyll_image_exists}" == "0" ]
then
    docker rmi joeysbytes/jekyll
fi

docker build \
    --tag joeysbytes/jekyll \
    --file ./Dockerfile \
    --pull \
    --rm \
    --force-rm \
    .
