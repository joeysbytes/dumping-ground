#!/bin/bash

JEKYLL_DATA_DIR="/media/joey/JoeyUSB/development/joeysbytes/joeysbytes-net-website"

docker run \
    --name jekyll \
    --publish 80:4000 \
    --volume "${JEKYLL_DATA_DIR}":"/jekyll/data" \
    --interactive \
    --tty \
    joeysbytes/jekyll bash
