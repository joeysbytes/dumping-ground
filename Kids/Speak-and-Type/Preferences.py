class Preferences:

	DATA_FILE_DIRECTORY = "./data/"
	DATA_FILE_GLOB = DATA_FILE_DIRECTORY + "*.json"
	WINNER_FILE_GLOB = "./winner/*.txt"
	CASE_SENSITIVE = False
