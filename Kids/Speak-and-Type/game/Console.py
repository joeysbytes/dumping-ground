import sys
import tty
import termios

class Console:

	# return a single character from user, not echoed to screen
	# http://code.activestate.com/recipes/134892/
	# http://stackoverflow.com/questions/510357/python-read-a-single-character-from-the-user
	def getChar(self):
		fd = sys.stdin.fileno()
		old_settings = termios.tcgetattr(fd)
		try:
			tty.setraw(sys.stdin.fileno())
			ch = sys.stdin.read(1)
		finally:
			termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
		return ch

	def getText(self):
		return input()

	def stopIO(self):
		fdin = sys.stdin.fileno()
		fdout = sys.stdout.fileno()
		termios.tcflow(fdout, termios.TCOOFF)
		termios.tcflow(fdin, termios.TCIOFF)

	def startIO(self):
		fdin = sys.stdin.fileno()
		fdout = sys.stdout.fileno()
		termios.tcflow(fdin, termios.TCION)
		termios.tcflush(fdin, termios.TCIFLUSH)
		termios.tcflush(fdout, termios.TCOFLUSH)
		termios.tcflow(fdout, termios.TCOON)

	def out(self, text):
		print(text, end='', flush=True)

	def outln(self, text):
		print(text, flush=True)

	def clear(self):
		self.out(self.color()) # to set the background color
		self.out('\033[2J') # clear screen
		self.out('\033[0;0H') # move cursor to 0,0 (ln, col)

	def color(self, fg=7, bg=0):
		# BLACK, RED, GREEN, BROWN, BLUE, MAGENTA, CYAN, WHITE
		fg_colors = [ '30', '31', '32', '33', '34', '35', '36', '37' ]
		bg_colors = [ '40', '41', '42', '43', '44', '45', '46', '47' ]
		num_colors = len(fg_colors)
		ansi_str = '\033['
		if (fg >= num_colors):
			ansi_str += '1;' # bold on
			fg -= num_colors
		else:
			ansi_str += '0;' # attributes off
		ansi_str += fg_colors[fg] + ';' + bg_colors[bg] + 'm'
		return ansi_str

	def nextln(self, qty=1):
		for i in range(0, qty):
			self.outln(self.color())
