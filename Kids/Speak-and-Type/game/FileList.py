import glob
import random

class FileList:

	def __init__(self, file_glob):
		self.files = glob.glob(file_glob)
		self.files.sort()
		self.num_files = len(self.files)

	def getRandomFileText(self):
		file_num = random.randint(0, self.num_files - 1)
		f = open(self.files[file_num], 'r')
		data = f.read()
		f.close()
		return data

	def getNumFiles(self):
		return self.num_files

	def getBaseName(self, file_num):
		split1 = self.files[file_num].split(sep='/')
		split2 = split1[len(split1) - 1].split(sep='.')
		return split2[0]

	def getFileName(self, file_num):
		return self.files[file_num]
