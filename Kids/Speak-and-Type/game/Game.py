from game.JsonData import JsonData
from game.Console import Console
from Preferences import Preferences as Pref
from game.Speak import Speak
from game.FileList import FileList

class Game:

	def __init__(self, console, speak):
		self.console = console
		self.speak = speak
		self.winner_file_list = FileList(Pref.WINNER_FILE_GLOB)

	def __printTitle(self, title):
		c = self.console
		c.clear()
		title = self.data.getTitle()
		c.outln(c.color(11) + title)
		c.outln('-' * len(title))
		c.outln(c.color())
		self.speak.speak(self.data.getTitle(speak=True))

	def __keepPlaying(self):
		c = self.console
		s = self.speak
		color_text = (
			c.color(14) + 'Do you want to play again? (' +
			c.color(11) + 'Y' + c.color(14) + 'es or ' +
			c.color(11) + 'N' + c.color(14) + 'o): ')
		color_error_text = (
			c.color(9) + 'You did not press ' +
			c.color(11) + 'Y' + c.color(9) + ' or ' +
			c.color(11) + 'N')
		answer = '~'
		bad_answer = True
		while (bad_answer):
			c.nextln()
			c.out(color_text)
			s.speak('Do you want to play again? Yes or No')
			ans = c.getChar().upper()
			c.out(c.color() + ans)
			if ((ans == 'Y') or (ans == 'N')):
				answer = ans.upper()
				bad_answer = False
			else:
				c.nextln()
				c.outln(color_error_text)
				s.speak('You did not press Y or N')
		if (answer == 'Y'):
			return True
		else:
			c.nextln()
			return False

	def __playLine(self, line_num):
		c = self.console
		s = self.speak
		data = self.data
		words = data.getLine(line_num)
		speak_words = data.getLine(line_num, speak=True)
		num_words = data.getNumWords(line_num)
		c.out(c.color(0, 6))
		for word_num in range(0, num_words):
			c.out(words[word_num])
			s.speak(speak_words[word_num])
			if (word_num != num_words -1):
				c.out(' ')
		c.nextln()
		for word_num in range(0, num_words):
			last = False;
			if (word_num == num_words - 1):
				last = True
			do_quit = self.__playLine_word(words[word_num], speak_words[word_num], last_word=last)
			if (do_quit):
				return True
		c.nextln()
		c.nextln()
		return False

	def __playLine_word(self, word, speak_word, last_word=False):
		c = self.console
		s = self.speak
		if (last_word == False):
			word = word + ' '
		word_length = len(word)
		for position in range(0, word_length):
			mistyped_letter = True
			the_letter = word[position]
			while (mistyped_letter):
				letter = c.getChar()
				if (the_letter.upper() == letter.upper()):
					c.out(the_letter)
					mistyped_letter = False
				else:
					if (letter == '~'):
						return True
					else:
						s.speak('DING!')
		s.speak(speak_word)
		return False

	def __showWinner(self):
		c = self.console
		s = self.speak
		data = self.winner_file_list.getRandomFileText()
		split_data = data.split('\n')
		c.clear()
		for i in range(1, len(split_data)):
			c.outln(c.color(11) + split_data[i].rstrip())
		s.speak(split_data[0])

	def play(self, file_name):
		self.data = JsonData(file_name)
		keep_playing = False
		data = self.data
		self.__printTitle(data.getTitle())
		numLines = data.getNumLines()
		for line_num in range(0, data.getNumLines()):
			do_quit = self.__playLine(line_num)
			if (do_quit):
				return False
		self.__showWinner()
		keep_playing = self.__keepPlaying()
		return keep_playing
