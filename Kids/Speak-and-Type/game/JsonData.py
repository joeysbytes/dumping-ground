import json

class JsonData:

	def __init__(self, file_name):
		input_file = open(file_name, 'r')
		self.json_data = json.load(input_file)
		input_file.close()
		self.json_lines = len(self.json_data['contents'])

	def dump(self):
		print(self.json_data)

	def getNumLines(self):
		return self.json_lines

	def getTitle(self, speak=False):
		if (speak):
			return self.json_data['title']['speak']
		else:
			return self.json_data['title']['text']

	def getLine(self, line_num, speak=False):
		if (speak):
			return self.json_data['contents'][line_num]['speak']
		else:
			return self.json_data['contents'][line_num]['text']

	def getNumWords(self, line_num):
		return len(self.json_data['contents'][line_num]['text'])
