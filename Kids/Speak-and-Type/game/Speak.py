import subprocess
from game.Console import Console

class Speak:

	# espeak conversion for single characters
	# only have to enter in this table if you need to override the
	# default pronounciation.
	characters = {
		'Z' : 'ZEE',
		'.' : 'PERIOD',
		',' : 'COMMA',
		'<' : 'LESS THAN',
		'>' : 'GREATER THAN',
		';' : 'SEMI COLON',
		':' : 'COLON',
		'"' : 'QUOTES',
		"'" : 'QUOTE',
		'[' : 'LEFT BRACKET',
		']' : 'RIGHT BRACKET',
		'{' : 'LEFT BRACE',
		'}' : 'RIGHT BRACE',
		'\\' : 'BACK SLASH',
		'|' : 'PIPE',
		'~' : 'TIL DAY',
		'`' : 'TICK',
		'!' : 'EXCLAMATION POINT',
		'@' : 'AT',
		'#' : 'POUND',
		'$' : 'DOLLAR',
		'%' : 'PERCENT',
		'^' : 'CARET',
		'&' : 'AMPERSAND',
		'*' : 'ASTERISK',
		'(' : 'LEFT PARENTHESIS',
		')' : 'RIGHT PARENTHESIS',
		'-' : 'MINUS',
		'_' : 'UNDERSCORE',
		'+' : 'PLUS',
		'=' : 'EQUALS',
		' ' : 'SPACE'
		}


	# defaults are set at Rosalyn defaults
	# amplitude = 0-20, default=10
	# pitch = 0-99, default=50
	# speed measured in words per minute, default=160
	def __init__(self, console, amplitude=20, pitch=30, speed=160):
		self.amplitude = str(amplitude)
		self.pitch = str(pitch)
		self.speed = str(speed)
		self.console = console

	def speak(self, text):
		self.console.stopIO()
		cmd = ['espeak',
			'-a', self.amplitude,
			'-p', self.pitch,
			'-s', self.speed,
			'"' + text + '"']
		subprocess.call(cmd,
		                      shell=False,
		                      stdin=subprocess.DEVNULL,
		                      stdout=subprocess.DEVNULL,
		                      stderr=subprocess.DEVNULL)
		self.console.startIO()

	def speakChar(self, character):
		upper_char = character.upper()
		if upper_char in Speak.characters:
			self.speak(Speak.characters[upper_char])
		else:
			self.speak(upper_char)
