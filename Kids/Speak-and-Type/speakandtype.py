import sys
from game.Game import Game
from game.FileList import FileList
from Preferences import Preferences as Pref
from game.Console import Console
from game.Speak import Speak

def menu(game_file_list, console, speak, game):
	c = console
	s = speak
	num_files = game_file_list.getNumFiles()
	c.clear()
	c.outln(c.color(11) + 'Main Menu')
	c.outln(c.color(11) + '---------')
	s.speak('Main Menu')
	c.nextln()
	for i in range(0, num_files):
		num = i + 1
		c.out(c.color(11) + str(num))
		c.out(c.color(12) + ') ')
		menu_name = game_file_list.getBaseName(i)
		c.outln(c.color(12) + menu_name)
	c.nextln()
	c.out(c.color(11) + '0')
	c.out(c.color(12) + ') Quit')
	c.nextln(2)
	c.out(c.color(14) + 'Which game do you want to play?  ')
	c.out(c.color(7))
	s.speak('Which game do you want to play?')
	selection = -1
	try:
		selection = int(c.getText())
	except ValueError:
		selection = -1
	if (selection == 0):
		c.nextln()
		c.outln(c.color() + 'Goodbye')
		s.speak('Goodbye')
		c.nextln()
		return False
	elif ((selection >= 1) and (selection <= num_files)):
		file_num = selection - 1
		file_name = game_file_list.getFileName(file_num)
		#s.speak(game_file_list.getBaseName(file_num))
		keep_playing = True
		while (keep_playing):
			keep_playing = game.play(file_name)
		return True
	else:
		c.out(c.color(9) + 'That is not a valid number')
		s.speak('That is not a valid number')
		return True # redisplay the menu
	return False

def main():
	game_file_list = FileList(Pref.DATA_FILE_GLOB)
	console = Console()
	speak = Speak(console)
	game = Game(console, speak)
	stay_in_game = True
	while (stay_in_game):
		stay_in_game = menu(game_file_list, console, speak, game)
	console.nextln()

main()
quit()
