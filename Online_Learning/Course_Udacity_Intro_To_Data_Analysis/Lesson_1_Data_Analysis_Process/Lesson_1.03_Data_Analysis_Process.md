# Lesson 1: Data Analysis Process

## 1.3 - Data Analysis Process

1. Question
   * What question do you want to answer?
   * What problem do you want to solve?
1. Wrangle
   1. Data acquisition
   1. Data cleaning
1. Explore
   * Build intuition
   * Find patterns
1. Draw Conclusions
   * Or make predictions
   * Usually requires statistics or machine learning
1. Communicate
   * Your findings are not useful if you do not share them.

* You will likely cycle through data wrangling and exploring as you work with the data.
* You may revisit previous steps as you learn more.
