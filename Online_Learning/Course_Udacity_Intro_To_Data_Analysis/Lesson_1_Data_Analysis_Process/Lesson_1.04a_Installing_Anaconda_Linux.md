# Lesson 1: Data Analysis Process

## 1.4a - Installing Anaconda (my Linux notes)

* Jumped to Udacity course "Anaconda and Jupyter Notebooks".
* Started with Anaconda 3.6 version.
* Anaconda installs in home directory by default, not in system folders. Can be installed by root at system level.
* Anaconda has to be in your path, installer will update .bashrc for you.
* conda package commands:

```bash
conda list                               # list installed packages and versions
conda upgrade --all                      # update all packages
conda install packagename1 packagename2  # install list of packages
conda install packagename=1.5            # force specific version
conda remove packagename                 # uninstall package
conda update packagename                 # update specific package
conda search searchterm                  # search for a specific term
```

* conda environment commands:
  * Note: When you are not in an environment, you are in the _root_ environment.

```bash
conda create -n environmentname listofpackages  # create environment, install packages
    conda create -n py3 python=3                # create environment with latest python 3
    conda create -n py3 python=3.4              # create environment with latest python 3.4
source activate environmentname                 # enter environment
conda list                                      # will only show what is in this environment now
conda install packagename                       # install package in current environment
source deactivate                               # leave environment
conda env export                                # export environment configuration
    conda env export > environment.yaml
conda env create -f environment.yaml            # create a new environment from yaml file
conda env list                                  # list all environments
conda env remove -n environmentname             # delete an environment
```
