# Lesson 1: Data Analysis Process

## 1.4b - Installing Jupyter Notebook

* Comes with Anaconda

```bash
conda install jupyter notebook
pip install jupyter notebook
```

* Running Jupyter notebook
  * When you start the notebook, the browser automatically opens.
  * Tokens are used in the URL, if you need it you can search the console, or the login page tells you how to get it.
  * You install "kernels" to use different languages in a notebook.

```bash
jupyter notebook  # be in directory where your notebook files will be
# http://localhost:8888 = default url
#     Running multiple notebooks, the port number increases by 1
# Control + C (twice) exits server from terminal
```

* To run a Jupyter Notebook from an environment, you should install "nb_conda". This adds conda environemt control features within Jupyter.
* To shutdown Jupyter Notebook server:
  * Press Control + C (twice) in the terminal

* Magic commands
  * % = command applies to line
  * %% = command applies to cell
  * %timeit - time how long it takes to execute
  * %matplotlib - embed matplotlib visualizations
  * %config - used after matplotlib for settings
  * %pdb - python debugger
  * [whole list here](http://ipython.readthedocs.io/en/stable/interactive/magics.html)

* Jupyter notebook files (.ipynb) are just json files.
* You can convert notebook files to other formats from the command line (including slide shows):

```bash
jupyter nbconvert --to html notebook.ipynb
```
