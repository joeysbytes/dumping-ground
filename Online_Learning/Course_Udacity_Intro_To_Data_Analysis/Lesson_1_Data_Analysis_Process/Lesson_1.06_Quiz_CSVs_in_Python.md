# Lesson 1: Data Analysis Process

## 1.6 - Quiz: CSVs in Python

* Course is using unicodecsv instead of python's built-in csv library, but the usage is exactly the same
* [Python 2 CSV docs](https://docs.python.org/2/library/csv.html)
* [Difference between iterators and lists](https://www.codementor.io/python/tutorial/python-generators-and-iterators)
* Code snippet from quiz:

```python
import unicodecsv

## Longer version of code (replaced with shorter, equivalent version below)

# enrollments = []
# f = open('enrollments.csv', 'rb')
# reader = unicodecsv.DictReader(f)
# for row in reader:
#     enrollments.append(row)
# f.close()

def read_csv(filename):
    with open(filename, 'rb') as f:
        reader = unicodecsv.DictReader(f)
        return list(reader)

enrollments = read_csv('enrollments.csv')
print enrollments[0]

#####################################
#                 1                 #
#####################################

## Read in the data from daily_engagement.csv and project_submissions.csv 
## and store the results in the below variables.
## Then look at the first row of each table.

daily_engagement = read_csv('daily_engagement.csv')
print daily_engagement[0]

project_submissions = read_csv('project_submissions.csv')
print project_submissions[0]
```
