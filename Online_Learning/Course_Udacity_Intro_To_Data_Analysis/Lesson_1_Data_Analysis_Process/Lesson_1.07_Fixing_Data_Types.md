# Lesson 1: Data Analysis Process

## 1.7 - Fixing Data Types

* The CSV data is all treated as strings.
* You should convert these strings to their actual data types up front.
