# Lesson 1: Data Analysis Process

## 1.8 Quiz: Questions About Student Data

* Question Phase
  * How long to submit projects?
  * How do students who pass their projects differ from those who don't?
  * Be curious!
* Quiz: Think of at least 5 more questions (and share them).
  1. What is the average number of times each project submitted before it is passed?
  1. What is the average number of times a student submits a project before it is passed?
  1. Is every student trying every project?
  1. Are there any projects that have never been passed?
  1. Are the students working on the same projects in roughly the same time frame?
* Answers from course:
  1. How much time students spend taking classes.
  1. How time spent relates to lessons / projects completed.
  1. How engagement changes over time.
  1. How many times students submit.
* Course is going to work against this question: How do students who pass their projects differ from those who don't?
