# Lesson 1: Data Analysis Process

## 1.9 Quiz: Investigating the Data

* Code snippet from quiz:

```python
#####################################
#                 2                 #
#####################################

## Find the total number of rows and the number of unique students (account keys)
## in each table.

def find_unique_value_count(table, key):
    temp_set = set()
    for entry in table:
        temp_set.add(entry[key])
    return len(temp_set)

enrollment_num_rows = len(enrollments)
enrollment_num_unique_students = find_unique_value_count(enrollments, 'account_key')
print "Enrollment num rows: " + str(enrollment_num_rows)
print "Enrollment unique students: " + str(enrollment_num_unique_students)

engagement_num_rows = len(daily_engagement)
engagement_num_unique_students = find_unique_value_count(daily_engagement, 'acct')
print "Engagement num rows: " + str(engagement_num_rows)
print "Engagement unique students: " + str(engagement_num_unique_students)

submission_num_rows = len(project_submissions)
submission_num_unique_students = find_unique_value_count(project_submissions, 'account_key')
print "Submission num rows: " + str(submission_num_rows)
print "Submission unique students: " + str(submission_num_unique_students)
```
