# Lesson 1: Data Analysis Process

## 1.10 Quiz: Problems in the Data

* Problems in the data
  1. More unique students in enrollment than engagement table.
     * Fix: Will come back to this.
  1. Column named account_key in two tables and acct in the third.
     * Fix: Change column from acct to account_key.
     * [Helpful Link](https://stackoverflow.com/questions/5844672/delete-an-item-from-a-dictionary)
* Quiz code snippet:

```python
#####################################
#                 3                 #
#####################################

## Rename the "acct" column in the daily_engagement table to "account_key".

def rename_key(table, old_key, new_key):
    for entry in table:
        entry[new_key] = entry.pop(old_key)

rename_key(daily_engagement, 'acct', 'account_key')
print daily_engagement[0]['account_key']
```
