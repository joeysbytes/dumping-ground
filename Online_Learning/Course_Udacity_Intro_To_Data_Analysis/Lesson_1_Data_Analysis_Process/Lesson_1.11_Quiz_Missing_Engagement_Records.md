# Lesson 1: Data Analysis Process

## 1.11 Quiz: Missing Engagement Records

* Why are students missing from daily_engagement?
1. Identify surprising data points.
   * Any enrollment record with no corresponding engagement data.
1. Print out one or a few surprising data points.

* [Python's break statement](http://www.tutorialspoint.com/python/python_break_statement.htm)
* Code snippet from quiz:

```python
#####################################
#                 4                 #
#####################################

## Find any one student enrollments where the student is missing from the daily engagement table.
## Output that enrollment.

def get_unique_values(table, key):
    temp_set = set()
    for entry in table:
        temp_set.add(entry[key])
    return temp_set

unique_engagement_accounts = get_unique_values(daily_engagement, 'account_key')

prev_account_key = ''
for enrollment in enrollments:
    if enrollment['account_key'] != prev_account_key:
        prev_account_key = enrollment['account_key']
        if enrollment['account_key'] not in unique_engagement_accounts:
            print enrollment
            break
```
