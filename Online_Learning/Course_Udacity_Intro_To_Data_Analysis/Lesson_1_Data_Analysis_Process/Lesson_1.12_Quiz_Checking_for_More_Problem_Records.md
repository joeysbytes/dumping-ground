# Lesson 1: Data Analysis Process

## 1.12 Quiz: Checking for More Problem Records

* Investigating Data Problems
  1. Identify surprising data points
  1. Print out one or a few surprising points
  1. Fix any problems you find
     * More investigation may be necessary
     * Or, there might not be a problem
* This cycle repeats as necessary
* Code snippet from quiz:

```python
#####################################
#                 5                 #
#####################################

## Find the number of surprising data points (enrollments missing from
## the engagement table) that remain, if any.

surprise_count = 0
for enrollment in enrollments:
    if enrollment['account_key'] not in unique_engagement_accounts:
        if enrollment['days_to_cancel'] != 0:
            surprise_count += 1
            print enrollment
print surprise_count
```
