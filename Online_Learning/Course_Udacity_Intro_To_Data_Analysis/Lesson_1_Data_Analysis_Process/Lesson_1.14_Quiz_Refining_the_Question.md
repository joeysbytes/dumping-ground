# Lesson 1: Data Analysis Process

## 1.14 Quiz: Refining the Question

* Explore Phase
  * Question: How do numbers in the daily engagement table differ for students who pass the first project?
  * Problems:
    1. This will include data from after the project submission.
    1. This compares data from different lengths of time.
    1. Includes engagement in courses not related to the first project.
  * Revision: Only look at engagement from first week, and exclude students who cancel within a week.
    * Knowledge: First week is a free trial.

* Getting Started
  * Create a dictionary of students who either:
    * Haven't canceled yet (days_to_cancel is None)
    * Stayed enrolled more than 7 days (days_to_cancel > 7)
    * Keys: account keys
    * Values: enrollment date
    * Name of dictionary: paid_students

* Code snippet from quiz:

```python
#####################################
#                 6                 #
#####################################

## Create a dictionary named paid_students containing all students who either
## haven't canceled yet or who remained enrolled for more than 7 days. The keys
## should be account keys, and the values should be the date the student enrolled.

paid_students = {}

for enrollment in non_udacity_enrollments:
    if (enrollment['days_to_cancel'] is None) or \
        (enrollment['days_to_cancel'] > 7):
        account_key = enrollment['account_key']
        enrollment_date = enrollment['join_date']

        # Learned from answer we want the latest enrollment date
        if (account_key not in paid_students) or \
            (enrollment_date > paid_students[account_key]):
            paid_students[account_key] = enrollment_date

print len(paid_students)
```
