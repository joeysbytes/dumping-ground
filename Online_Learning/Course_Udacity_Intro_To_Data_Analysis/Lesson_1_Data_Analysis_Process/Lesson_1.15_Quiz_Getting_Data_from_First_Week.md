# Lesson 1: Data Analysis Process

## 1.15 Quiz: Getting Data from First Week

* Create a list of engagement records containing only data for paid students (who you found in the last exercise) during their first week.
* Code snippet from quiz:

```python
#####################################
#                 7                 #
#####################################

## Create a list of rows from the engagement table including only rows where
## the student is one of the paid students you just found, and the date is within
## one week of the student's join date.

paid_engagement_in_first_week = []
for engagement in non_udacity_engagement:
    account_key = engagement['account_key']
    if account_key in paid_students:
        student_join_date = paid_students[account_key]
        if within_one_week(student_join_date, engagement['utc_date']):
            paid_engagement_in_first_week.append(engagement)

print len(paid_engagement_in_first_week)
```
