# Lesson 1: Data Analysis Process

## 1.16 Indulge Curiosity

* Going to split data in to 2 parts:
  * Students who eventually pass the first project
  * Students who do not
* When you are curious about your data, investigate it.
  * Always useful to have a better sense of your data.
  * Exploring your data is never a waste of time.
