# Lesson 1: Data Analysis Process

## 1.17 Exploring Student Engagement

* Average Minutes Spent in Classroom (in first week)
  * See Jupyter notebook for instructor-provided code
  * python defaultdict
    * let's you specify the default value if a key is not found (like an empty list)
  * numpy
    * np.mean(list)
    * np.std(list) - standard deviation
    * np.min(list)
    * np.max(list)
