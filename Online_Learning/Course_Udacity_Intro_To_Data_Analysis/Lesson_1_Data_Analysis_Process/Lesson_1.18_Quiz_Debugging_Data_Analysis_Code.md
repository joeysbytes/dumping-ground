# Lesson 1: Data Analysis Process

## 1.18 Quiz: Debugging Data Analysis Code

* Debugging Data Problems
  1. Identify surprising data points.
  1. Print out one or a few surprising points.
  1. Fix any problems you find.

* My Observations
  1. There are 10080 minutes in a full week.
  1. There is 1 record with 10568, so this computes to more minutes than in a week.
  1. The next highest record is 7482 minutes, which is 5.2 full days. This seems excessive.

* Code snippet from quiz:

```python
#####################################
#                 8                 #
#####################################

## Go through a similar process as before to see if there is a problem.
## Locate at least one surprising piece of data, output it, and take a look at it.

# Look at minutes in list
#print sorted(total_minutes)

# Find student with max minutes

max_minutes_student = None
max_minutes = 0

for student, total_minutes in total_minutes_by_account.items():
    if total_minutes > max_minutes:
        max_minutes = total_minutes
        max_minutes_student = student

print max_minutes
print student

# Find engagement records for student

for engagement_record in paid_engagement_in_first_week:
    if engagement_record['account_key'] == max_minutes_student:
        print engagement_record

# Fix within_one_week function

# Takes a student's join date and the date of a specific engagement record,
# and returns True if that engagement record happened within one week
# of the student joining.
def within_one_week(join_date, engagement_date):
    time_delta = engagement_date - join_date
    # return time_delta.days < 7
    return time_delta.days < 7 and time_delta.days >= 0
```