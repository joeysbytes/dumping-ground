# Lesson 1: Data Analysis Process

## 1.19 Quiz: Lessons Completed in First Week

* Adapt instructor code to analyze lessons completed instead of minutes spent.
  * Calculate:
    * mean
    * standard deviation
    * minumum
    * maximum
  * Write functions rather than copying and pasting
* Code snippet from quiz:

```python
from collections import defaultdict

# Create a dictionary of engagement grouped by student.
# The keys are account keys, and the values are lists of engagement records.

def group_records(data_list, key):
    group_by_dict = defaultdict(list)
    for data_rec in data_list:
        group_by_key = data_rec[key]
        group_by_dict[group_by_key].append(data_rec)
    return group_by_dict

engagement_by_account = group_records(paid_engagement_in_first_week, 'account_key')

# Create a dictionary with the total minutes each student spent in the classroom during the first week.
# The keys are account keys, and the values are numbers (total minutes)

def total_group_records(group_data, key):
    group_totals = {}
    for data_key, data_value in group_data.items():
        total = 0
        for data_rec in data_value:
            total += data_rec[key]
        group_totals[data_key] = total
    return group_totals
    
total_minutes_by_account = total_group_records(engagement_by_account, 'total_minutes_visited')

import numpy as np

# Summarize the data about minutes spent in the classroom

def print_statistics(numbers):
    print 'Mean:', np.mean(numbers)
    print 'Standard deviation:', np.std(numbers)
    print 'Minimum:', np.min(numbers)
    print 'Maximum:', np.max(numbers)    

total_minutes = total_minutes_by_account.values()
print_statistics(total_minutes)

#####################################
#                 9                 #
#####################################

## Adapt the code above to find the mean, standard deviation, minimum, and maximum for
## the number of lessons completed by each student during the first week. Try creating
## one or more functions to re-use the code above.

total_lessons_by_account = total_group_records(engagement_by_account, 'lessons_completed')
total_lessons = total_lessons_by_account.values()
print_statistics(total_lessons)
```