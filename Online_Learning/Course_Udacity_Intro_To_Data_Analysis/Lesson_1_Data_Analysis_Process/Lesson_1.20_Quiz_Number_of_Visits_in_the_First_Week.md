# Lesson 1: Data Analysis Process

## 1.20 Quiz: Number of Visits in the First Week

* Goal: To analyze the number of days the student visited the classroom at all.
  * Options:
    * Change the "sum_grouped_items" function (or create a similar one) that only adds records where number > 0
    * Create a new field in data, called has_visited, either 1 or 0. Then sum_grouped_items function can add these without changes.
* Calculate:
  * mean
  * standard deviation
  * minumum
  * maximum

* Code snippet from quiz:

```python
# Clean up the data types in the engagement table
for engagement_record in daily_engagement:
    engagement_record['lessons_completed'] = int(float(engagement_record['lessons_completed']))
    engagement_record['num_courses_visited'] = int(float(engagement_record['num_courses_visited']))
    engagement_record['projects_completed'] = int(float(engagement_record['projects_completed']))
    engagement_record['total_minutes_visited'] = float(engagement_record['total_minutes_visited'])
    engagement_record['utc_date'] = parse_date(engagement_record['utc_date'])
    if engagement_record['total_minutes_visited'] > 0:
        engagement_record['has_visited'] = 1
    else:
        engagement_record['has_visited'] = 0
    
daily_engagement[0]

######################################
#                 10                 #
######################################

## Find the mean, standard deviation, minimum, and maximum for the number of
## days each student visits the classroom during the first week.

total_visits_by_account = total_group_records(engagement_by_account, 'has_visited')
total_visits = total_visits_by_account.values()
print_statistics(total_visits)
```