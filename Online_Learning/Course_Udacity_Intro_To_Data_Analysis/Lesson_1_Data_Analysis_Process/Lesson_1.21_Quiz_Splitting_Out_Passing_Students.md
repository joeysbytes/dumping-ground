# Lesson 1: Data Analysis Process

## 1.21 Quiz: Splitting Out Passing Students

* Split the engagement data into two lists:
  * Records for students who pass the subway project at some point (passing_engagement)
  * Records for students who do not (non_passing_engagement)

* Code snippet from quiz:

```python
######################################
#                 11                 #
######################################

## Create two lists of engagement data for paid students in the first week.
## The first list should contain data for students who eventually pass the
## subway project, and the second list should contain data for students
## who do not.

paid_submissions = []
for student in paid_students:
    for submission in non_udacity_submissions:
        if submission['account_key'] == student:
            paid_submissions.append(submission)

subway_project_lesson_keys = ['746169184', '3176718735']
passed_subway_project = set()
for submission in paid_submissions:
    if submission['lesson_key'] in subway_project_lesson_keys:
        if submission['assigned_rating'] == 'PASSED' or \
           submission['assigned_rating'] == 'DISTINCTION':
            passed_subway_project.add(submission['account_key'])

passing_engagement = []
non_passing_engagement = []

for engagement_rec in paid_engagement_in_first_week:
    if engagement_rec['account_key'] in passed_subway_project:
        passing_engagement.append(engagement_rec)
    else:
        non_passing_engagement.append(engagement_rec)
print len(passing_engagement)
print len(non_passing_engagement)
```
