# Lesson 1: Data Analysis Process

## 1.22 Quiz: Comparing the Two Student Groups

* Calculate some metrics you're interested in for both groups of students.
  * For example:
    * The three we looked at earlier:
      * minutes spent
      * lessons completed
      * days visited
      * Calculate the mean for these 3
  * Are these students more likely to complete other projects?
  * Look at the more granular data in daily_engagement_flow.csv
  * Be creative!
  * Do any results surprise you?
  * Which metric has the most interesting difference?

* My notes:
  * Total number of minutes seems to be the driving factor on whether a student passes.
  * It does somewhat correlate to number of days visited.

* Code snippet from quiz:

```python
######################################
#                 12                 #
######################################

## Compute some metrics you're interested in and see how they differ for
## students who pass the subway project vs. students who don't. A good
## starting point would be the metrics we looked at earlier (minutes spent
## in the classroom, lessons completed, and days visited).

passing_engagement_by_account = group_records(passing_engagement, 'account_key')
non_passing_engagement_by_account = group_records(non_passing_engagement, 'account_key')

def print_statistic_comparison(group1, group2, key):
    total_by_group1 = total_group_records(group1, key)
    total_group1 = total_by_group1.values()
    print("\nGroup 1: " + key)
    print_statistics(total_group1)
    total_by_group2 = total_group_records(group2, key)
    total_group2 = total_by_group2.values()
    print("\nGroup 2: " + key)
    print_statistics(total_group2)

print_statistic_comparison(passing_engagement_by_account, non_passing_engagement_by_account, 'total_minutes_visited')
print_statistic_comparison(passing_engagement_by_account, non_passing_engagement_by_account, 'lessons_completed')
print_statistic_comparison(passing_engagement_by_account, non_passing_engagement_by_account, 'has_visited')
```