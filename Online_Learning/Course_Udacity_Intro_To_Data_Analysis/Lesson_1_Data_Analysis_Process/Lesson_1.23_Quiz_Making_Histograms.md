# Lesson 1: Data Analysis Process

## 1.23 Quiz: Making Histograms

* matplotlib
  * Can be used to make a histogram.
  * Example:

```python
data = [1, 2, 1, 3, 3, 1, 4, 2]

%matplotlib inline  # plots appear in notebook instead of new window
import matplotlib.pyplot as plt
plt.hist(data)

# plt.show()  <-- if you were not using a notebook
```

* Create histograms of the 3 metrics measured for both passing and non-passing students (6 histograms total).

* Code snippet from quiz:

```python
######################################
#                 13                 #
######################################

## Make histograms of the three metrics we looked at earlier for both
## students who passed the subway project and students who didn't. You
## might also want to make histograms of any other metrics you examined.

passing_total_minutes_visited = total_group_records(passing_engagement_by_account, 'total_minutes_visited').values()
non_passing_total_minutes_visited = total_group_records(non_passing_engagement_by_account, 'total_minutes_visited').values()
passing_total_lessons_completed = total_group_records(passing_engagement_by_account, 'lessons_completed').values()
non_passing_total_lessons_completed = total_group_records(non_passing_engagement_by_account, 'lessons_completed').values()
passing_total_has_visited = total_group_records(passing_engagement_by_account, 'has_visited').values()
non_passing_total_has_visited = total_group_records(non_passing_engagement_by_account, 'has_visited').values()

%matplotlib inline
import matplotlib.pyplot as plt
plt.hist(passing_total_minutes_visited)
plt.hist(non_passing_total_minutes_visited)
plt.hist(passing_total_lessons_completed)
plt.hist(non_passing_total_lessons_completed)
plt.hist(passing_total_has_visited)
plt.hist(non_passing_total_has_visited)
```