# Lesson 1: Data Analysis Process

## 1.24 Are Your Results Just Noise?

* Drawing Conclusions Phase
  * Tentative conclusion:
    * Students who pass the subway project spend more minutes in the classroom during their first week.
  * But is this a true difference, or due to noise in the data?
    * You can check this using statistics.
    