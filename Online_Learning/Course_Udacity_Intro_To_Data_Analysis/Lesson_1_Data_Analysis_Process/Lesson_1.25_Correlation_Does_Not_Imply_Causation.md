# Lesson 1: Data Analysis Process

## 1.25 Correlation Does Not Imply Causation

* Correlation: Students who pass the first project are more likely to visit the classroom multiple times in their first week.
* Causation: Does visiting the classroom multiple times __cause__ students to pass their project?
* Third factors that could cause visiting the classroom and passing projects:
  * Level of interest
  * Background knowledge
* Or this correlation __could__ be because of causation! We just don't know.
  * To find out, run an A/B test (see Udacity course).
* [Great site on ridiculous correlations](http://tylervigen.com/spurious-correlations)
