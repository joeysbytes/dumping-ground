# Lesson 1: Data Analysis Process

## 1.26 Predicting Based On Many Features

* Making Predictions
  * Which students are likely to pass their first project?
    * Could take a first pass using heuristics, but getting a really good prediction this way could be difficult.
      * Lots of different pieces of information to look at.
      * These features can interact.
    * Machine learning can make predictions automatically. (see Udacity course)
