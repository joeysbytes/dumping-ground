# Lesson 1: Data Analysis Process

## 1.27 Communication

* Communication Phase
  * Communicate your findings
  * What findings are most interesting?
  * How will you present them?
    * Polish any visulizations
  * Examples:
    * Difference in total minutes, present average minutes
    * Difference in days visited, present histograms
