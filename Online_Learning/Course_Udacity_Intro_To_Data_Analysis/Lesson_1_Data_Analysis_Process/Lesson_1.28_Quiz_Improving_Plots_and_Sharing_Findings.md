# Lesson 1: Data Analysis Process

## 1.28 Quiz: Improving Plots and Sharing Findings

* matplotlib
  * plt.xlabel("Label for x axis")
  * plt.ylabel("Label for y axis")
  * plt.title("Title of plot")
  * Look at documentation for plt.hist to see what arguments are available
    * plt.hist(data, bins=20)

* seaborn
  * Library for making matplotlib plots look nicer
  * Just importing seaborn by itself will improve the look of your plots
  * Not included by default with anaconda (it was already installed in mine, so maybe this has changed?)
    * conda install seaborn
    * import seaborn as sns

* Improve at least one plot from before.

* Code snippet from quiz:

```python
######################################
#                 14                 #
######################################

## Make a more polished version of at least one of your visualizations
## from earlier. Try importing the seaborn library to make the visualization
## look better, adding axis labels and a title, and changing one or more
## arguments to the hist() function.

plt.title("Number of Minutes for Passing Students")
plt.xlabel("Minutes")
plt.hist(passing_total_minutes_visited)
```