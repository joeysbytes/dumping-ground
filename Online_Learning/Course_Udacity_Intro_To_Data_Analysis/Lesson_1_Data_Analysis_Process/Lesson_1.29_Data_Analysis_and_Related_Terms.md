# Lesson 1: Data Analysis Process

## 1.29 Data Analysis and Related Terms

* Data Science
  * Similar to data analysis
  * More focused on building systems (recommendation system, algorithm)
  * May require more experience
* Data Engineering
  * More focused on data wrangling
  * Involves data storage and processing
* Big Data
  * Fuzzy term for "a lot" of data
  * Data analysts, scientists, and engineers can all work with big data
