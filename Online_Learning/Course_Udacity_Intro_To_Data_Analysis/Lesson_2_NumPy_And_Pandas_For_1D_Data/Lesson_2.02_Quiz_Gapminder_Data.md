# Lesson 2: NumPy and Pandas for 1D Data

## Lesson 2.02: Quiz: Gapminder Data

* Gapminder Data
  * Employment Levels
  * Life Expectancy
  * GDP
  * School Completion Rates

* Data collected from [gapminder.org](http://www.gapminder.org/)

* Quiz: Think of 5 questions you could answer using this data.
  1. What percent of those who do/do not complete school are employed?
  1. Is there a difference in Life Expectency if a person is employed?
  1. Is there a difference in Life Expectency if a person completes school?
  1. Is there a correlation between GDP and Employment levels?
  1. Is there a better school completion rate for employed versus unemployed during school?

* Course answers:
  1. How has employment in the US varied over time?
     * or another country?
  1. What are the highest and lowest employment levels?
     * Which countries have them?
     * Where is the U.S. on the spectrum
  1. Same questions for other variables.
  1. How do these variables relate to each other?
  1. Are there consistent trends across countries?
     * Have there been any global recessions? Can they be found in this data?
