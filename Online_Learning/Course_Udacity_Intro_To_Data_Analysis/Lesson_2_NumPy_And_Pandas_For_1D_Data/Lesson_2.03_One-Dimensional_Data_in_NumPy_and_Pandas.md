# Lesson 2: NumPy and Pandas for 1D Data

## Lesson 2.03: One-Dimensional Data in NumPy and Pandas

* Pandas code is usually much faster than raw Python at doing tasks.

```python
import pandas as pd

# read csv file
daily_engagement = pd.read_csv('daily_engagement_full.csv')

# get number of unique accounts
len(daily_engagement['acct'].unique()
```
