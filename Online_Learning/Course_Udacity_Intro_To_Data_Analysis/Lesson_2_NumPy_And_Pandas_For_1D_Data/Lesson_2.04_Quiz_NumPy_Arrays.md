# Lesson 2: NumPy and Pandas for 1D Data

## Lesson 2.04: Quiz: NumPy Arrays

* NumPy (Numerical Python)
  * Data structure is called an array
    * Simpler than Pandas
    * Similar to a Python list
      * Access elements by position (indexing)
      * Access a range of elements (slicing)
      * Can use in loops
    * Differences from a Python List
      * Designed for each element to have the same data type
      * Convenient functions (type of elements matter)
        * mean()
        * std()
      * Can be multi-dimensional

* Pandas
  * Data structure is called a series.
    * Has more features than NumPy
    * Built upon NumPy arrays

```python
# NumPy functions

# create a NumPy array
    countries = np.array(['Afghanistan', 'Albania'])

# dtype: data type
    print np.array([0, 1, 2, 3]).dtype
    print np.array([1.0, 1.5, 2.0, 2.5]).dtype
    print np.array([True, False, True]).dtype
    print np.array(['AL', 'AK', 'AZ', 'AR', 'CA']).dtype

# some NumPy functions
    print employment.mean()
    print employment.std()
    print employment.max()
    print employment.sum()

# My code snippet for the quiz:
def max_employment(countries, employment):
    max_value = employment.max()
    for i in range(len(countries)):
        if employment[i] == max_value:
            max_country = countries[i]
            break;
    return (max_country, max_value)

# more NumPy functions
    print employment.argmax() # gives index where max value is
```
