# Lesson 2: NumPy and Pandas for 1D Data

## Lesson 2.05: Quiz: Vectorized Operations

* A vector is a list of numbers

```text
Vector 1 = 1, 2, 3
Vector 2 = 4, 5, 6

What is Vector 1 + Vector 2?

Vector Addition = 5, 7, 9
Python List Concatenation = 1, 2, 3, 4, 5, 6
```
