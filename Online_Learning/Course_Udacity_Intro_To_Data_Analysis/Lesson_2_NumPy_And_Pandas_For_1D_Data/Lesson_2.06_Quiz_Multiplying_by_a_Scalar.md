# Lesson 2: NumPy and Pandas for 1D Data

## Lesson 2.06: Quiz: Multiplying by a Scalar

```text
Vector 1 = 1, 2, 3
Scalar = 3

What is Vector 1 * Scalar?
    3, 6, 9 (Linear Algebra and NumPy Arrays)
    1, 2, 3, 1, 2, 3, 1, 2, 3 (Python Arrays)
```
