# Lesson 2: NumPy and Pandas for 1D Data

## Lesson 2.07: Quiz: Calculate Overall Completion Rate

* Vectorized Operations
  * Works with 2 vectors, or a vector and a single number
  * Math Operations
    * Add: +
    * Subtract: -
    * Multiple: *
    * Divide: /
    * Exponentiate: **
  * Logical Operations
    * Performs as expected on boolean values
    * On numeric values, performs bitwise-and, bitwise-or, bitwise-not
    * And: &
    * Or: |
    * Not: ~
  * Comparison Operations
    * Greater: >
    * Greater or Equal: >=
    * Less: <
    * Less or Equal: <=
    * Equal: ==
    * Not Equal: !=

* See [here](https://en.wikipedia.org/wiki/Bitwise_operation) to learn more about bitwise operators.

* Quiz: Calculate overall completion rate, assuming half male and half female country populations.

```python
def overall_completion_rate(female_completion, male_completion):
    total_completion = female_completion + male_completion
    avg_completion = total_completion / 2.0
    return avg_completion

    # also: return (female_completion + male_completion) / 2.0
```
