# Lesson 2: NumPy and Pandas for 1D Data

## Lesson 2.08: Quiz: Standardizing Data

* How does one data point compare to the rest?
  * Example: How does employment in the US compare to other countries?
* To answer, convert each data point to number of standard deviations away from the mean
  * In 2007:
    * Mean employment rate: 58.6%
    * Standard deviation: 10.5%
    * United States
      * Employment rate: 62.3%
      * Difference: 3.7%
        * 0.35 standard deviations
    * Mexico
      * Employment rate: 57.9%
      * Difference: -0.7%
        * -0.067 standard deviations

Quiz: Write a function that takes a NumPy array and converts the values to standardized values

```python
def standardize_data(values):
    mean = values.mean()
    standard_deviation = values.std()
    return (values - mean) / standard_deviation
```
