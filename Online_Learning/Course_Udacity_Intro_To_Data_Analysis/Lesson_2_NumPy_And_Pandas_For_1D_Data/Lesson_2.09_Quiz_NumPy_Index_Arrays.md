# Lesson 2: NumPy and Pandas for 1D Data

## Lesson 2.09: Quiz: NumPy Index Arrays

* You have 2 NumPy arrays
  * First array has any type of data
  * Second type has boolean data (this is called an index array)

```text
a = 1, 2, 3, 4, 5
b = F, F, T, T, T

a[b] = 3, 4, 5
    b = a > 2 : creates index array
    a[a > 2]  : 3, 4, 5
```

* Quiz: Write a function that takes in time_spent and days_to_cancel and returns mean time spent for students who stay at least 7 days.

```python
def mean_time_for_paid_students(time_spent, days_to_cancel):
    paid_time_spent = time_spent[days_to_cancel >= 7]
    return paid_time_spent.mean()
```
