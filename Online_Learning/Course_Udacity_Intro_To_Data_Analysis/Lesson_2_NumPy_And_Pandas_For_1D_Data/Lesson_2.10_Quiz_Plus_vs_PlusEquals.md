# Lesson 2: NumPy and Pandas for 1D Data

## Lesson 2.10: Quiz: + vs +=

+= will not modify a pointer address

```python
import numpy as np

a = np.array([1, 2, 3, 4])
b = a
a += np.array([1, 1, 1, 1])
print b
# Answer: 2, 3, 4, 5

a = np.array([1, 2, 3, 4])
b = a
a = a + np.array([1, 1, 1, 1])
print b
# Answer: 1, 2, 3, 4
```
