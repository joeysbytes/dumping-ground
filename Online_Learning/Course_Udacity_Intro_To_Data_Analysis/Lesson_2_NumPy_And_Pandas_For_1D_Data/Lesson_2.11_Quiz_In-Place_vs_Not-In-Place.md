# Lesson 2: NumPy and Pandas for 1D Data

## Lesson 2.11: Quiz: In-Place vs Not-In-Place

* += operates in place (stores values in original array)
* \+ does not operate in place (stores values in a new array)
  * Much easier to think about

```python
import numpy as np
a = np.array([1, 2, 3, 4, 5])
slice = a[:3]
slice[0] = 100
print a
# Answer: 100, 2, 3, 4, 5
```

* NumPy arrays behave differently from Python arrays, as the previous few examples have been showing.
* "slice" actually becomes a view of "a", not a new array.
* When "slice" is modified, it is also modifying "a"
