# Lesson 2: NumPy and Pandas for 1D Data

## Lesson 2.12: Quiz: Pandas Series

* Pandas Series
  * Similar to NumPy array, but with extra functionality
    * s.describe() - prints a bunch of statistics about the array
      * mean
      * standard deviation
      * median
      * and other statistics
  * Similarities with NumPy arrays
    * Access elements: s[0], s[3:7]
    * Looping: for x in s
    * Convenient functions: s.mean(), s.max()
    * Vectorized operations: s1 + s2
    * Implemented in C for speed

* Quiz: Write a function that takes in 2 series (life expectancy and GDP).
  * Question we are answering: When a country has a life expectancy above the mean, is the GDP above the mean also? (or vice versa)
  * Return 2 numbers
    1. Number of countries where both values are above or both are below the mean.
    1. Number of countries where one value is above and one is below the mean.
  * Hint: You can add booleans in Python
    * True = 1
    * False = 0
    * So you could make an index array and add them together
  * You should be able to write the same code you would write with NumPy

```python
# My note: I could not get boolean addition to work correctly, so converting to ints instead
    var1_mean = variable1.mean()
    var2_mean = variable2.mean()
    var1_idx = (variable1 >= var1_mean).astype(int)
    var2_idx = (variable2 >= var2_mean).astype(int)
    sum_idx = var1_idx + var2_idx

    num_different_direction = len(sum_idx[sum_idx == 1])
    num_same_direction = len(variable1) - num_different_direction

    return (num_same_direction, num_different_direction)
```
