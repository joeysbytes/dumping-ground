# Lesson 2: NumPy and Pandas for 1D Data

## Lesson 2.13: Quiz: Series Indexes

* Benefits of a Pandas Series over a NumPy Array
  * More functionality (s.describe())
  * NumPy is like a souped up Python list, but Pandas series has an index (NumPy has a position)
    * A cross between a list and a dictionary (like a linked-list)
    * Order is maintained
    * Examples
      * s[0] = item at position 0
      * s.loc['Key'] = value for key
      * s.iloc[0] = item at position 0
    * If you do not specify an index, numbers 0 thru # are used as keys

* Quiz: Rewrite max_employment function to utilze a single Pandas series with a country index

```python
def max_employment(employment):
    max_country = employment.idxmax()
    max_value = employment.loc[max_country]
    return (max_country, max_value)
```

* Note: previously argmax() was mentioned as a way to get the position of the maximum value, but you should use idxmax() instead.