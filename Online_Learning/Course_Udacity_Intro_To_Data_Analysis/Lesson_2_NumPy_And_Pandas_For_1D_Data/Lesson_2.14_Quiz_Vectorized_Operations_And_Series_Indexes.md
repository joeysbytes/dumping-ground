# Lesson 2: NumPy and Pandas for 1D Data

## Lesson 2.14: Quiz: Vectorized Operations and Series Indexes

* When you add two NumPy arrays, you're adding by position (there is no index)
* What do you think happens if you add two series with different indexes?
  * When the indexes match, they will be added.
  * If an index is in one series but not the other, the value is NaN
    * You will get all unique indexes in the answer
