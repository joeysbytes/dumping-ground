# Lesson 2: NumPy and Pandas for 1D Data

## Lesson 2.15: Quiz: Filling Missing Values

* With Pandas, you can handle indexes that are in 1 series but not another in different ways
  * s.dropna() - drop NaN entries

* Quiz: Add 2 Pandas Series together, treating missing values as 0

```python
    s3 = s1.add(s2, fill_value=0)
    print s3
````
