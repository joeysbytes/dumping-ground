# Lesson 2: NumPy and Pandas for 1D Data

## Lesson 2.16: Quiz: Pandas Series apply()

* Non Build-In Calculations
  * So far we have been using build-in functions (like mean()) and operations (like +)
  * Options to apply more complex calculations against the series
    1. Treat the series as a list (for loops, for example)
    1. Use Pandas function apply() to apply non built-in computations
       * Takes a Series and a function, and returns a new series
       * Exactly like Python function Map, but works on Series instead of lists
       * Example: s.apply(function_name)

* Quiz: Write a function that takes a series of names in format Firstname Lastname, and creates a new series in format Lastname, Firstname. Use Pandas apply()

```python
def reverse_names(names):

    def perform_reverse(full_name):
        name_parts = full_name.split()
        return name_parts[1] + ", " + name_parts[0]

    return names.apply(perform_reverse)
```
