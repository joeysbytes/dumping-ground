# Lesson 2: NumPy and Pandas for 1D Data

## Lesson 2.17: Quiz: Plotting in Pandas

```python
# This works if data is a list, NumPy array, or Pandas Series
import matplotlib.pyplot as plt
plt.hist(data)
plt.show()

# If data is a Series, you can use the built-in hist(), which uses
# matplotlib behind the scenes.
data.plot()
```

Quiz: Take given data, pick a country you are interested in, make a plot of each variable over time.

```python
import matplotlib.pyplot as plt

employment_us.plot()
plt.show()

female_completion_us.plot()
plt.show()

male_completion_us.plot()
plt.show()

life_expectancy_us.plot()
plt.show()

gdp_us.plot()
plt.show()
```
