# Lesson 3: NumPy and Pandas for 2D Data

## Lesson 3.02: Quiz: Subway Data

* Analyzing Subway and Weather Data
  * Look at data for this lesson, think of at least 5 questions.

* My questions:
  1. Do any of the weather elements by themselves affect subway volume?
     * Temperature
     * Rainfall
     * Sky Conditions
     * Pressure
  1. Does the average subway volume shift to another location based on weather?
  1. What day of the week is the busiest?  The quietest?
  1. What subway station is the busiest overall?
  1. For each subway station, what is the usual weather conditions?

* Instructor questions:
  1. What variables are related to higher or lower subway ridership?
     * Which stations have the most riders?  How dramatic is the difference?
     * What are the ridership patterns over time?  Can we see rush hour effects?  Can we see weekend / weekday effects?
     * Data is collected for May, which includes a holiday.  Can we see an effect?
     * How does the weather affect ridership?
  1. What patterns can I find in the weather? (independent of the subway)
     * Is the temperature rising throughout the month?
     * How does the weather vary across the city?
