# Lesson 3: NumPy and Pandas for 2D Data

## Lesson 3.03: Quiz: Two-Dimensional NumPy Arrays

* Two-Dimensional Data
  * Has both rows and columns
    * Python: list of lists
    * NumPy: 2D array
    * Pandas: Data Frame
  * NumPy 2D Arrays
    * More memory efficient
      * [Layout of 2D NumPy Arrays](https://docs.scipy.org/doc/numpy/reference/arrays.ndarray.html#internal-memory-layout-of-an-ndarray)
    * Syntax to accessing elements is different
      * a[1,3] instead of a[1][3]
      * The row and column values can be slices
    * mean(), std(), etc. operate on entire array

* Quiz: Write a function to:
  1. Find the station with the max riders on the first day.
  1. Find the mean riders per day for that station.
  1. Return mean for all riders at all stations for comparison.

```python
def mean_riders_for_max_station(ridership):
    station_with_max_riders_first_day = np.argmax(ridership[0,:])
    overall_mean = ridership.mean()
    mean_for_max = ridership[:,station_with_max_riders_first_day].mean()
    return (overall_mean, mean_for_max)
```
