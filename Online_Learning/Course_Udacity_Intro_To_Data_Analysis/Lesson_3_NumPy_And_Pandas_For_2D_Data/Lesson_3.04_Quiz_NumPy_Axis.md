# Lesson 3: NumPy and Pandas for 2D Data

## Lesson 3.04: Quiz: NumPy Axis

* Most operations in NumPy take an axis argument
  * axis 0 = column
  * axis 1 = row
  * Example: ridership.mean(axis=0)

* Quiz: Find the mean ridership per day for each subway station.
  * Out of all the subway stations, find the maximum and minimum ridership per day

```python
def min_and_max_riders_per_day(ridership):
    mean_daily_ridership = ridership.mean(axis=0)
    max_daily_ridership = mean_daily_ridership.max()
    min_daily_ridership = mean_daily_ridership.min()
    return (max_daily_ridership, min_daily_ridership)
```