# Lesson 3: NumPy and Pandas for 2D Data

## Lesson 3.05: NumPy and Pandas Data Types

* NumPy.dtype - shows type of data in array
  * NumPy expects all data in array to be of the same type
* Pandas data frames can have multiple data types
  * Each frame is assumed to be its own type
  * Has indexes for each row, and names for each column
