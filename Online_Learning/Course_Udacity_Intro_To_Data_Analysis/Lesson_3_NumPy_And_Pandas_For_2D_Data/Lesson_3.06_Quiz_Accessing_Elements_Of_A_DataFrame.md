# Lesson 3: NumPy and Pandas for 2D Data

## Lesson 3.06: Quiz: Accessing Elements of a DataFrame

* dataframe.loc(row-label) - get a row
* dataframe.loc(row-label, column-label) - get a single cell
* dataframe.iloc[col-index-num] - get a column by position
* dataframe.iloc[row-index-num, column-index-num] - get a single cell by position
* dataframe[column-label] - get a column
* dataframe.values - get 2d NumPy array containing only the values
  * Use case: get mean of all values, rather than just each row or column: dataframe.values.mean()

Quiz: Rewrite a function done previously with NumPy, using DataFrames. Do not use .values to get a NumPy array

```python
def mean_riders_for_max_station(ridership):
    max_riders_first_day_station = ridership.iloc[0].argmax()
    mean_for_max = ridership[max_riders_first_day_station].mean()
    overall_mean = ridership.values.mean()
    return (overall_mean, mean_for_max)
```
