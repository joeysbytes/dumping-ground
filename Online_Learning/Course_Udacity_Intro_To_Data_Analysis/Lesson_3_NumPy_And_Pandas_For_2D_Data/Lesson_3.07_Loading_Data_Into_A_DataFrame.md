# Lesson 3: NumPy and Pandas for 2D Data

## Lesson 3.07: Loading Data into a DataFrame

* DataFrames are great for representing a CSV file
  * pd.read_csv('filename.csv')
  * dataframe.head() - print first 5 lines
  * dataframe.describe() - get statistics about the dataframe
