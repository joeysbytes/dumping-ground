# Lesson 3: NumPy and Pandas for 2D Data

## Lesson 3.08: Quiz: Calculating Correlation

* Correlation of two variables, also called "Pearson's r"
  * Will be a value between -1 and 1
    * If near zero, the values are not stronly correlated
  * Only measures linear correlation
  * To calculate:
    * First standardize each variable
      * This puts each variable on a similar scale
    * Multiply each pair of values, and take the average
    * r = average of (x in standard units) * (y in standard units)
      * if r is positive, then as 1 variable increases, the other increases
      * if r is negative, then as 1 variable increases, the other decreases
* Panda's std() function by default uses corrected standard deviation
  * Uses [Bessel's correction](https://en.wikipedia.org/wiki/Bessel%27s_correction)
* NumPy calculates Pearson's r with [corrcoef()](https://docs.scipy.org/doc/numpy/reference/generated/numpy.corrcoef.html) function
  * Also known as the correlation coefficient
* References:
  * [Pearson's Correlation](http://onlinestatbook.com/2/describing_bivariate_data/pearson.html)
  * [Interpreting Correlation](http://rpsychologist.com/d3/correlation/)
  * [Image of different correlation types](https://en.wikipedia.org/wiki/Correlation_and_dependence#/media/File:Correlation_examples2.svg)

Quiz: Correlate New York Subway Data

* Have to use variable.std(ddof=0)
  * This says use uncorrected standard deviation
  * By default, corrected standard deviation is used

```python
def correlation(x, y):
    x_std = (x - x.mean()) / x.std(ddof=0)
    y_std = (y - y.mean()) / y.std(ddof=0)
    return (x_std * y_std).mean()
```
