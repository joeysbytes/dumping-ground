# Lesson 3: NumPy and Pandas for 2D Data

## Lesson 3.09: Pandas Axis Names

* Instead of axis=0 or axis=1 for numpy, you can also use axis='index' or axis='columns'
  * axis='columns' will give you rows (you are going along the columns)
  * axis='index' will give you columns (you are going along the rows)
