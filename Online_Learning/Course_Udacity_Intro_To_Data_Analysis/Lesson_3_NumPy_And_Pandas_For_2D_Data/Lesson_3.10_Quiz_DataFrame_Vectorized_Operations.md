# Lesson 3: NumPy and Pandas for 2D Data

## Lesson 3.10: Quiz: DataFrame Vectorized Operations

* Similar to NumPy 2D vectorized operations
* Match up elements by index and column names rather than by position
* References:
  * [Pandas shift()](http://pandas.pydata.org/pandas-docs/stable/generated/pandas.DataFrame.shift.html)

Quiz: Convert a data frame with cumulative totals to hourly totals.

```python
def get_hourly_entries_and_exits(entries_and_exits):
    return entries_and_exits - entries_and_exits.shift(1)

    # Alternative answer
    # return entries_and_exits.diff()
```
