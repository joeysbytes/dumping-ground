# Lesson 3: NumPy and Pandas for 2D Data

## Lesson 3.11: Quiz: DataFrame applymap()

* applymap(function)
  * function(s)
  * Applies function to each element in a dataframe, and returns a new dataframe
  * Use this if there is not a built-in function for what you want to do

Quiz: Use applymap() to convert numerical grades to letter grades.

```python
def convert_grade(grade):
    if grade >= 90:
        return 'A'
    elif grade >= 80:
        return 'B'
    elif grade >= 70:
        return 'C'
    elif grade >= 60:
        return 'D'
    else:
        return 'F'

def convert_grades(grades):
    return grades.applymap(convert_grade)
```
