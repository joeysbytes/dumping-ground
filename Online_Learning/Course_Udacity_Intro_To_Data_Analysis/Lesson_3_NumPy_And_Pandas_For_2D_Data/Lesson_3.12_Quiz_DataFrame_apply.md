# Lesson 3: NumPy and Pandas for 2D Data

## Lesson 3.12: Quiz: DataFrame apply()

* apply() does not take a single cell, instead it takes a single column.
  * This is a Pandas series
  * Applies function on each column of the data frame
  * Example usage: grading on a curve, have to know all data
  * Also takes an axis argument, so it can go by rows instead of columns

Quiz: Use apply() to standardize each column of a data frame

```python
def standardize_column(column):
    return (column - column.mean()) / column.std()

def standardize(df):
    return df.apply(standardize_column)
```
