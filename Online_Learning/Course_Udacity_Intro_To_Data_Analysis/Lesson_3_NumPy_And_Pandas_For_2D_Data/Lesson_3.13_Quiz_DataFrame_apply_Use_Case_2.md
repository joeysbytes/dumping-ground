# Lesson 3: NumPy and Pandas for 2D Data

## Lesson 3.13: Quiz: DataFrame apply() Use Case 2

* The function apply uses can take a column, and return a single value.
  * You will get a new series instead of a new data frame.
  * Example: use a function to find the second largest max value in a column.

Quiz: Use apply() to find the second-largest value in each column of a data frame.

```python
def second_largest_in_column(column):
    sorted_column = column.sort_values(ascending=False)
    return sorted_column.iloc[1]

def second_largest(df):
    return df.apply(second_largest_in_column)
```
