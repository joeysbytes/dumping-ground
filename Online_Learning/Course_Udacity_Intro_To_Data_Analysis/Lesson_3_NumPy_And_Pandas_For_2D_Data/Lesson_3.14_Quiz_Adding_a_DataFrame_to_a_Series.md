# Lesson 3: NumPy and Pandas for 2D Data

## Lesson 3.14: Quiz: Adding a DataFrame to a Series

* Vectorized operations allow you to:
  * Add 2 DataFrames together
  * Add 2 Series together
* You can also add a DataFrame and a Series

* Quiz: Practice adding a DataFrame and a Series, note what happens
  * Get colum addition, based on matching indexes
