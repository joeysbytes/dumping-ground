# Lesson 3: NumPy and Pandas for 2D Data

## Lesson 3.15: Quiz: Standardizing Each Column Again

Quiz: Standardize each column without using apply(), instead use vectorized operations.

* Bonus: Try to standardize each row without using apply()

```python
def standardize(df):
    return (df - df.mean()) / df.std()

def standardize_rows(df):
    mean_diffs = df.sub(df.mean(axis='columns'), axis='index')
    return  mean_diffs.div(df.std(axis='columns'), axis='index')
```
