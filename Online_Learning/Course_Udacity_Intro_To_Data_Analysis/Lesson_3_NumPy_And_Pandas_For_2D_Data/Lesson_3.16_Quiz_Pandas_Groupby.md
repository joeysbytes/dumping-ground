# Lesson 3: NumPy and Pandas for 2D Data

## Lesson 3.16: Quiz: Pandas groupby()

* Looking at subway data, we want to group by:
  * Hour of day
  * Rain
  * Day of week
* Example: engagement\_df.groupby('account\_key).sum()['total_minutes_visited'].mean()
  * DataFrame.groupby('key') - creates a DataFrameGroupBy object
  * .groups - will show which rows correspond to which key
  * .sum() - will add up each value per key
  * [index] - pulls out the series for this index
  * .mean() - gets mean
  * .describe() - gets several statistics
* You can use apply() with DataFrameGroupBy objects

Quiz: Group subway data by variable of your choice. Find mean subway ridership per value. Print or plot values.

```python
filename = '/data/joey/development/lesson3_nb/nyc_subway_weather.csv'
subway_df = pd.read_csv(filename)

ridership_by_day = subway_df.groupby('day_week').mean()['ENTRIESn_hourly']

%pylab inline
import seaborn as sns
ridership_by_day.plot()
```