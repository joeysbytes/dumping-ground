# Lesson 3: NumPy and Pandas for 2D Data

## Lesson 3.17 Quiz: Calculating Hourly Entries and Exits

* References:
  * [Pandas groupby()](http://pandas.pydata.org/pandas-docs/stable/groupby.html)

Quiz: Write a function to group subway data by station and day, then calculate the hourly entries and exits within each day. Use apply() function.

```python
def hourly_for_group(entries_and_exits):
    return entries_and_exits - entries_and_exits.shift(1)

def get_hourly_entries_and_exits(entries_and_exits):
    return entries_and_exits.groupby('UNIT')[['ENTRIESn', 'EXITSn']].apply(hourly_for_group)
```
