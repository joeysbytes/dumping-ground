# Lesson 3: NumPy and Pandas for 2D Data

## Lesson 3.18: Quiz: Combining Pandas DataFrames

* Working with multiple dataframes
  * merge: combine 2 tables in to 1 table
    * Works very similar to join in SQL
  * Example: submissions.merge(enrollments, on='account_key', how='left')
    * 'how' can be: left, inner, outer
    * If column names do not match between tables, can use left\_on and right\_on.
* Depending on use case, recommend removing duplicate rows before doing a merge.

Quiz: Use merge() to combine the subway and weather data

```python
def combine_dfs(subway_df, weather_df):
    return subway_df.merge(weather_df,
                           on=['DATEn', 'hour', 'latitude', 'longitude'],
                           how='inner')
```
