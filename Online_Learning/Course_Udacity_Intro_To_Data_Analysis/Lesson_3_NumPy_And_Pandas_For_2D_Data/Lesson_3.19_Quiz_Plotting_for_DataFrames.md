# Lesson 3: NumPy and Pandas for 2D Data

## Lesson 3.19: Quiz: Plotting for DataFrames

* DataFrames have a plot() method.

* References:
  * [matplotlib pyplot api](http://matplotlib.org/api/pyplot_api.html)

Quiz: Create a plot of your choice showing smething interesting about the subway data.

```python
# Instructor answer
# Make a scatterplot of subway stations with latitude and longitude
# as the x and y axes and ridership as the bubble size

data_by_location = subway_df.groupby(['latitude', 'longitude'],
                                     as_index=False).mean()
# data_by_location.head()['latitude']

%pylab inline
import matplotlib.pyplot as plt
import seaborn as sns

scaled_entries = (data_by_location['ENTRIESn_hourly'] / data_by_location['ENTRIESn_hourly'].std())

plt.scatter(data_by_location['latitude'],
            data_by_location['longitude'],
            s=scaled_entries)
```