# Lesson 3: NumPy and Pandas for 2D Data

## Lesson 3.20: Three-Dimensional Data

* 3D data in NumPy
  * NumPy arrays can have many dimensions
    * You can create from lists of lists of lists

* 3D data in Pandas
  * [Panel](http://pandas.pydata.org/pandas-docs/stable/dsintro.html#panel) - data structure for 3D data
