# Udacity Course: Intro To Data Analysis

## Thoughts on Course

This is a great course if you do not know anything about data analysis. Lesson 1 concentrates on the techniques, while lessons 2 and 3 concentrate on using NumPy and Pandas. For myself, I have learned if I want to pursue further learning, I am going to need a course in statistics.

## Journal of Progress

Udacity course notes from "Intro to Data Analysis".
Started September 5, 2017 with Data Science group at work:

9/18/2017: I was the only student who completed lesson 1. All others had technical issues. We are pausing to get everyone running on their work computers (I used my personal computer).

9/25/2017: Several of us got together to get Anaconda and Jupyter Notebooks running.

9/29/2017: We had a study group to get everyone started on Lesson 1. I am waiting until the group finishes to begin lesson 2.

10/2/2017: Due to upcoming commitments for my evenings in the forseeable future, I decided to start on Lesson 2 ahead of the study group.

10/3/2017: Completed lesson 2.

10/11/2017: Continuing to lesson 3 on my own, as I have the time for the next few evenings.

10/13/2017: As was expecting, evenings became engaged with other activites, learning on pause.

11/3/2017: Lesson 3 resumes.

11/12/2017: The study group has failed to get together the last few weeks, I am finishing this on my own today.

11/12/2017: I am skipping the final project, however if my study group at work comes back together and wants to do it, then I will participate.
