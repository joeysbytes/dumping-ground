from core import files_db
import os


def delete_file(full_file_name, full_dir_name):
    """Update database entries, and physically delete file."""
    print("Deleting file: " + full_file_name)
    files_db.delete_file(full_file_name)
    files_db.decrease_file_count(full_dir_name)
    _delete_file(full_file_name)


def delete_directory(full_dir_name):
    """Update database entries, and physically delete directory."""
    print("Deleting directory: " + full_dir_name)
    files_db.delete_dir(full_dir_name)
    _delete_directory(full_dir_name)


def _delete_file(full_file_name):
    """Physically delete a file.
    Should not be called directly from outside this module.
    """
    try:
        os.remove(full_file_name)
    except:
        print("ERROR: Failed to delete file: " + full_file_name)
        files_db.rollback_transaction()
        files_db.close()
        quit()


def _delete_directory(full_dir_name):
    """Physically delete a directory.
    Should not be called directly from outside this module.
    """
    try:
        os.rmdir(full_dir_name)
    except:
        print("ERROR: Failed to delete directory: " + full_dir_name)
        files_db.rollback_transaction()
        files_db.close()
        quit()
