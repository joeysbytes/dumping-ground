from core.config import *
import sqlite3

connection = None
transaction_counter = 0


def open():
    """Open the database connection.
    Isolation level set to None, so developer is in complete control of transactions.
    """
    print("Open database: " + DATABASE_FILE)
    global connection
    connection = sqlite3.connect(DATABASE_FILE, isolation_level=None)


def close():
    """Close database connection."""
    print("Close database: " + DATABASE_FILE)
    global connection
    connection.close()
    connection = None


def create_tables():
    """Create files database tables."""
    print("Creating tables if needed")
    dir_table = """
                CREATE TABLE IF NOT EXISTS DIRECTORIES
                (NAME TEXT PRIMARY KEY,
                 NUM_DIRS INTEGER NOT NULL,
                 NUM_FILES INTEGER NOT NULL);
                """
    files_table = """
                  CREATE TABLE IF NOT EXISTS FILES
                  (FULL_NAME TEXT PRIMARY KEY,
                   NAME TEXT NOT NULL,
                   EXTENSION TEXT NOT NULL,
                   DIR TEXT NOT NULL,
                   TYPE TEXT NOT NULL,
                   SIZE INTEGER NOT NULL,
                   CHECKSUM TEXT);
                  """
    connection.execute(dir_table)
    connection.execute(files_table)


def begin_transaction():
    """Begin a transaction."""
    print("Beginning transaction")
    global transaction_counter
    connection.execute("BEGIN TRANSACTION;")
    transaction_counter = 0


def increment_transaction_counter():
    """Increment the transaction counter."""
    global transaction_counter
    transaction_counter += 1


def commit_transaction(force=False, begin_trans=True):
    """Perform a transaction commit if the transaction counter is at a limit, or forced.
    If a commit occurs, a new transaction will occur if begin_trans is True.
    """
    global transaction_counter
    if force or transaction_counter >= DATABASE_TRANSACTION_SIZE:
        print("Committing transaction")
        connection.execute("COMMIT TRANSACTION;")
        transaction_counter = 0
        if begin_trans:
            begin_transaction()


def rollback_transaction():
    """Rollback the current transaction."""
    global transaction_counter
    connection.execute("ROLLBACK TRANSACTION;")
    transaction_counter = 0


def add_dir(dir_name, num_dirs, num_files):
    """Add an entry to the directory table."""
    dir_sql = """
              INSERT INTO DIRECTORIES (NAME, NUM_DIRS, NUM_FILES)
              VALUES (?, ?, ?);
              """
    connection.execute(dir_sql, (dir_name, num_dirs, num_files))


def add_file(full_name, file_name, extension, dir_name, file_type, size):
    """Add an entry to the files table."""
    file_sql = """
               INSERT INTO FILES (FULL_NAME, NAME, EXTENSION, DIR, TYPE, SIZE)
               VALUES (?, ?, ?, ?, ?, ?);
               """
    connection.execute(file_sql, (full_name, file_name, extension, dir_name, file_type, size))


def delete_file(full_name):
    """Delete a file entry.
    Will issue a rollback and exit if more than 1 entry is deleted.
    """
    delete_sql = """
                 DELETE FROM FILES
                 WHERE FULL_NAME = ?;
                 """
    row_count = connection.execute(delete_sql, (full_name,)).rowcount
    if row_count != 1:
        print("ERROR: File delete row count was not one: " + str(row_count))
        print("  File full name: " + full_name)
        rollback_transaction()
        close()
        quit()


def delete_dir(full_dir_name):
    """Delete a directory entry.
    Will issue a rollback and exit if more than 1 entry is deleted.
    """
    delete_sql = """
                 DELETE FROM DIRECTORIES
                 WHERE NAME = ?;
                 """
    row_count = connection.execute(delete_sql, (full_dir_name,)).rowcount
    if row_count != 1:
        print("ERROR: Directory delete row count was not one: " + str(row_count))
        print("  File directory name: " + full_dir_name)
        rollback_transaction()
        close()
        quit()



def decrease_file_count(dir_name):
    """Look up a given directory name, and decrease the file count by 1.
    Will cause a rollback and quit if more than 1 record is updated.
    """
    decrease_sql = """
                   UPDATE DIRECTORIES
                   SET NUM_FILES = NUM_FILES - 1
                   WHERE NAME = ?;
                   """
    row_count = connection.execute(decrease_sql, (dir_name,)).rowcount
    if row_count != 1:
        print("ERROR: Decreasing file count row count was not one: " + str(row_count))
        print("  Directory full name: " + dir_name)
        rollback_transaction()
        close()
        quit()


def select(sql):
    """Run sql select query provided and return the cursor."""
    return connection.execute(sql)


def select_single_value(sql):
    """Run a sql and return the first column of the first row.
    The intent of this method is to provide convenience for queries like row counts.
    """
    return connection.execute(sql).fetchone()[0]
