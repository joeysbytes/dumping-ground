from core import delete_utils
from core import files_db


# Set this to False to physically remove files
SIMULATE = True

SQL_SELECT = """
             SELECT NAME
             FROM DIRECTORIES
             """
SQL_SELECT_COUNT = """
                   SELECT COUNT(NAME)
                   FROM DIRECTORIES
                   """
SQL_ORDER_BY = """
               ORDER BY NAME
               """

# Define where clauses to use to select files
empty_directories_where_clause = """
                           WHERE NUM_FILES = 0
                           AND NUM_DIRS = 0
                           """


# State the where clause to use
where_clause = empty_directories_where_clause


def main():
    print("Delete empty directories, simulate = " + str(SIMULATE))
    files_db.open()
    perform_deletes = confirm_by_user()
    if perform_deletes:
        print("Performing empty directory deletions")
        delete_directories()
    else:
        print("Not performing empty directory deletions")
    files_db.close()


def confirm_by_user():
    """Give user initial statistics, and make them confirm to perform deletions."""
    delete_sql_count = SQL_SELECT_COUNT + where_clause + ";"
    print("\nSQL to select record count for deletion")
    print(delete_sql_count + "\n")
    record_count = files_db.select_single_value(delete_sql_count)
    if record_count == 0:
        print("No records selected to delete")
        return False
    else:
        print("\nInitial records selected to delete: " + str(record_count))
        if SIMULATE:
            return False
        else:
            print("\nNote: This will delete all parent directories if they become empty.")
            print("Note: You must enter YES in all capitals to confirm deletion.")
            confirmation = input("Do you wish to delete these records? ")
            print("")
            if confirmation == "YES":
                return True
            else:
                return False


def delete_directories():
    """Run SQL and physically delete the directories returned."""
    total_dirs_deleted = 0
    delete_sql_count = SQL_SELECT_COUNT + where_clause + ";"
    delete_sql = SQL_SELECT + where_clause + SQL_ORDER_BY + ";"
    record_count = files_db.select_single_value(delete_sql_count)
    print("\nSQL to select records for deletion")
    print(delete_sql + "\n")
    while record_count > 0:
        cursor = files_db.select(delete_sql)
        files_db.begin_transaction()
        row = cursor.fetchone()
        while row:
            delete_utils.delete_directory(row[0])
            files_db.increment_transaction_counter()
            files_db.commit_transaction()
            row = cursor.fetchone()
        files_db.commit_transaction(force=True, begin_trans=False)
        total_dirs_deleted += record_count
        record_count = files_db.select_single_value(delete_sql_count)
    print("Total directories deleted: " + str(total_dirs_deleted))


main()
