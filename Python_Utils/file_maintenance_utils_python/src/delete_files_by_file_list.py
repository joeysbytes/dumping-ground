from core import delete_utils
from core import files_db
import os


# Set this to False to physically remove files
SIMULATE = False

# Attributes about the input file, columns are 0-based indexes
INPUT_FILE_NAME = "/tmp2/iso.csv"
FIELD_SEPARATOR = "\t"
FULL_FILE_NAME_COL = 2
FULL_DIR_NAME_COL = 3


def main():
    print("Delete files by file list, simulate = " + str(SIMULATE))
    print("File list: " + INPUT_FILE_NAME)
    files_db.open()
    perform_deletes = confirm_by_user()
    if perform_deletes:
        print("Performing file deletions")
        delete_files()
    else:
        print("Not performing file deletions")
    files_db.close()


def confirm_by_user():
    """Give user initial statistics, and make them confirm to perform deletions."""
    input_file_found = os.path.exists(INPUT_FILE_NAME) and os.path.isfile(INPUT_FILE_NAME)
    if not input_file_found:
        print("Input file not found")
        return False
    else:
        print("Input file found")
        if SIMULATE:
            return False
        else:
            print("\nNote: You must enter YES in all capitals to confirm deletion.")
            confirmation = input("Do you wish to delete these records? ")
            print("")
            if confirmation == "YES":
                return True
            else:
                return False


def delete_files():
    """Read file list and physically delete the files."""
    delete_count = 0
    files_db.begin_transaction()
    with open(INPUT_FILE_NAME) as f:
        for line in f:
            if len(line.strip()) > 0:
                data_parts = line.split(FIELD_SEPARATOR)
                full_file_name = data_parts[FULL_FILE_NAME_COL]
                # TODO: need to remove newline
                full_dir_name = data_parts[FULL_DIR_NAME_COL].strip()
                print(full_file_name + " | " + full_dir_name)
                delete_count += 1
            # delete_utils.delete_file(row[0], row[1])
            # files_db.increment_transaction_counter()
            # files_db.commit_transaction()
    files_db.commit_transaction(force=True, begin_trans=False)
    print("Total files deleted: " + str(delete_count))

main()
