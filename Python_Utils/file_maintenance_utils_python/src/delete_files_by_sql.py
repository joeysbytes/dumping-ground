from core import delete_utils
from core import files_db


# Set this to False to physically remove files
SIMULATE = True

SQL_SELECT = """
             SELECT FULL_NAME, DIR
             FROM FILES
             """
SQL_SELECT_COUNT = """
                   SELECT COUNT(FULL_NAME)
                   FROM FILES
                   """
SQL_ORDER_BY = """
               ORDER BY DIR, FULL_NAME
               """

# Define where clauses to use to select files
empty_files_where_clause = """
                           WHERE SIZE = 0
                           AND TYPE = 'f'
                           """


# State the where clause to use
where_clause = empty_files_where_clause


def main():
    print("Delete files by sql, simulate = " + str(SIMULATE))
    files_db.open()
    perform_deletes = confirm_by_user()
    if perform_deletes:
        print("Performing file deletions")
        delete_files()
    else:
        print("Not performing file deletions")
    files_db.close()


def confirm_by_user():
    """Give user initial statistics, and make them confirm to perform deletions."""
    delete_sql_count = SQL_SELECT_COUNT + where_clause + ";"
    print("\nSQL to select record count for deletion")
    print(delete_sql_count + "\n")
    record_count = files_db.select_single_value(delete_sql_count)
    if record_count == 0:
        print("No records selected to delete")
        return False
    else:
        print("\nRecords selected to delete: " + str(record_count))
        if SIMULATE:
            return False
        else:
            print("\nNote: You must enter YES in all capitals to confirm deletion.")
            confirmation = input("Do you wish to delete these records? ")
            print("")
            if confirmation == "YES":
                return True
            else:
                return False


def delete_files():
    """Run SQL and physically delete the files returned."""
    delete_sql = SQL_SELECT + where_clause + SQL_ORDER_BY + ";"
    print("\nSQL to select records for deletion")
    print(delete_sql + "\n")
    cursor = files_db.select(delete_sql)
    files_db.begin_transaction()
    row = cursor.fetchone()
    while row:
        delete_utils.delete_file(row[0], row[1])
        files_db.increment_transaction_counter()
        files_db.commit_transaction()
        row = cursor.fetchone()
    files_db.commit_transaction(force=True, begin_trans=False)


main()
