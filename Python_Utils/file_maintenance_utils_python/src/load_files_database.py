from core.config import *
from core import files_db
import os


def main():
    print("Base Directory: " + BASE_DIR)
    os.remove(DATABASE_FILE)
    files_db.open()
    files_db.create_tables()
    load_files_db()
    files_db.close()


def load_files_db():
    """Load database with file system information.
    Note: os.walk will return symbolic links in dirs and files, but not follow them.
          You may want to research the symbolic links ahead of time.
    """
    files_db.begin_transaction()
    for base_dir, sub_dirs, file_names in os.walk(BASE_DIR):
        files_db.add_dir(base_dir, len(sub_dirs), len(file_names))
        for file_name in file_names:
            size = 0
            file_type = 'f'
            full_name = os.path.join(base_dir, file_name)
            extension = os.path.splitext(file_name)[1]
            if os.path.islink(full_name):
                file_type = 's'
            else:
                size = os.path.getsize(full_name)
            files_db.add_file(full_name, file_name, extension, base_dir, file_type, size)
        files_db.increment_transaction_counter()
        files_db.commit_transaction()
    files_db.commit_transaction(force=True, begin_trans=False)


main()
