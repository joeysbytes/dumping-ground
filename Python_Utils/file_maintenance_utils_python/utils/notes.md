# Project Notes

* Files and Paths
  * [Python os module](https://docs.python.org/3/library/os.html)
  * [Python os.path module](https://docs.python.org/3/library/os.path.html)
* Database
  * [Sqlite SQL Statements](https://www.sqlite.org/lang.html)
  * [Python Sqlite module](https://docs.python.org/3/library/sqlite3.html)

## Symbolic Links

```bash
# Find symbolic links
find /media/joey/3e6294da-b4f6-4ea5-ad01-11ad2a9aeeff -type l>/tmp2/symlinks.txt

# Delete symbolic links
find /media/joey/3e6294da-b4f6-4ea5-ad01-11ad2a9aeeff -type l -exec rm {} \;
```

## SQL

```sql
-- Find empty directories
select * from directories where num_files = 0 and num_dirs = 0;
select count(*) from directories where num_files = 0 and num_dirs = 0;

-- Find empty files
select * from files where size = 0;
select count(*) from files where size = 0;

```
