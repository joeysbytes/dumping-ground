class Menu():
    """Simple menu class, must provide list of items to display."""
    def __init__(self, items, title=None, prompt="Enter Choice: "):
        self.items = [ str(i) for i in items ]
        self.title = None if title is None else str(title)
        self.prompt = str(prompt)

    def choose(self):
        """Display menu, get user choice, return (list_index, menu_text)."""
        user_choice = None
        while user_choice is None:
            if self.title is not None:
                print(self.title + "\n" + "-" * len(self.title))
            for idx, item in enumerate(self.items):
                print(f"{idx+1:>2}) {item}")
            try:
                temp_choice = int(input("\n" + self.prompt)) - 1
                if temp_choice >= 0 and temp_choice < len(self.items):
                    user_choice = (temp_choice, self.items[temp_choice])
                else:
                    raise ValueError
            except ValueError:
                print("\n*** INVALID CHOICE ***\n")
        return user_choice

    def choose_index(self):
        """Return menu item index chosen by user."""
        return self.choose()[0]

    def choose_item(self):
        """Return menu item text chosen by user."""
        return self.choose()[1]
