# One-Method Menu (one_method_menu.py)

## Author

Joey Rockhold - [Joey's Bytes](https://joeysbytes.net)

## Description

One-Method Menu is a single Python method which provides the following features:

* A fully-functional menu with minimal required parameters, while at the same time providing customization features.
* Gathering user input, and returning the menu index as either as a single value, or a tuple of sorted unique multiple values.

## Installation

There are 2 ways to install One-Method Menu:

* Copy the method menu in to your Python code.
* Copy one_method_menu.py in to your source code directories, and then import the method.

## Usage

One-Method Menu returns the index of the menu item the user selected.  If multiple choices is selected, a tuple of sorted, unique indexes is returned.

You do not necessarily need to provide parameters as strings, though this is highly recommended.  Each parameter is automatically called by the str() function to get the human-readable format for displaying.  The reason for this feature is to save you the work on converting say, a list of numbers, to a string before submitting them to the menu method.

### Parameters

* items - Required:  An iterative object (list, tuple, etc.).  These are the items in the menu for the user to choose from.
* title - Optional:  This is the title of the menu.
* prompt - Optional, default = "Enter choice: ".  This is the text given to the user to enter their choice(s).  Remember to add your own ending symbol and spacing.
* pre_menu_text - Optional:  This text is printed after the title, and before the menu items are printed.
* post_menu_text - Optional:  This text is printed after the menu items, and before the user prompt.
* multiple_choice - Optional, default = False:
  * False = The user can only select 1 menu item, and the index will be returned as an integer.
  * True = The user can select multiple menu items (comma separated), and it will be returned as a tuple of sorted, unique indexes.
  * **Tip:** You can change the value of item_separator in the method if you want to separate on a character other than a comma.
* default_indexes - Optional: This can be an int or an iterable of ints, that will be the values used if the user presses Enter at the prompt line without entering anyhing.  For convenience, the values can be any object that int() can handle.

## Tutorials

**You can get a working menu with nothing more than a list of items to choose from.**

```python
pets = [ "Dog", "Cat", "Goldfish", "Parrot", "Hamster" ]
choice = menu(pets)
print()
print("You are getting a " + pets[choice].lower() + ".")
```

**Result:**

```text
 1) Dog
 2) Cat
 3) Goldfish
 4) Parrot
 5) Hamster

Enter choice: 3

You are getting a goldfish.
```

**You can add a title to your menu.**

```python
pets = [ "Dog", "Cat", "Goldfish", "Parrot", "Hamster" ]
choice = menu(pets, title="Bob's Pet Shop")
print()
print("You are getting a " + pets[choice].lower() + ".")
```

**Result:**

```text
==============
Bob's Pet Shop
==============

 1) Dog
 2) Cat
 3) Goldfish
 4) Parrot
 5) Hamster

Enter choice: 1

You are getting a dog.
```

**This example shows all parts of the menu.**

```python
pets = [ "Dog", "Cat", "Goldfish", "Parrot", "Hamster" ]
menu_title = "Bob's Pet Shop"
before_menu_text = "Choose a pet from the options below."
after_menu_text = "All of these pets come with a 30-day money-back guarantee."
prompt = "Your pet choice > "
choice = menu(pets, title=menu_title, prompt=prompt, pre_menu_text=before_menu_text, post_menu_text=after_menu_text)
print()
print("You are getting a " + pets[choice].lower() + ".")
```

**Result:**

```text
==============
Bob's Pet Shop
==============

Choose a pet from the options below.

 1) Dog
 2) Cat
 3) Goldfish
 4) Parrot
 5) Hamster

All of these pets come with a 30-day money-back guarantee.

Your pet choice > 4

You are getting a parrot.
```

**This example shows how multiple choice works.**

```python
pets = [ "Dog", "Cat", "Goldfish", "Parrot", "Hamster" ]
menu_title = "Bob's Pet Shop"
prompt = "Your pet choice(s): "
choices = menu(pets, title=menu_title, prompt=prompt, multiple_choice=True)
print()
print("You are getting a:")
for choice in choices:
    print("  " + pets[choice].lower())
```

**Result (note the order of the printed results, and that spacing is allowed between commas):**

```text
==============
Bob's Pet Shop
==============

 1) Dog
 2) Cat
 3) Goldfish
 4) Parrot
 5) Hamster

Your pet choice(s): 5, 2,4,   1

You are getting a:
  dog
  cat
  parrot
  hamster
```

**This example shows how to set a menu item as the default.**
```python
pets = [ "Dog", "Cat", "Goldfish", "Parrot", "Hamster" ]
menu_title = "Bob's Pet Shop"
prompt = "Your pet choice(s): "
defaults = 1
choices = menu(pets, title=menu_title, prompt=prompt, multiple_choice=True, default_indexes=defaults)
print()
print("You are getting a:")
for choice in choices:
    print("  " + pets[choice].lower())
```

**Result:**

```text
==============
Bob's Pet Shop
==============

  1) Dog
* 2) Cat
  3) Goldfish
  4) Parrot
  5) Hamster

Your pet choice(s):

You are getting a:
  cat
```

**This example shows how to set multiple menu items as the default.**
```python
pets = [ "Dog", "Cat", "Goldfish", "Parrot", "Hamster" ]
menu_title = "Bob's Pet Shop"
prompt = "Your pet choice(s): "
defaults = [ 0, 3 ]
choices = menu(pets, title=menu_title, prompt=prompt, multiple_choice=True, default_indexes=defaults)
print()
print("You are getting a:")
for choice in choices:
    print("  " + pets[choice].lower())
```

**Result:**

```text
==============
Bob's Pet Shop
==============

* 1) Dog
  2) Cat
  3) Goldfish
* 4) Parrot
  5) Hamster

Your pet choice(s):

You are getting a:
  dog
  parrot
```
