def menu(items, title=None, prompt="Enter choice: ", pre_menu_text=None, post_menu_text=None,
        multiple_choice=False, default_indexes=None):
    """One-Method-Menu: https://gitlab.com/joeysbytes/one-method-python/tree/master/one-method-menu"""
    newline, menu_choices, menu_text, user_selections, item_separator = "\n", None, "\n\n", None, ","
    if len(items) == 0: raise ValueError("No menu items provided")
    if default_indexes is not None:
        if not hasattr(default_indexes, "__iter__"): default_indexes = [ default_indexes ]
        default_indexes = tuple(sorted(set([ int(idx) for idx in default_indexes ])))
        if not multiple_choice: default_indexes = (default_indexes[0],)
    if title is not None: menu_text += ("=" * len(str(title)) + newline + str(title) + newline + "=" * len(str(title)) + newline * 2)
    if pre_menu_text is not None: menu_text += (str(pre_menu_text) + newline * 2)
    for i in range(0, len(items)):
        menu_text += " " if default_indexes is None or i not in default_indexes else "*"
        menu_text += "{:2d}".format(i + 1) + ") " + str(items[i]) + newline
    if post_menu_text is not None: menu_text += (newline + str(post_menu_text) + newline)
    menu_text += (newline + str(prompt if prompt is not None else ""))
    while menu_choices is None:
        try:
            if not multiple_choice:
                user_choice = input(menu_text).strip()
                user_choice = default_indexes[0] + 1 if default_indexes is not None and len(user_choice) == 0 else int(user_choice)
                if user_choice < 1 or user_choice > len(items): raise ValueError
                menu_choices = user_choice - 1
            else:
                user_choices = input(menu_text)
                if default_indexes is not None and len(user_choices.strip()) == 0:
                    user_selections = [ idx + 1 for idx in default_indexes ]
                else:
                    user_selections = [ int(user_choice.strip()) for user_choice in user_choices.split(item_separator) ]
                menu_set = set()
                for user_choice in user_selections:
                    if user_choice < 1 or user_choice > len(items): raise ValueError
                    menu_set.add(user_choice - 1)
                menu_choices = tuple(sorted(list(menu_set)))
        except ValueError: print(newline + "*** INVALID CHOICE(S) ***")
    return menu_choices
