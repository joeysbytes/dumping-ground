import sys
sys.path.append("../src")

from one_method_menu import menu

pets = [ "Dog", "Cat", "Goldfish", "Parrot", "Hamster" ]
menu_title = "Bob's Pet Shop"
prompt = "Your pet choice(s): "
defaults = [ 0, 3 ]
choices = menu(pets, title=menu_title, prompt=prompt, multiple_choice=True, default_indexes=defaults)
print()
print("You are getting a:")
for choice in choices:
    print("  " + pets[choice].lower())
