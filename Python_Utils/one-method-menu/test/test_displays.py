import sys
sys.path.append("../src")
from one_method_menu import menu
from . import testing_utils as utils


class TestOneMethodMenuDisplays():


    #######################################################################
    # TEST MENU ITEMS
    #######################################################################

    # Single menu items

    def test_1_item_list_display(self, capsys, monkeypatch):
        expected_menu = utils.build_expected_menu_text(1)
        monkeypatch.setattr('builtins.input', self._mock_input)
        menu(utils.build_item_list(1))
        self._check_menu_text(capsys, expected_menu)


    def test_1_item_list_display_with_default_index(self, capsys, monkeypatch):
        expected_menu = utils.build_expected_menu_text(1, default_indexes=0)
        monkeypatch.setattr('builtins.input', self._mock_input)
        menu(utils.build_item_list(1), default_indexes=0)
        self._check_menu_text(capsys, expected_menu)


    def test_1_item_list_display_with_default_index_list(self, capsys, monkeypatch):
        expected_menu = utils.build_expected_menu_text(1, default_indexes=[0])
        monkeypatch.setattr('builtins.input', self._mock_input)
        menu(utils.build_item_list(1), default_indexes=[ 0 ])
        self._check_menu_text(capsys, expected_menu)


    def test_1_item_list_display_with_default_index_float(self, capsys, monkeypatch):
        expected_menu = utils.build_expected_menu_text(1, default_indexes=0)
        monkeypatch.setattr('builtins.input', self._mock_input)
        menu(utils.build_item_list(1), default_indexes=0.6)
        self._check_menu_text(capsys, expected_menu)


    # Multiple menu items

    def test_5_item_list_display(self, capsys, monkeypatch):
        """Testing 1 digit menu items."""
        expected_menu = utils.build_expected_menu_text(5)
        monkeypatch.setattr('builtins.input', self._mock_input)
        menu(utils.build_item_list(5))
        self._check_menu_text(capsys, expected_menu)


    def test_5_item_list_display_default_index(self, capsys, monkeypatch):
        """Testing 1 digit menu items."""
        expected_menu = utils.build_expected_menu_text(5, default_indexes=1)
        monkeypatch.setattr('builtins.input', self._mock_input)
        menu(utils.build_item_list(5), default_indexes=1)
        self._check_menu_text(capsys, expected_menu)


    def test_5_item_list_display_default_indexes(self, capsys, monkeypatch):
        """Testing 1 digit menu items."""
        expected_menu = utils.build_expected_menu_text(5, default_indexes=[1, 4])
        monkeypatch.setattr('builtins.input', self._mock_input)
        menu(utils.build_item_list(5), default_indexes=[1, 4])
        self._check_menu_text(capsys, expected_menu)


    def test_12_item_list_display(self, capsys, monkeypatch):
        """Testing 2 digit menu items."""
        expected_menu = utils.build_expected_menu_text(12)
        monkeypatch.setattr('builtins.input', self._mock_input)
        menu(utils.build_item_list(12))
        self._check_menu_text(capsys, expected_menu)


    def test_12_item_list_display_default_indexes_list_single_choice(self, capsys, monkeypatch):
        """Testing 2 digit menu items."""
        expected_menu = utils.build_expected_menu_text(12, default_indexes=[0, 11, 5])
        monkeypatch.setattr('builtins.input', self._mock_input)
        menu(utils.build_item_list(12), default_indexes=[11, 0, 5])
        self._check_menu_text(capsys, expected_menu)


    def test_12_item_list_display_default_indexes_list_multiple_choice(self, capsys, monkeypatch):
        """Testing 2 digit menu items."""
        expected_menu = utils.build_expected_menu_text(12, default_indexes=[0, 11, 5], multiple_choice=True)
        monkeypatch.setattr('builtins.input', self._mock_input)
        menu(utils.build_item_list(12), default_indexes=[11, 0, 5], multiple_choice=True)
        self._check_menu_text(capsys, expected_menu)


    def test_128_item_list_display(self, capsys, monkeypatch):
        """Testing 3 digit menu items."""
        expected_menu = utils.build_expected_menu_text(128)
        monkeypatch.setattr('builtins.input', self._mock_input)
        menu(utils.build_item_list(128))
        self._check_menu_text(capsys, expected_menu)


    #######################################################################
    # TEST MENU TITLE
    #######################################################################

    def test_title_display(self, capsys, monkeypatch):
        title = "Main Menu"
        expected_menu = utils.build_expected_menu_text(5, title=title)
        monkeypatch.setattr('builtins.input', self._mock_input)
        menu(utils.build_item_list(5), title=title)
        self._check_menu_text(capsys, expected_menu)


    def test_empty_title_display(self, capsys, monkeypatch):
        title = ""
        expected_menu = utils.build_expected_menu_text(5, title=title)
        monkeypatch.setattr('builtins.input', self._mock_input)
        menu(utils.build_item_list(5), title=title)
        self._check_menu_text(capsys, expected_menu)


    def test_numeric_title_display(self, capsys, monkeypatch):
        title = 12345
        expected_menu = utils.build_expected_menu_text(5, title=title)
        monkeypatch.setattr('builtins.input', self._mock_input)
        menu(utils.build_item_list(5), title=title)
        self._check_menu_text(capsys, expected_menu)


    def test_none_title_display(self, capsys, monkeypatch):
        title = None
        expected_menu = utils.build_expected_menu_text(5, title=title)
        monkeypatch.setattr('builtins.input', self._mock_input)
        menu(utils.build_item_list(5), title=title)
        self._check_menu_text(capsys, expected_menu)


    #######################################################################
    # TEST MENU PRE-ITEMS TEXT
    #######################################################################

    def test_pre_menu_text_display(self, capsys, monkeypatch):
        pre_menu_text = "Here are your choices"
        expected_menu = utils.build_expected_menu_text(5, pre_menu_text=pre_menu_text)
        monkeypatch.setattr('builtins.input', self._mock_input)
        menu(utils.build_item_list(5), pre_menu_text=pre_menu_text)
        self._check_menu_text(capsys, expected_menu)


    def test_empty_pre_menu_text_display(self, capsys, monkeypatch):
        pre_menu_text = ""
        expected_menu = utils.build_expected_menu_text(5, pre_menu_text=pre_menu_text)
        monkeypatch.setattr('builtins.input', self._mock_input)
        menu(utils.build_item_list(5), pre_menu_text=pre_menu_text)
        self._check_menu_text(capsys, expected_menu)


    def test_numeric_pre_menu_text_display(self, capsys, monkeypatch):
        pre_menu_text = 98765
        expected_menu = utils.build_expected_menu_text(5, pre_menu_text=pre_menu_text)
        monkeypatch.setattr('builtins.input', self._mock_input)
        menu(utils.build_item_list(5), pre_menu_text=pre_menu_text)
        self._check_menu_text(capsys, expected_menu)


    def test_none_pre_menu_text_display(self, capsys, monkeypatch):
        pre_menu_text = None
        expected_menu = utils.build_expected_menu_text(5, pre_menu_text=pre_menu_text)
        monkeypatch.setattr('builtins.input', self._mock_input)
        menu(utils.build_item_list(5), pre_menu_text=pre_menu_text)
        self._check_menu_text(capsys, expected_menu)


    #######################################################################
    # TEST MENU POST-ITEMS TEXT
    #######################################################################

    def test_post_menu_text_display(self, capsys, monkeypatch):
        post_menu_text = "Please make a selection"
        expected_menu = utils.build_expected_menu_text(5, post_menu_text=post_menu_text)
        monkeypatch.setattr('builtins.input', self._mock_input)
        menu(utils.build_item_list(5), post_menu_text=post_menu_text)
        self._check_menu_text(capsys, expected_menu)


    def test_empty_post_menu_text_display(self, capsys, monkeypatch):
        post_menu_text = ""
        expected_menu = utils.build_expected_menu_text(5, post_menu_text=post_menu_text)
        monkeypatch.setattr('builtins.input', self._mock_input)
        menu(utils.build_item_list(5), post_menu_text=post_menu_text)
        self._check_menu_text(capsys, expected_menu)


    def test_numeric_post_menu_text_display(self, capsys, monkeypatch):
        post_menu_text = 98765
        expected_menu = utils.build_expected_menu_text(5, post_menu_text=post_menu_text)
        monkeypatch.setattr('builtins.input', self._mock_input)
        menu(utils.build_item_list(5), post_menu_text=post_menu_text)
        self._check_menu_text(capsys, expected_menu)


    def test_none_post_menu_text_display(self, capsys, monkeypatch):
        post_menu_text = None
        expected_menu = utils.build_expected_menu_text(5, post_menu_text=post_menu_text)
        monkeypatch.setattr('builtins.input', self._mock_input)
        menu(utils.build_item_list(5), post_menu_text=post_menu_text)
        self._check_menu_text(capsys, expected_menu)


    #######################################################################
    # TEST MENU PROMPT TEXT
    #######################################################################

    def test_prompt_text_display(self, capsys, monkeypatch):
        prompt = "Enter your choice from above: "
        expected_menu = utils.build_expected_menu_text(5, prompt=prompt)
        monkeypatch.setattr('builtins.input', self._mock_input)
        menu(utils.build_item_list(5), prompt=prompt)
        self._check_menu_text(capsys, expected_menu)


    def test_empty_prompt_text_display(self, capsys, monkeypatch):
        prompt = ""
        expected_menu = utils.build_expected_menu_text(5, prompt=prompt)
        monkeypatch.setattr('builtins.input', self._mock_input)
        menu(utils.build_item_list(5), prompt=prompt)
        self._check_menu_text(capsys, expected_menu)


    def test_numeric_prompt_text_display(self, capsys, monkeypatch):
        prompt = 6789
        expected_menu = utils.build_expected_menu_text(5, prompt=prompt)
        monkeypatch.setattr('builtins.input', self._mock_input)
        menu(utils.build_item_list(5), prompt=prompt)
        self._check_menu_text(capsys, expected_menu)


    def test_none_prompt_text_display(self, capsys, monkeypatch):
        prompt = None
        expected_menu = utils.build_expected_menu_text(5, prompt=prompt)
        monkeypatch.setattr('builtins.input', self._mock_input)
        menu(utils.build_item_list(5), prompt=prompt)
        self._check_menu_text(capsys, expected_menu)


    #######################################################################
    # TEST EVERYTHING TOGETHER
    #######################################################################

    def test_all_menu_attributes_populated(self, capsys, monkeypatch):
        title = "BOB'S PET SHOP"
        pre_menu_text = "Here is what we have."
        post_menu_text = "Your parents are ok with you getting a new pet."
        prompt = "Pick your next best friend: "
        expected_menu = utils.build_expected_menu_text(5, title=title, prompt=prompt,
                                                       pre_menu_text=pre_menu_text, post_menu_text=post_menu_text,
                                                       default_indexes=[0, 3])
        monkeypatch.setattr('builtins.input', self._mock_input)
        menu(utils.build_item_list(5), title=title, prompt=prompt, pre_menu_text=pre_menu_text, post_menu_text=post_menu_text,
             default_indexes=(0, 3))
        self._check_menu_text(capsys, expected_menu)


    #######################################################################
    # HELPER METHODS
    #######################################################################

    def _check_menu_text(self, capsys, expected_menu, force_print=False):
        """This method will print the full menu text if the assertion is not equal."""
        captured = capsys.readouterr()
        resulting_menu = captured.out
        if (expected_menu != resulting_menu) or (force_print):
            with capsys.disabled():
                print("\nEXPECTED MENU\n" + expected_menu)
                print("\nRESULTING MENU\n" + resulting_menu)
        assert expected_menu == resulting_menu
        assert captured.err == ""


    def _mock_input(self, text):
        """This method is to replace the built-in input method of Python."""
        # so capsys can capture the output from stdout, it doesn't capture output from input function
        print(text, end="")
        # User enters 1
        return "1"
