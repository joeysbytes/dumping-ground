import sys
sys.path.append("../src")

from one_method_menu import menu
import pytest

class TestOneMethodMenuExceptions():

    #######################################################################
    # TEST EXCEPTIONS
    #######################################################################

    def test_none_menu_items_exception(self, capsys):
        """We want the menu to raise an exception if no menu items are presented."""
        with pytest.raises(TypeError):
            menu(None)

    def test_empty_menu_items_exception(self, capsys):
        """We want the menu to raise an exception if no menu items are presented."""
        with pytest.raises(ValueError):
            menu([])

    def test_empty_string_menu_items_exception(self, capsys):
        """We want the menu to raise an exception if no menu items are presented."""
        with pytest.raises(ValueError):
            menu("")

    def test_non_numeric_default_indexes_exception(self, capsys):
        """We want the menu to raise an exception if a non-integer default integer is presented."""
        with pytest.raises(ValueError):
            menu([ "Item 1" ], default_indexes="a")

    def test_string_float_default_indexes_exception(self, capsys):
        """We want the menu to raise an exception if a non-integer default integer is presented."""
        with pytest.raises(ValueError):
            menu([ "Item 1" ], default_indexes="2.5")
