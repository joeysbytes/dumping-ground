import sys
sys.path.append("../src")

from one_method_menu import menu
from . import testing_utils as utils


class TestOneMethodMenuInputs():

    user_inputs = None

    #######################################################################
    # TEST SINGLE CHOICE MENU
    #######################################################################

    def test_valid_item_selected_single_item(self, capsys, monkeypatch):
        self.user_inputs = [ "1" ]
        self._check_choice(capsys, monkeypatch, 1, 0)


    def test_invalid_and_valid_item_selected_single_item(self, capsys, monkeypatch):
        self.user_inputs = [ "0", "-1", "2", "2.1", "", " ", "a", "1" ]
        self._check_choice(capsys, monkeypatch, 1, 0)


    def test_valid_item_selected_multiple_items(self, capsys, monkeypatch):
        for i in range(0, 12):
            self.user_inputs = []
            self.user_inputs.append(str(i + 1))
            self._check_choice(capsys, monkeypatch, 12, i)


    def test_invalid_and_valid_item_selected_multiple_items(self, capsys, monkeypatch):
        for i in range(0, 12):
            self.user_inputs = [ "-1" , "0", "13", "2.1", "-0.3", "a", " ", "" ]
            self.user_inputs.append(str(i + 1))
            self._check_choice(capsys, monkeypatch, 12, i)


    def test_default_item_selected_single_item(self, capsys, monkeypatch):
        self.user_inputs = [ "" ]
        self._check_choice(capsys, monkeypatch, 1, 0, default_indexes=0)


    def test_valid_item_selected_single_item_with_default(self, capsys, monkeypatch):
        self.user_inputs = [ "1" ]
        self._check_choice(capsys, monkeypatch, 1, 0, default_indexes=0)


    def test_default_item_selected_single_item_multiple_defaults(self, capsys, monkeypatch):
        self.user_inputs = [ "" ]
        self._check_choice(capsys, monkeypatch, 1, 0, default_indexes=[0, 1])


    def test_default_item_selected_single_item_multiple_tries(self, capsys, monkeypatch):
        self.user_inputs = [ "2", "3", "" ]
        self._check_choice(capsys, monkeypatch, 1, 0, default_indexes=[0])


    def test_valid_item_selected_single_item_multiple_tries_with_default(self, capsys, monkeypatch):
        self.user_inputs = [ "2", "3", "1" ]
        self._check_choice(capsys, monkeypatch, 1, 0, default_indexes=[0])


    #######################################################################
    # TEST MULTIPLE CHOICE MENU
    #######################################################################

    def test_valid_item_selected_single_item_multiple_choice(self, capsys, monkeypatch):
        self.user_inputs = [ "1" ]
        self._check_choice(capsys, monkeypatch, 1, (0,), multiple_choice=True)


    def test_invalid_and_valid_item_selected_single_item_multiple_choice(self, capsys, monkeypatch):
        self.user_inputs = [ "0", "-1", "a", "2", "1.1", "1" ]
        self._check_choice(capsys, monkeypatch, 1, (0,), multiple_choice=True)


    def test_duplicate_valid_item_selected_single_item_multiple_choice(self, capsys, monkeypatch):
        self.user_inputs = [ "1, 1,1,   1 " ]
        self._check_choice(capsys, monkeypatch, 1, (0,), multiple_choice=True)


    def test_valid_items_selected_multiple_items_multiple_choice(self, capsys, monkeypatch):
        self.user_inputs = [ "5, 1, 12, 3, 7" ]
        self._check_choice(capsys, monkeypatch, 12, (0, 2, 4, 6, 11), multiple_choice=True)


    def test_invalid_and_valid_items_selected_multiple_items_multiple_choice(self, capsys, monkeypatch):
        self.user_inputs = [ "0", "-1", "a", "1.1", "12,11, 10,  5, 4, 12,11,10,5,4, 4,5,10,11,12" ]
        self._check_choice(capsys, monkeypatch, 12, (3, 4, 9, 10, 11), multiple_choice=True)


    def test_valid_items_defaulted_multiple_items_multiple_choice(self, capsys, monkeypatch):
        self.user_inputs = [ "" ]
        self._check_choice(capsys, monkeypatch, 12, (0, 2, 4, 6, 11), multiple_choice=True, default_indexes=[11, 4, 2, 6, 0])


    def test_valid_items_selected_multiple_items_multiple_choice_with_defaults(self, capsys, monkeypatch):
        self.user_inputs = [ "1, 3" ]
        self._check_choice(capsys, monkeypatch, 12, (0, 2), multiple_choice=True, default_indexes=[11, 4, 2, 6, 0])


    def test_valid_items_selected_multiple_items_multiple_choice_with_invalid_defaults(self, capsys, monkeypatch):
        self.user_inputs = [ "", "1, 3" ]
        self._check_choice(capsys, monkeypatch, 12, (0, 2), multiple_choice=True, default_indexes=[15, 2])


    #######################################################################
    # HELPER METHODS
    #######################################################################

    def _check_choice(self, capsys, monkeypatch, menu_size, expected_index, multiple_choice=False, default_indexes=None):
        # print(str(self.user_inputs) + " | " + str(expected_index))
        monkeypatch.setattr('builtins.input', self._mock_input)
        menu_index = menu(utils.build_item_list(menu_size), multiple_choice=multiple_choice, default_indexes=default_indexes)
        assert expected_index == menu_index
        captured = capsys.readouterr()
        assert captured.err == ""


    def _mock_input(self, text):
        """This method is to replace the built-in input method of Python."""
        # so capsys can capture the output from stdout, as it doesn't capture output from input function
        print(text, end="")
        # User enters something
        return self.user_inputs.pop(0)
