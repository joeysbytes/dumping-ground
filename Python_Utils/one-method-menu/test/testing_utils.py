#######################################################################
# TESTING UTILITY METHODS
#######################################################################

newline = "\n"
default_prompt = "Enter choice: "

def build_expected_menu_text(num_items, title=None, prompt=default_prompt,
                             pre_menu_text=None, post_menu_text=None, default_indexes=None,
                             multiple_choice=False):
    menu_text = ""
    # Blank lines before first line of menu text
    menu_text += newline * 2
    # Header of menu
    if title is not None:
        menu_text += "=" * len(str(title)) + newline
        menu_text += str(title) + newline
        menu_text += "=" * len(str(title)) + newline * 2
    # Text before menu items are listed
    if pre_menu_text is not None:
        menu_text += str(pre_menu_text) + newline * 2
    # Menu items
    if default_indexes is not None:
        if not hasattr(default_indexes, "__iter__"):
            default_indexes = [ default_indexes ]
        if not multiple_choice:
            default_indexes = [ default_indexes[0] ]
    for i in range(1, num_items + 1):
        if default_indexes is not None and i - 1 in default_indexes:
            menu_text += "*"
        else:
            menu_text += " "
        if i < 10:
            menu_text += " "
        menu_text += str(i) + ") Item " + str(i) + newline
    # Text after menu items are listed
    if post_menu_text is not None:
        menu_text += newline + str(post_menu_text) + newline
    # Prompt to user
    menu_text += newline
    if prompt is not None:
        menu_text += str(prompt)
    return menu_text


def build_item_list(num_items):
    menu_items = []
    for i in range(1, num_items + 1):
        menu_items.append("Item " + str(i))
    return menu_items
