from core import config
from core import file_utils
from core import requests_utils

from abc import ABC, abstractmethod
import logging
import re

log = logging.getLogger()


class BaseSoftware(ABC):

    VERSION_INT_TYPE_STANDARD = "standard"
    VERSION_INT_TYPE_MAJOR_MINOR_PATH = "major.minor.patch"


    def __init__(self,
                 title,
                 download_dir,
                 downloaded_file_globex,
                 downloaded_version_regex,
                 downloaded_version_regex_group_index=0,
                 version_int_type=VERSION_INT_TYPE_STANDARD,
                 delete_old_downloaded_versions=False,
                 hash_type=None):
        log.debug("Initializing: " + self.__class__.__name__)
        self.title = title
        self.downloaded_version = None
        self.available_version = None
        self.hash_type = hash_type
        self.hash_value = None
        self.download_dir = config.DOWNLOAD_BASE_DIR + "/" + download_dir
        self.downloaded_file_globex = downloaded_file_globex
        self.downloaded_version_regex = downloaded_version_regex
        self.downloaded_version_regex_group_index = downloaded_version_regex_group_index
        self.version_int_type = version_int_type
        self.delete_old_downloaded_versions = delete_old_downloaded_versions


    def __str__(self):
        newline = "\n"
        return ("Title:                                " + str(self.title) + newline +
                "Downloaded Version:                   " + str(self.downloaded_version) + newline +
                "Available Version:                    " + str(self.available_version) + newline +
                "Download Directory:                   " + str(self.download_dir) + newline +
                "Downloaded File Globex:               " + str(self.downloaded_file_globex) + newline +
                "Downloaded Version Regex:             " + str(self.downloaded_version_regex) + newline +
                "Downloaded Version Regex Group Index: " + str(self.downloaded_version_regex_group_index) + newline +
                "Version Int Type:                     " + str(self.version_int_type) + newline +
                "Hash Type:                            " + str(self.hash_type) + newline +
                "Hash Value:                           " + str(self.hash_value) + newline +
                "Delete Old Downloaded Versions:       " + str(self.delete_old_downloaded_versions))


    def process(self):
        """Main logic to get most current version of the software."""
        # Create download directory if it does not exist
        file_utils.create_directory(self.download_dir)
        # Get the version number of already downloaded software
        self.get_downloaded_version()
        # Get the current available version of the software from the web
        log.debug("Getting available version")
        self.get_available_version()
        log.info("Available version: " + str(self.available_version))
        if self.available_version == None:
            raise RuntimeError("Available version of software not found")
        if self.downloaded_version < self.available_version:
            if self.hash_type:
                # Get the hash value of the available version
                log.info("Getting hash value of available version")
                self.get_available_hash_value()
                log.debug("Available hash value: " + str(self.hash_value))
                if self.hash_value == None:
                    raise RuntimeError("Available hash value of software not found")
            # Download the available version
            log.debug("Downloading available version")
            self.download_available_version()
            if self.delete_old_downloaded_versions:
                self.delete_old_versions()
        else:
            log.info("Downloaded and available versions match")


    def get_downloaded_version(self):
        """Go through downloaded files and return the highest version number."""
        if self.downloaded_version is None:
            self.downloaded_version = -1
            downloaded_files = file_utils.get_list_of_files_by_glob(self.download_dir, self.downloaded_file_globex)
            for downloaded_file in downloaded_files:
                downloaded_version = self.get_downloaded_version_from_file_name(downloaded_file)
                if downloaded_version > self.downloaded_version:
                    self.downloaded_version = downloaded_version
        log.info("Downloaded version: " + str(self.downloaded_version))


    def get_downloaded_version_from_file_name(self, file_name):
        """Given a file name, return the version number.

        The version number is retrieved using downloaded_version_regex
        and downloaded_version_regex_group_index.
        The regular expression must use groups, and the group index specified will be returned.
        """
        pattern = re.compile(self.downloaded_version_regex)
        matches = re.findall(pattern, file_name)
        version_number = self.get_version_int(matches[self.downloaded_version_regex_group_index])
        log.debug("File name: " + file_name + ", version number: " + str(version_number))
        return version_number


    def get_version_int(self, version_text):
        """Given a version string, return the integer equivalent.

        This method is controlled by self.version_int_type
        """
        version_number_string = ""
        if self.version_int_type == "standard":
            # just remove all non-digit characters
            for version_char in version_text:
                if version_char.isdigit():
                    version_number_string += version_char
        elif self.version_int_type == "major.minor.patch":
            version_parts = version_text.split(".")
            version_number_string += version_parts[0]
            if len(version_parts) > 1:
                version_number_string += version_parts[1] if len(version_parts[1]) == 2 else "0" + version_parts[1]
            else:
                version_number_string += "00"
            if len(version_parts) > 2:
                version_number_string += version_parts[2] if len(version_parts[2]) == 2 else "0" + version_parts[2]
            else:
                version_number_string += "00"
        else:
            raise RuntimeError("Invalid version int type: " + self.version_int_type)
        return int(version_number_string)


    def download_file(self,
                      url,
                      file_name,
                      make_executable=False,
                      delete_on_error=True):
        """Download a file from a given url.

        If a hash type and value are defined, it will be checked.
        On any errors, the downloaded file will be deleted, unless delete_on_error is set to False.
        """
        output_file_name = self.download_dir + "/" + file_name
        log.info("Downloading url: " + url + ", to file: " + output_file_name)
        try:
            requests_utils.download_file(url, output_file_name)
            if self.hash_type:
                self.validate_hash_value(output_file_name)
            if make_executable:
                file_utils.make_executable(output_file_name)
            self.downloaded_version = self.available_version
        except BaseException as exception:
            log.error("Failed to download file: " + file_name +
                      ", from url: " + url +
                      ", cause: " + repr(exception))
            if delete_on_error:
                file_utils.delete_file(output_file_name)
                if self.hash_type:
                    file_utils.delete_file(output_file_name + "." + self.hash_type)
            raise exception


    def validate_hash_value(self, file_name):
        """Get hash value of downloaded file, and compare it to the expected value.

        If the hash matches, the hash value will be written to the file name,
        with the hash type used as an extension.
        """
        actual_hash_value = file_utils.get_hash_value(file_name, self.hash_type)
        if actual_hash_value != self.hash_value:
            raise RuntimeError("Hash value of downloaded file did not match" +
                                ", file: " + file_name +
                                ", hash type: " + self.hash_type +
                                ", expected hash value: " + self.hash_value,
                                ", actual hash value: " + actual_hash_value)
        hash_file_name = file_name + "." + self.hash_type
        file_utils.write_text_to_file(hash_file_name, self.hash_value + "\n")


    def delete_old_versions(self):
        """Go through download directory and delete old software downloads."""
        if not self.delete_old_downloaded_versions:
            log.info("This software module does not delete old versions.")
        else:
            log.info("Deleting old downloaded software versions")
            downloaded_files = file_utils.get_list_of_files_by_glob(self.download_dir, self.downloaded_file_globex)
            for downloaded_file in downloaded_files:
                version_number = self.get_downloaded_version_from_file_name(downloaded_file)
                if version_number != self.downloaded_version:
                    file_utils.delete_file(self.download_dir + "/" + downloaded_file)
                    if self.hash_type:
                        file_utils.delete_file(self.download_dir + "/" + downloaded_file + "." + self.hash_type)


    @abstractmethod
    def get_available_version(self):
        pass


    def get_available_hash_value(self):
        """If a hash_type is set, this method must be implemented/overriden by the software module."""
        if self.hash_type:
            raise RuntimeError("Hash type is declared, you must implement get_available_hash_value")


    @abstractmethod
    def download_available_version(self):
        pass



