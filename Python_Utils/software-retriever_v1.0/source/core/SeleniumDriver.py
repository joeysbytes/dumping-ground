import logging
from core import config
from selenium import webdriver

log = logging.getLogger()

class SeleniumDriver():
    __instance = None


    @staticmethod
    def get_instance():
        """Return the singleton instance of this SeleniumDriver class."""
        log.debug("Getting instance of SeleniumDriver")
        if SeleniumDriver.__instance == None:
            SeleniumDriver()
        return SeleniumDriver.__instance


    def __init__(self):
        """Initialize the singleton instance of this SeleniumDriver class."""
        log.debug("Creating instance of SeleniumDriver")
        if SeleniumDriver.__instance != None:
            raise Exception("Selenium Driver must be a singleton")
        self.selenium_driver = None
        SeleniumDriver.__instance = self


    def __del__(self):
        """Properly shut down the Selenium driver when this class is destroyed."""
        log.debug("Destroying SeleniumDriver instance")
        if self.selenium_driver is not None:
            log.debug("Quitting Selenium web driver")
            self.selenium_driver.quit()
            self.selenium_driver = None


    def get_web_driver(self):
        log.debug("Getting Selenium web driver")
        if self.selenium_driver is None:
            log.debug("Activating Selenium web driver")
            # activate web driver
            options = webdriver.ChromeOptions()
            if config.WEB_BROWSER_HEADLESS:
                options.set_headless()
            options.binary_location = config.CHROMIUM_BINARY
            options.add_argument("--disable-notifications")
            options.add_argument("--ignore-certificate-errors")
            self.selenium_driver = webdriver.Chrome(chrome_options=options)
        return self.selenium_driver
