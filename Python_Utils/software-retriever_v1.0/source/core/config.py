import logging

# This is the folder where the software modules are stored
SOFTWARE_MODULES_DIR = "software_modules"
# This is the glob of software module file names to load
# For testing, you can make this a single file name
SOFTWARE_MODULES_GLOB = "[A-Za-z]*.py"
#SOFTWARE_MODULES_GLOB = "SystemRescue*.py"

# This is the base directory where downloads are kept
DOWNLOAD_BASE_DIR = "/data/current_software"

# For the Selenium driver, set the browser binary location here
CHROMIUM_BINARY = "/usr/bin/chromium-browser"
# If the web browser supports it, run in headless mode
WEB_BROWSER_HEADLESS = False

# For BeautifulSoup, set the html parser
BEAUTIFUL_SOUP_PARSER = "html5lib"

# Set the level of logging here
LOGGING_LEVEL = logging.INFO
# Set file name for log file
LOGGING_FILE_NAME = None

# Set the buffer size for downloading files
DOWNLOAD_FILE_BUFFER_SIZE = 4096
HASH_FILE_BUFFER_SIZE = 4096


log = None

def initialize():
    """
    Run initialization steps for configuration.
    """
    _set_up_logging()


def _set_up_logging():
    global log
    logging.basicConfig(
        format="%(asctime)s [%(module)s] %(levelname)s: %(message)s",
        datefmt="%Y%m%d_%H%M%S",
#       filename=LOGGING_FILE_NAME,
        level=LOGGING_LEVEL)
    log = logging.getLogger()
    log.debug("Logger initialized")


initialize()
