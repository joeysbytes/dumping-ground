import logging
import glob
import hashlib
from pathlib import Path
from core import config

log = logging.getLogger()

HASH_TYPE_SHA256 = "sha256"
HASH_TYPE_SHA512 = "sha512"
HASH_TYPE_SHA1 = "sha1"
HASH_TYPE_MD5 = "md5"


def get_list_of_files_by_glob(directory, glob_expression, remove_extension=False):
    """Get list of files in a directory matching the glob expression.

    This list will be file names only, the directory is removed from the result items.
    """
    log.debug("Get list of files from directory: " + directory +
              ", matching: " + glob_expression +
              ", remove file extension: " + str(remove_extension))
    file_list = None
    file_names = glob.glob(directory + "/" + glob_expression)
    if remove_extension:
        file_list = [ Path(file_name).stem for file_name in file_names ]
    else:
        file_list = [ Path(file_name).name for file_name in file_names ]
    file_list.sort()
    log.debug("Files found: " + str(file_list))
    return file_list


def create_directory(directory, create_parents=True):
    """Create a directory if it does not already exist."""
    directory_path = Path(directory)
    if not directory_path.exists():
        log.debug("Creating directory: " + directory)
        directory_path.mkdir(parents=create_parents)


def delete_file(file_name):
    """If a file exists, delete it."""
    file_path = Path(file_name)
    if file_path.exists():
        log.debug("Deleting file: " + file_name)
        file_path.unlink()


def touch_file(file_name):
    """Touch a file."""
    log.debug("Touching file: " + file_name)
    Path(file_name).touch()


def make_executable(file_name):
    """Make a file executable."""
    log.debug("Making file executable: " + file_name)
    Path(file_name).chmod(0o775)


def get_hash_value(file_name, hash_type):
    """Calculate the hash value of a file for a given hash type."""
    hash_calculator = None
    if hash_type == HASH_TYPE_SHA256:
        hash_calculator = hashlib.sha256()
    elif hash_type == HASH_TYPE_SHA512:
        hash_calculator = hashlib.sha512()
    elif hash_type == HASH_TYPE_SHA1:
        hash_calculator = hashlib.sha1()
    elif hash_type == HASH_TYPE_MD5:
        hash_calculator = hashlib.md5()
    else:
        raise RuntimeError("Invalid hash type: " + hash_type)
    with open(file_name,"rb") as f:
        for byte_block in iter(lambda: f.read(config.HASH_FILE_BUFFER_SIZE), b""):
            hash_calculator.update(byte_block)
    hash_value = hash_calculator.hexdigest()
    log.debug("File: " + file_name + ", hash type: " + hash_type + ", value: " + hash_value)
    return hash_value


def write_text_to_file(file_name, text):
    """Write given text out to a file."""
    log.debug("(Over)-Writing text to file: " + file_name)
    f = open(file_name, "w")
    f.write(text)
    f.close()
