import logging
import requests
from core import config

log = logging.getLogger()


def get_web_page(url):
    """Given a url, retrieve the web page and return the HTML source."""
    log.debug("Getting web page via requests: " + url)
    r = requests.get(url)
    r.raise_for_status()
    return r.text


def download_file(url, file_name):
    """Download a file from a given url."""
    log.debug("Downloading file: " + file_name + ", from url: " + url)
    r = requests.get(url, stream=True)
    r.raise_for_status()
    with open(file_name, 'wb') as fd:
        for chunk in r.iter_content(chunk_size=config.DOWNLOAD_FILE_BUFFER_SIZE):
            fd.write(chunk)
