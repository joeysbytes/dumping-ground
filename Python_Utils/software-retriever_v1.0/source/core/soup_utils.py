import logging
from core import config
from core import requests_utils
from bs4 import BeautifulSoup

log = logging.getLogger()


def get_soup(url):
    """Get a BeautifulSoup object for a given url."""
    log.debug("Getting BeautifulSoup for url: " + url)
    html_source = requests_utils.get_web_page(url)
    return BeautifulSoup(html_source, config.BEAUTIFUL_SOUP_PARSER)
