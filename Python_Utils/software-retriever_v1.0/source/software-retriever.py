"""This script will go through websites and keep the latest versions of software downloaded."""

from core import config
from core import file_utils

import importlib
import logging

log = logging.getLogger()


def main():
    """The main processing logic to retrieve the latest software."""
    log.info("Begin software retrieval")
    try:
        software_modules = file_utils.get_list_of_files_by_glob(config.SOFTWARE_MODULES_DIR,
                                                                config.SOFTWARE_MODULES_GLOB,
                                                                remove_extension=True)
        for software_module in software_modules:
            process_software_module(software_module)
    except BaseException as exception:
        log.critical("An error occurred processing software modules: " + repr(exception))
        log.exception(exception)
    log.info("End software retrieval")


def process_software_module(software_module):
    """Given a module name, load the module and download the latest software."""
    log.info("Processing software module: " + software_module)
    try:
        software_instance = _instantiate_software_class(software_module)
        software_instance.process()
    except BaseException as exception:
        log.error("An error occurred processing software module: " + software_module + ": " + repr(exception))
        log.exception(exception)
        log.error("The processing will continue to the next software module, if any")


def _instantiate_software_class(software_module):
    """Given a module name, instantiate the specific software class and return it.

    The file name for software module must match the name of the class to instantiate.
    """
    log.debug("Instantiating software module: " + software_module)
    module = importlib.import_module(config.SOFTWARE_MODULES_DIR + "." + software_module)
    SoftwareClass = getattr(module, software_module)
    software_instance = SoftwareClass()
    return software_instance


if __name__ == "__main__":
    main()
