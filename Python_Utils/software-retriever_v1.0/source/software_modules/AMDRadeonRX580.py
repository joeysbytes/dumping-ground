from core import file_utils
from core.BaseSoftware import BaseSoftware
import logging
from core.SeleniumDriver import SeleniumDriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions

log = logging.getLogger()

class AMDRadeonRX580(BaseSoftware):


    def __init__(self):
        super().__init__(title = "AMD Radeon 580 RX Ubuntu 18.04 Graphics Drivers",
                         download_dir = "amd_radeon_rx_580",
                         downloaded_file_globex = "amdgpu-pro-*-ubuntu-18.04*",
                         downloaded_version_regex = r"([0-9]{2}\.[0-9]{2})",
                         version_int_type = BaseSoftware.VERSION_INT_TYPE_MAJOR_MINOR_PATH)


    def get_available_version(self):
        """Find latest version of the AMD Radeon 580 RX drivers available for download."""
        driver = SeleniumDriver.get_instance().get_web_driver()
        log.debug("Loading AMD Radeon RX 580 Drivers web page")
        url = "https://www.amd.com/en/support/graphics/radeon-500-series/radeon-rx-500-series/radeon-rx-580"
        cookies_accept_link_xpath = "//div[@id='cookieCompliance']//a[text()='I understand']"
        driver.get(url)
        # accept cookies
        log.debug("Accepting cookies for web site")
        cookie_accept_link = WebDriverWait(driver, 60).until(expected_conditions.presence_of_element_located((By.XPATH, cookies_accept_link_xpath)))
        cookie_accept_link.click()
        # get driver version
        log.debug("Extracting AMD Radeon RX 580 Driver version from web page")
        ubuntu_heading_xpath = "//summary[text()='Ubuntu x86 64-Bit']"
        ubuntu_heading = driver.find_element(By.XPATH, ubuntu_heading_xpath)
        ubuntu_heading.click()
        ubuntu_version_xpath = "//div[contains(string(),'Driver for Ubuntu 18.04')]/following-sibling::div[@class='driver-metadata']//div[@class='field__item']"
        ubuntu_version = driver.find_element(By.XPATH, ubuntu_version_xpath)
        self.available_version = self.get_version_int(ubuntu_version.text)


    def download_available_version(self):
        """Download available version of the AMD Radeon 580 RX drivers."""
        log.warning("The current code for this software module is unable to download the driver.  An empty file will be created instead.")
        driver = SeleniumDriver.get_instance().get_web_driver()
        download_button_xpath = "//div[contains(string(),'Driver for Ubuntu 18.04')]/following-sibling::div[@class='driver-metadata']//a[text()='Download*']"
        download_button = driver.find_element(By.XPATH, download_button_xpath)
        url = download_button.get_attribute("href")
        file_utils.touch_file(self.download_dir + "/" + url.split("/")[-1])
