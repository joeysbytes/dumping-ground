from core.BaseSoftware import BaseSoftware
import logging
from core import soup_utils

log = logging.getLogger()

class DDWRTLinksysWRT3200ACM(BaseSoftware):


    def __init__(self):
        super().__init__(title = "DD-WRT for Linksys WRT3200ACM",
                         download_dir = "ddwrt_linksys_wrt3200acm",
                         downloaded_file_globex = "ddwrt-linksys-wrt3200acm-webflash-*.bin",
                         downloaded_version_regex = "-r([0-9]{5,})-",
                         delete_old_downloaded_versions=True)


    def get_available_version(self):
        """Find latest version of DD-WRT for Linksys WRT3200ACM available for download."""
        # Start at years page
        log.debug("Go to DD-WRT download page by years")
        starting_url = "https://download1.dd-wrt.com/dd-wrtv2/downloads/betas/"
        soup = soup_utils.get_soup(starting_url)
        latest_year = soup.find_all("a")[-1]["href"]
        # Go to latest year
        log.debug("Go to DD-WRT download page of latest year: " + latest_year[:-1])
        versions_url = starting_url + latest_year
        soup = soup_utils.get_soup(versions_url)
        latest_version = soup.find_all("a")[-1]["href"]
        self.available_version = int(latest_version.split("-")[-1][1:-1])
        self.version_date = latest_version[:10]


    def download_available_version(self):
        """Download available version of DD-WRT for Linksys WRT3200ACM."""
        file_name = ("ddwrt-linksys-wrt3200acm-webflash-r" +
                     str(self.available_version) + "-" +
                     self.version_date + ".bin")
        url = ("https://download1.dd-wrt.com/dd-wrtv2/downloads/betas/" +
               self.version_date[6:] + "/" +
               self.version_date + "-r" + str(self.available_version) +
               "/linksys-wrt3200acm/ddwrt-linksys-wrt3200acm-webflash.bin")
        self.download_file(url, file_name)
