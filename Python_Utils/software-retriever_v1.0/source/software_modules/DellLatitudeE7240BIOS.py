from core.BaseSoftware import BaseSoftware
import logging
from core import soup_utils

log = logging.getLogger()

class DellLatitudeE7240BIOS(BaseSoftware):


    def __init__(self):
        super().__init__(title = "Dell Latitude e7240 Ultrabook BIOS",
                         download_dir = "dell_latitude_e7240_bios",
                         downloaded_file_globex = "E7240A*.exe",
                         downloaded_version_regex = "^E7240A([0-9]{2}).exe$")


    def get_available_version(self):
        """Find latest version of Dell Latitude e7240 BIOS available for download."""
        url = "http://downloads.dell.com/published/pages/latitude-e7240-ultrabook.html#Drivers"
        soup = soup_utils.get_soup(url)
        self.soup = soup
        bios_version = soup.find(id="Drivers-Category.BI-Type.BIOS").table.find_all("tr")[1].find_all("td")[2].get_text()[1:]
        self.available_version = int(bios_version)


    def download_available_version(self):
        """Download available version of the software."""
        soup = self.soup
        url_prefix = "http://downloads.dell.com"
        url_file = soup.find(id="Drivers-Category.BI-Type.BIOS").table.find_all("tr")[1].find_all("td")[5].a["href"]
        url = url_prefix + url_file
        file_name = url_file.split("/")[-1]
        self.download_file(url, file_name)
