import logging
from core.BaseSoftware import BaseSoftware
from core import requests_utils
import re
from core import soup_utils
from core import file_utils

log = logging.getLogger()

class KDENeon(BaseSoftware):


    def __init__(self):
        super().__init__(title = "KDE Neon",
                         download_dir = "kde_neon",
                         downloaded_file_globex = "neon-useredition-*-*-amd64.iso",
                         downloaded_version_regex = "([0-9]{8}-[0-9]{4})",
                         delete_old_downloaded_versions = True,
                         hash_type = file_utils.HASH_TYPE_SHA256)
        self.base_url = "https://files.kde.org/neon/images/neon-useredition/current/"


    def get_available_version(self):
        """Find latest version of KDE Neon available for download."""
        url = self.base_url
        pattern = re.compile("^neon-useredition-[0-9]{8}-[0-9]{4}-amd64.iso$")
        soup = soup_utils.get_soup(url)
        available_iso = soup.table.find(string=pattern)
        self.available_version = self.get_downloaded_version_from_file_name(available_iso)
        self.available_file_name = "neon-useredition-" + str(self.available_version)[0:8] + "-" + str(self.available_version)[8:12] + "-amd64.iso"


    def get_available_hash_value(self):
        """Get the hash value of the currently available version."""
        file_name = self.available_file_name.replace(".iso", ".sha256sum")
        url = self.base_url + file_name
        html_source = requests_utils.get_web_page(url)
        self.hash_value = html_source[0:64]


    def download_available_version(self):
        """Download available version of KDE Neon."""
        file_name = self.available_file_name
        url = self.base_url + file_name
        self.download_file(url, file_name)
