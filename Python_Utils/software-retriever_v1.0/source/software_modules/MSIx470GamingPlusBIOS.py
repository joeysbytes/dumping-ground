from core.BaseSoftware import BaseSoftware
import logging
from core.SeleniumDriver import SeleniumDriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions

log = logging.getLogger()

class MSIx470GamingPlusBIOS(BaseSoftware):


    def __init__(self):
        super().__init__(title = "MSI x470 Gaming Plus BIOS",
                         download_dir = "msi_x470_gaming_plus_bios",
                         downloaded_file_globex = "7B79vA*.zip",
                         downloaded_version_regex = "([0-9]{1,2})(?:.zip{0,1})$")


    def get_available_version(self):
        """Find the latest MSI BIOS version available for download."""
        driver = SeleniumDriver.get_instance().get_web_driver()
        log.debug("Loading MSI BIOS web page")
        url = "https://www.msi.com/Motherboard/support/X470-GAMING-PLUS#down-bios"
        cookies_accept_button_xpath = "//div[@id='ccc-notify']//button[text()='Accept']"
        driver.get(url)
        log.debug("Accepting cookies of MSI BIOS web page")
        cookie_accept_button = WebDriverWait(driver, 60).until(expected_conditions.presence_of_element_located((By.XPATH, cookies_accept_button_xpath)))
        cookie_accept_button.click()
        log.debug("Getting available version from MSI BIOS web page")
        bios_xpath = "//div[@id='collapse-download-AMIBIOS']//div[contains(string(),'Version')]/following-sibling::div[@class='tbody']"
        version = driver.find_element(By.XPATH, bios_xpath)
        self.available_version = int(version.text[6:])


    def download_available_version(self):
        """Download available version of the MSI BIOS."""
        file_name = "7B79vA" + str(self.available_version) + ".zip"
        url = "http://download.msi.com/bos_exe/mb/" + file_name
        self.download_file(url, file_name)
