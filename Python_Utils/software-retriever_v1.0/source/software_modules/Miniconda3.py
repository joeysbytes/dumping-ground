from core.BaseSoftware import BaseSoftware
import logging
from core import soup_utils
import re
from core import file_utils

log = logging.getLogger()

class Miniconda3(BaseSoftware):


    def __init__(self):
        super().__init__(title = "Miniconda 3 for Linux 64-bit",
                         download_dir = "miniconda3",
                         downloaded_file_globex = "Miniconda3-*-Linux-x86_64.sh",
                         downloaded_version_regex = "^Miniconda3-([0-9.]*)-Linux-x86_64.sh$",
                         delete_old_downloaded_versions = True,
                         version_int_type = BaseSoftware.VERSION_INT_TYPE_MAJOR_MINOR_PATH,
                         hash_type=file_utils.HASH_TYPE_MD5)
        self.base_url = "https://repo.anaconda.com/miniconda/"


    def get_available_version(self):
        """Find latest version of Miniconda 3 available for download."""
        soup = soup_utils.get_soup(self.base_url)
        self.soup = soup
        version_table_links = soup.table.find_all("a")
        pattern = re.compile("^Miniconda3-([0-9.]*)-Linux-x86_64.sh$")
        file_names = [ version_table_link["href"] for version_table_link in version_table_links if re.match(pattern, version_table_link["href"])]
        self.available_version = self.get_downloaded_version_from_file_name(file_names[0])


    def get_available_hash_value(self):
        """Get the hash value of the currently available version of Miniconda 3."""
        version_text = (str(self.available_version)[0:1] + "." +
                        str(int(str(self.available_version)[1:3])) + "." +
                        str(int(str(self.available_version)[3:5])))
        self.file_name = "Miniconda3-" + version_text + "-Linux-x86_64.sh"
        soup = self.soup
        version_table_links = soup.table.find_all("a")
        version_link = [ version_table_link for version_table_link in version_table_links if version_table_link["href"] == self.file_name ][0]
        self.hash_value = version_link.parent.parent.find_all("td")[3].get_text()


    def download_available_version(self):
        """Download available version of Miniconda 3."""
        url = self.base_url + self.file_name
        self.download_file(url, self.file_name, make_executable=True)
