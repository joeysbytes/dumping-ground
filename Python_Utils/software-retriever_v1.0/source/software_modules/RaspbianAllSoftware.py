from core.BaseSoftware import BaseSoftware
import logging
from core import soup_utils
from core import file_utils

log = logging.getLogger()

class RaspbianAllSoftware(BaseSoftware):


    def __init__(self):
        super().__init__(title = "Raspian Desktop with Recommended Software",
                         download_dir = "raspbian_all_software",
                         downloaded_file_globex = "*-raspbian-*-full.zip",
                         downloaded_version_regex = "^([0-9-]{10})-raspbian",
                         delete_old_downloaded_versions = True,
                         hash_type = file_utils.HASH_TYPE_SHA256)


    def get_available_version(self):
        """Find latest version of the software available for download."""
        url = "https://www.raspberrypi.org/downloads/raspbian/"
        soup = soup_utils.get_soup(url)
        self.soup = soup
        span_tags = soup.find(class_="downloads").find_all("span")
        version_text = [ span_tag for span_tag in span_tags if span_tag.get_text() == "Release date:" ][0].parent.strong.get_text()
        self.available_version = self.get_version_int(version_text)


    def get_available_hash_value(self):
        """Get the hash value of the currently available version."""
        soup = self.soup
        self.hash_value = soup.find(class_="downloads").find_all(class_="sha256")[0].strong.get_text()


    def download_available_version(self):
        """Download available version of the software."""
        # Get Debian version name that Raspbian is based on
        soup = self.soup
        debian_version_name = soup.find(class_="downloads").h3.get_text().split(" ")[1].lower()
        log.debug("Debian version name: " + debian_version_name)
        # Download file
        version = str(self.available_version)
        file_name = version[0:4] + "-" + version[4:6] + "-" + version[6:8] + "-raspbian-" + debian_version_name + "-full.zip"
        url = "https://downloads.raspberrypi.org/raspbian_full_latest"
        self.download_file(url, file_name)
