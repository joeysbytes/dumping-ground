from core.BaseSoftware import BaseSoftware
import logging
from core import soup_utils
from core import file_utils

log = logging.getLogger()

class SystemRescueCD(BaseSoftware):


    def __init__(self):
        super().__init__(title = "System Rescue CD",
                         download_dir = "system_rescue_cd",
                         downloaded_file_globex = "systemrescuecd-*.iso",
                         downloaded_version_regex = "^systemrescuecd-([0-9.]{5,}).iso$",
                         version_int_type = BaseSoftware.VERSION_INT_TYPE_MAJOR_MINOR_PATH,
                         delete_old_downloaded_versions = True,
                         hash_type = file_utils.HASH_TYPE_SHA256)


    def get_available_version(self):
        """Find latest version of the software available for download."""
        url = "http://www.system-rescue-cd.org/Download/"
        soup = soup_utils.get_soup(url)
        self.soup = soup
        table_cells = soup.table.find_all("td")
        download_link_label = [ table_cell for table_cell in table_cells if table_cell.get_text() == "Download link" ][0]
        self.url = download_link_label.parent.a["href"]
        self.file_name = self.url.split("/")[-1]
        self.available_version = self.get_downloaded_version_from_file_name(self.file_name)


    def get_available_hash_value(self):
        """Get the hash value of the currently available version."""
        soup = self.soup
        table_cells = soup.table.find_all("td")
        sha256sum_label = [ table_cell for table_cell in table_cells if table_cell.get_text() == "sha256sum" ][0]
        self.hash_value = sha256sum_label.parent.code.get_text()


    def download_available_version(self):
        """Download available version of the software."""
        file_name = self.file_name
        download_page_url = self.url
        # Go to download page
        log.debug("Going to download page")
        soup = soup_utils.get_soup(download_page_url)
        url = "https://osdn.net" + soup.p.a["href"]
        self.download_file(url, file_name)
