from core.BaseSoftware import BaseSoftware
import logging
from core import soup_utils
from core import file_utils
from core import requests_utils

log = logging.getLogger()

class UbuntuMateLTS(BaseSoftware):


    def __init__(self):
        super().__init__(title = "Ubuntu Mate LTS",
                         download_dir = "ubuntu_mate_lts",
                         downloaded_file_globex = "ubuntu-mate-*-desktop-amd64.iso",
                         downloaded_version_regex = "mate-([0-9.]{7,8})-desktop",
                         delete_old_downloaded_versions = True,
                         version_int_type = BaseSoftware.VERSION_INT_TYPE_MAJOR_MINOR_PATH,
                         hash_type = file_utils.HASH_TYPE_SHA256)


    def get_available_version(self):
        """Find latest version of the software available for download."""
        # Go to Ubuntu releases page
        log.debug("Going to Ubuntu releases main page")
        starting_url = "http://releases.ubuntu.com/"
        soup = soup_utils.get_soup(starting_url)
        version_links = soup.ul.find_all("a")
        version_link = [ version_link for version_link in version_links if "LTS" in version_link.get_text() ][0]
        version_text = version_link.get_text().split(" ")[1].strip()
        # Go to LTS release page
        log.debug("Going to Ubuntu Mate releases page")
        self.mate_url = "http://cdimage.ubuntu.com/ubuntu-mate/releases/" + version_text + "/release/"
        soup = soup_utils.get_soup(self.mate_url)
        self.soup = soup
        version_links = soup.table.find_all("a")
        self.file_name = [ version_link for version_link in version_links if (version_link.get_text() == "ubuntu-mate-" + version_text +"-desktop-amd64.iso") ][0].get_text()
        self.available_version = self.get_downloaded_version_from_file_name(self.file_name)


    def get_available_hash_value(self):
        """Get the hash value of the currently available version."""
        file_name = self.file_name
        sha256sum_url = self.mate_url + "SHA256SUMS"
        sha256_page_source = requests_utils.get_web_page(sha256sum_url)
        for sha256_line in sha256_page_source.splitlines():
            if "ubuntu-mate-" in sha256_line and sha256_line.endswith("-desktop-amd64.iso") and file_name in sha256_line:
                self.hash_value = sha256_line[0:64]
                break


    def download_available_version(self):
        """Download available version of the software."""
        file_name = self.file_name
        url = self.mate_url + file_name
        self.download_file(url, file_name)
