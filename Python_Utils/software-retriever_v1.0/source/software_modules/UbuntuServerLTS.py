from core.BaseSoftware import BaseSoftware
import logging
from core import soup_utils
from core import requests_utils
from core import file_utils

log = logging.getLogger()

class UbuntuServerLTS(BaseSoftware):


    def __init__(self):
        super().__init__(title = "Ubuntu Server LTS",
                         download_dir = "ubuntu_server_lts",
                         downloaded_file_globex = "ubuntu-*-live-server-amd64.iso",
                         downloaded_version_regex = "-([0-9\\.]{5,8})-",
                         delete_old_downloaded_versions = True,
                         version_int_type = BaseSoftware.VERSION_INT_TYPE_MAJOR_MINOR_PATH,
                         hash_type = file_utils.HASH_TYPE_SHA256)


    def get_available_version(self):
        """Find latest version of Ubuntu Server LTS available for download."""
        # Go to main releases page
        log.debug("Going to Ubuntu releases main page")
        starting_url = "http://releases.ubuntu.com/"
        soup = soup_utils.get_soup(starting_url)
        version_links = soup.ul.find_all("a")
        version_link = [ version_link for version_link in version_links if "LTS" in version_link.get_text() ][0]
        # Go to LTS release page
        log.debug("Going to Ubuntu LTS release page")
        self.lts_url = starting_url + version_link["href"]
        soup = soup_utils.get_soup(self.lts_url)
        self.soup = soup
        version_links = soup.table.find_all("a")
        self.file_name = [ version_link for version_link in version_links if (version_link.get_text().startswith("ubuntu-")) and (version_link.get_text().endswith("-live-server-amd64.iso")) ][0].get_text()
        self.available_version = self.get_downloaded_version_from_file_name(self.file_name)


    def get_available_hash_value(self):
        """Get the hash value of the currently available version."""
        sha256_url = self.lts_url + "SHA256SUMS"
        sha256_page_source = requests_utils.get_web_page(sha256_url)
        for sha256_line in sha256_page_source.splitlines():
            if "ubuntu-" in sha256_line and sha256_line.endswith("-live-server-amd64.iso"):
                self.hash_value = sha256_line[0:64]
                break


    def download_available_version(self):
        """Download available version of Ubuntu Server LTS software."""
        file_name = self.file_name
        url = self.lts_url + file_name
        self.download_file(url, file_name)
