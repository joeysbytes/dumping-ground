from core.BaseSoftware import BaseSoftware
import logging
from core import soup_utils
from core import file_utils

log = logging.getLogger()

class RetroPie(BaseSoftware):


    def __init__(self):
        super().__init__(title = "RetroPie",
                         download_dir = "retropie_pi_2_3",
                         downloaded_file_globex = "retropie-*-rpi2_rpi3.img.gz",
                         downloaded_version_regex = "^retropie-([0-9\\.]{3,})-rpi2_rpi3.img.gz$",
                         version_int_type = BaseSoftware.VERSION_INT_TYPE_MAJOR_MINOR_PATH,
                         delete_old_downloaded_versions = True,
                         hash_type = file_utils.HASH_TYPE_MD5)


    def get_available_version(self):
        """Find latest version of the software available for download."""
        url = "https://retropie.org.uk/download/"
        soup = soup_utils.get_soup(url)
        self.soup = soup
        self.download_url = soup.find(id="post-10").select('a[href$="retropie-4.4-rpi2_rpi3.img.gz"]')[0].get("href")
        self.available_version = self.get_downloaded_version_from_file_name(self.download_url.split("/")[-1])


    def get_available_hash_value(self):
        """Get the hash value of the currently available version."""
        soup = self.soup
        md5sum_tag = soup.find(id="post-10").select('a[href$="retropie-4.4-rpi2_rpi3.img.gz"]')[0].parent.tt
        self.hash_value = md5sum_tag.get_text().split(":")[-1].strip()


    def download_available_version(self):
        """Download available version of the software."""
        url = self.download_url
        file_name = url.split("/")[-1]
        #self.download_file(url, file_name)
