from core.BaseSoftware import BaseSoftware
import logging
from core import soup_utils
from core import file_utils

log = logging.getLogger()

class SoftwareTemplate(BaseSoftware):


    def __init__(self):
        super().__init__(title = "Software Template",
                         download_dir = "software_template",
                         downloaded_file_globex = "*.zip",
                         downloaded_version_regex = "$")


    def get_available_version(self):
        """Find latest version of the software available for download."""


    def get_available_hash_value(self):
        """Get the hash value of the currently available version."""


    def download_available_version(self):
        """Download available version of the software."""
        file_name = ".zip"
        url = "http://" + file_name
        # self.download_file(url, file_name)
