# TODO

## To Fix

* AMDRadeonRX580 - can't download driver
* RetroPie accidentally has hard coded versioning

## Must-Haves

* document software module parameters
* scan logs for errors, email it if needed
* move common logging to process method in BaseSoftware

## Nice-to-Haves

* Debian
* Clonezilla (usb or iso image?)
* Multi-boot USB - http://multibootusb.org/
* FreeDOS
* Install to internal website
* Add validations to input parameters (not necessarily required, exceptions are handled)
* Add unit testing
* documentation
* requirements.txt, packaging

## Requirements

* Python 3.5 (to work on Raspbian)
* selenium
* requests
* bs4
  * html5lib
* chromium-browser installed
* chromium-chromedriver installed and on path

## Useful websites

* [Python Regular Expression Tester](https://pythex.org/)
* [HTML Formatter](https://htmlformatter.com/)
* [Beautiful Soup](https://www.crummy.com/software/BeautifulSoup/bs4/doc/)
* [Beautiful Soup Cheatsheet](http://akul.me/blog/2016/beautifulsoup-cheatsheet/)
