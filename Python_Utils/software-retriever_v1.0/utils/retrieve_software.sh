#!/bin/bash

HOST_NAME=`hostname`
ENVIRONMENT_NAME="software-retriever-env"
CHROMIUM_DRIVER_PATH="/usr/lib/chromium-browser"

export PATH=$PATH:${CHROMIUM_DRIVER_PATH}

if [ "${HOST_NAME}" == "rpi_b" ]
then
    # production environment
    SOURCE_DIR="/work/software-retriever/source"
    ENVIRONMENT_DIR="/work/python-environments/software-retriever-env/bin"
    cd "${ENVIRONMENT_DIR}"
    source activate
    cd "${SOURCE_DIR}"
    python software-retriever.py
    deactivate
else
    # development environment
    SOURCE_DIR="/work/joey/development/software-retriever/source"
    source activate "${ENVIRONMENT_NAME}"
    cd "${SOURCE_DIR}"
    python software-retriever.py
    conda deactivate
fi
