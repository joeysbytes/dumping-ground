#!/bin/bash

# QEMU version: 5.0

# Variables for developer workstation
base_dir="/work/qemu_vms/developer_workstation"
install_iso="/home/joey/Downloads/iso/manjaro-cinnamon-20.0.1-minimal-200511-linux56.iso"
cd_image=""  # empty string is ok for mounting
hd1_image="${base_dir}/developer_workstation.qcow2"
hd1_image_size_gb=50
hd2_image="${base_dir}/developer_workstation_data.qcow2"
hd2_image_size_gb=50
boot_order="cd"
forward_ssh_port=7622  # ssh to localhost:7622

# These are set correctly in the script below
memory=-1
num_cpus=-1


###########################################################################
# MAIN LOGIC TO RUN VIRTUAL MACHINE
###########################################################################
function main() {
    echo "============================="
    echo "    DEVELOPER WORKSTATION    "
    echo "============================="
    echo "Base working directory: ${base_dir}"
    cd "${base_dir}"
    set_memory
    set_cpus
    check_for_hd
    run_developer_workstation
}


#--------------------------------------------------------------------------
# If the hard drive images are missing, we assume we want to install
# a new machine.  Adjust variables accordingly.
#--------------------------------------------------------------------------
function check_for_hd() {
    if [ ! -f "${hd1_image}" ]
    then
        # hard drive image not found, assuming we want to install new from CD
        set -e
        echo "Creating new hard drive images for installation"
        qemu-img create -f qcow2 "${hd1_image}" "${hd1_image_size_gb}G"
        qemu-img create -f qcow2 "${hd2_image}" "${hd2_image_size_gb}G"
        cd_image="${install_iso}"
        boot_order="dc"
        set +e
    fi
    echo "Hard drive image 1: ${hd1_image}"
    echo "Hard drive image 2: ${hd2_image}"
}


#--------------------------------------------------------------------------
# Set the amount of memory for the workstation
#--------------------------------------------------------------------------
function set_memory() {
    local total_mem_in_kb=$(cat /proc/meminfo|grep MemTotal|cut -f2 -d:|cut -f1 -dk)
    local mem_in_gb=$(( ${total_mem_in_kb} / 1024 /1024 ))
    if [ ${mem_in_gb} -ge 20 ]
    then
        mem_in_gb=16
    else
        mem_in_gb=8
    fi
    memory=$(( ${mem_in_gb} * 1024 ))
    echo "Memory in MB: ${memory}"
}


#--------------------------------------------------------------------------
# Set the number of cpus for the workstation
#--------------------------------------------------------------------------
function set_cpus() {
    local total_cpus=$(cat /proc/cpuinfo|grep processor|wc -l)
    if [ ${total_cpus} -ge 6 ]
    then
        num_cpus=4
    else
        num_cpus=2
    fi
    echo "Number of CPUs: ${num_cpus}"
}


#--------------------------------------------------------------------------
# Start the virtual machine
#--------------------------------------------------------------------------
function run_developer_workstation() {
    qemu-system-x86_64 \
        -name Developer_Workstation \
        -enable-kvm \
        -machine type=pc,accel=kvm \
        -cpu host \
        -smp cpus=${num_cpus} \
        -m size=${memory} \
        -vga virtio \
        -display sdl,gl=on \
        -audiodev pa,id=pa \
        -soundhw ac97 \
        -nic user,hostfwd=tcp::${forward_ssh_port}-:22 \
        -usb \
        -device usb-tablet \
        -device usb-kbd \
        -hda "${hd1_image}" \
        -hdb "${hd2_image}" \
        -cdrom "${cd_image}" \
        -boot order="${boot_order}"
}


main
