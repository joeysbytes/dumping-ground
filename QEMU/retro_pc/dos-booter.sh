#!/bin/bash
set -e

# Run QEMU i386 for DOS
# QEMU version 3.1 (Raspberry Pi OS)

############################################################################
# Computer Attributes
############################################################################
# This name is used by QEMU
VM_NAME="MSDOS-6.22"
# Set to empty string to ignore drive
# Floppy Drives
# FLOPPY_A="/data/floppy-box/pcdos-3.30/Disk01.img"
# FLOPPY_A="/data/floppy-box/msdos-6.22/Disk1.img"
FLOPPY_A=""
FLOPPY_B=""
# Hard Drives, If CD-Rom used, cannot use hdc
# HARDDRIVE_C="/work/qemu-vms/msdos-6.22-1MB/drive_c.qcow2"
HARDDRIVE_C="/dev/sdb"
HARDDRIVE_D=""
HARDDRIVE_E=""
HARDDRIVE_F=""
# CD Rom Drive, if used is assigned to hdc
CDROM_E=""
# The drive to boot from, must be lower case, a=floppy, c=hard drive, d=cdrom
BOOT_DRIVE="c"
# How much memory in MB to use
RAM_MB="1"
# Set these values to "true" for audio capabilities
PC_SPEAKER="true"
ADLIB="false"
SOUNDBLASTER_16="false"


############################################################################
# Build / Run Computer
############################################################################
function main() {
    command=""
    comma_command=""
    base_computer
    computer_audio
    computer_drives
    # TODO: computer_video
    # TODO: display
    # TODO: serial port?
    # TODO: mouse (serial or ps/2)?
    # TODO: mount physical devices as drives
    echo ""
    echo "Executing: ${command}"
    echo ""
    eval "${command}"
}


# The base computer setup
function base_computer() {
    append_cmd "qemu-system-i386"
    append_cmd "-name ${VM_NAME}"
    # Motherboard / computer type
    append_cmd "-machine pc"
    # CPU type, 486 CPU does not work
    append_cmd "-cpu pentium"
    # Set CPU SMP properties
    comma_command=""
    append_comma "cpus=1"
    append_comma "cores=1"
    append_comma "threads=1"
    append_comma "sockets=1"
    append_comma "maxcpus=1"
    append_cmd "-smp ${comma_command}"
    # Amount of ram
    append_cmd "-m size=${RAM_MB}"
    # Force keyboard type
    append_cmd "-k en-us"
    # localhost base time is not working, living with utc clock
    append_cmd "-rtc base=utc"
    append_cmd "-no-acpi"
}


# The sound capabilities
function computer_audio() {
    comma_command=""
    if [ "${PC_SPEAKER}" == "true" ]
    then
        append_comma "pcspk"
    fi
    if [ "${ADLIB}" == "true" ]
    then
        append_comma "adlib"
    fi
    if [ "${SOUNDBLASTER_16}" == "true" ]
    then
        append_comma "sb16"
    fi
    if [ ! "${comma_command}" == "" ]
    then
        append_cmd "-soundhw ${comma_command}"
    fi
}


# Attach all the possible drives, and set the boot order
function computer_drives() {
    append_cmd "-boot order=${BOOT_DRIVE}"
    mount_drive "${FLOPPY_A}" "0" "floppy" "disk"
    mount_drive "${FLOPPY_B}" "1" "floppy" "disk"
    mount_drive "${HARDDRIVE_C}" "0" "ide" "disk"
    mount_drive "${HARDDRIVE_D}" "1" "ide" "disk"
    mount_drive "${HARDDRIVE_E}" "2" "ide" "disk"
    mount_drive "${HARDDRIVE_F}" "3" "ide" "disk"
    mount_drive "${CDROM_E}" "2" "ide" "cdrom"
}


# Attach a drive
#   1 - Disk Image
#   2 - Drive Index
#   3 - Interface Type
#   4 - Media Type
function mount_drive() {
    local disk_image="${1}"
    local drive_index="${2}"
    local interface="${3}"
    local media="${4}"
    if [ ! "${disk_image}" == "" ]
    then
        comma_command=""
        append_comma "file=\"${disk_image}\""
        append_comma "index=${drive_index}"
        append_comma "if=${interface}"
        append_comma "media=${media}"
        local disk_ext="${disk_image##*.}"
        if [[ "${disk_image}" = "/dev/"* ]]
        then
            disk_ext="raw"
        fi
        case "${disk_ext}" in
          "img" | "IMG" | "raw")
            append_comma "format=raw"
            ;;
          "qcow2" | "QCOW2")
            append_comma "format=qcow2"
            ;;
        esac
        append_cmd "-drive ${comma_command}"
    fi
}


# Helper function to join QEMU command parts
function append_cmd() {
    if [ "${command}" == "" ]
    then
        command="${1}"
    else
        command="${command} ${1}"
    fi
}


# Helper function to string attributes together which are
# separated by commas.
function append_comma() {
    if [ "${comma_command}" = "" ]
    then
        comma_command="${1}"
    else
        comma_command="${comma_command},${1}"
    fi
}


main
