############################################################################
# The start of the program.
############################################################################
def main():
    """Main processing logic."""
    print()
    try:
        main_menu()
    except KeyboardInterrupt as ki:
        print("\nForced Quit.")
    print()


def main_menu():
    """The main menu."""
    menu_items = ["Create Floppy Disk Image", "Create Hard Disk Image", "Format Disk Image",
                  "Mount Disk Image", "Unmount Disk Image", "Quit"]
    menu = Menu(menu_items, "QEMU Utilities")
    choice = menu.choose_item()
    while True:
        print()
        if choice == "Quit":
            print("Quitting.")
            break
        elif choice == "Create Floppy Disk Image":
            create_floppy_disk_image()
        elif choice == "Create Hard Disk Image"
            create_hard_disk_image()
        else:
            print("*** Not Implemented: " + choice)
        print()
        choice = menu.choose_item()


############################################################################
# Create disk images
############################################################################
def create_floppy_disk_image():
    """Prompt user for disk type and file name, to create a raw floppy disk image."""
    # Choose disk size
    menu_items = ["180 Kb  - 5.25\" Single-Sided Double-Density     (SSDD)",
                  "360 Kb  - 5.25\" Double-Sided Double-Density     (DSDD)",
                  "1.2 Mb  - 5.25\" Double-Sided High-Density       (DSHD)",
                  "720 Kb  - 3.5\"  Double-Sided Double-Density     (DSDD)",
                  "1.44 Mb - 3.5\"  Double-Sided High-Density       (DSHD)",
                  "2.88 Mb - 3.5\"  Double-Sided Extra-High-Density (DSED)"
                  "Back to Main Menu"]
    # This must be in the same order as the menu
    floppy_sizes = ["180K", "360K", "1200K", "720K", "1440K", "2880K"]
    menu = Menu(menu_items, "Choose A Disk Size")
    user_choice = menu.choose()
    if user_choice[1] != "Back to Main Menu":
        # Get the file name to create
        print()
        floppy_size = floppy_sizes[user_choice[0]]
        file_name = get_user_file_name(False, ".img")
        if file_name is not None:
            # Make the floppy image
            create_disk_image(file_name, floppy_size, "raw")


def create_hard_disk_image():
    """Create hard disk image in either qcow2 or raw format."""
    # K, M, G, T, qcow2 or raw
    pass

############################################################################
# QEMU Commands
############################################################################
def create_disk_image(file_name, size, fmt):
    print("\nCreating disk image")
    print(f"  File name: {file_name}")
    print(f"  Size:      {size}")
    print(f"  Format:    {fmt}")
    command_string = f"qemu-img create -f {fmt} \"{file_name}\" {size}"
    command_args = ["qemu-img", "create", "-f", fmt, file_name, size]
    execute_command(command_string, command_args)


def execute_command(command_string, command_args):
    """command_string is solely for displaying, command_args is executed."""
    import subprocess
    print(f"\nExecuting command: {command_string}")
    completed_process = subprocess.run(command_args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    if completed_process.returncode == 0:
        print("Success.")
    else:
        print(f"*** Failed with return code: {completed_process.returncode}")
        print(f"STDOUT:\n{completed_process.stdout}")
        print(f"STDERR:\n{completed_process.stderr}")


############################################################################
# Utilities
############################################################################
class Menu():
    """Simple menu class, must provide list of items to display."""
    def __init__(self, items, title=None, prompt="Enter Choice: "):
        self.items = [ str(i) for i in items ]
        self.title = None if title is None else str(title)
        self.prompt = str(prompt)

    def choose(self):
        """Display menu, get user choice, return (list_index, menu_text)."""
        user_choice = None
        while user_choice is None:
            if self.title is not None:
                print(self.title + "\n" + "-" * len(self.title))
            for idx, item in enumerate(self.items):
                print(f"{idx+1:>2}) {item}")
            try:
                temp_choice = int(input("\n" + self.prompt)) - 1
                if temp_choice >= 0 and temp_choice < len(self.items):
                    user_choice = (temp_choice, self.items[temp_choice])
                else:
                    raise ValueError
            except ValueError:
                print("\n*** INVALID CHOICE ***\n")
        return user_choice

    def choose_index(self):
        """Return menu item index chosen by user."""
        return self.choose()[0]

    def choose_item(self):
        """Return menu item text chosen by user."""
        return self.choose()[1]


def get_user_file_name(should_exist, recommended_extension):
    """Get a file name from the user, None is returned if the user cancels."""
    from pathlib import Path
    # type quit to exit
    file_name = None
    while file_name is None:
        print("Enter a file name, with extension:")
        print(f"  Should exist:          {should_exist}")
        print("  Current directory:     " + str(Path.cwd()))
        print(f"  Recommended extension: {recommended_extension}")
        print("Type 'quit' to exit.\n")
        user_file_name = input("Enter file name: ")
        try:
            if user_file_name.lower() == "quit":
                break
            else:
                user_file = Path(user_file_name)
                if should_exist:
                    if not user_file.exists():
                        print("\n*** File does not exist.\n")
                    else:
                        file_name = user_file_name
                else:
                    if user_file.exists():
                        print("\n*** File already exists.\n")
                    else:
                        file_name = user_file_name
        except Exception as e:
            print("*** AN UNEXPECTED ERROR OCCURRED ***")
            print(e)
    return file_name


############################################################################

if __name__ == "__main__":
    main()
