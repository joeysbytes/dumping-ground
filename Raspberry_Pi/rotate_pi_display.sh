#!/bin/bash
set -e

# This script will flip-flop the display rotation between horizontal
# and vertical rotation.


###########################################################################
# Global Variables / Constants
###########################################################################
CONFIG_FILE="/boot/config.txt"
BACKUP_DIR="/data/backups/config"
ENTRY_KEY="display_rotate"
HORIZONTAL_ROTATION="0"
VERTICAL_ROTATION="3"
CURRENT_ROTATION="?"


###########################################################################
# Main Processing Logic
###########################################################################
function main() {
    echo ""
    echo "Config File: "${CONFIG_FILE}
    backup_config_file
    get_current_value
    show_current_value
    change_value
    get_current_value
    show_current_value
    echo ""
    ask_for_reboot
}


###########################################################################
# Backup Configuration File
###########################################################################
function backup_config_file() {
    mkdir -p "${BACKUP_DIR}"
    local timestamp=$(date +%Y%m%d_%H%M%S)
    local cfg_file=$(basename "${CONFIG_FILE}")
    local cfg_file_base="${cfg_file%.*}"
    local cfg_file_ext="${cfg_file##*.}"
    local backup_file="${cfg_file_base}_${timestamp}.${cfg_file_ext}"
    local backup_file_full="${BACKUP_DIR}/${backup_file}"
    echo "Backing up to file: ${backup_file_full}"
    cp "${CONFIG_FILE}" "${backup_file_full}"
}


###########################################################################
# Show the current display rotation value in the configuration file
###########################################################################
function show_current_value() {
    local rotation_desc=""
    if [ "${CURRENT_ROTATION}" == "${HORIZONTAL_ROTATION}" ]
    then
        rotation_desc="(Horizontal - 0 degrees)"
    elif [ "${CURRENT_ROTATION}" == "${VERTICAL_ROTATION}" ]
    then
        rotation_desc="(Vertical - 270 degrees)"
    else
        # we should never get here
        rotation_desc="(*** Unknown ***)"
    fi
    echo "Current Rotation: ${CURRENT_ROTATION} ${rotation_desc}"
}


###########################################################################
# Set CURRENT_ROTATION to the value in the configuration file
###########################################################################
function get_current_value() {
    CURRENT_ROTATION=$(cat "${CONFIG_FILE}"|grep "${ENTRY_KEY}"|cut -f2 -d=)
}


###########################################################################
# Change display rotation from horizontal to vertical or vice-versa
###########################################################################
function change_value() {
    # Figure out new value
    local new_rotation=""
    if [ "${CURRENT_ROTATION}" == "${HORIZONTAL_ROTATION}" ]
    then
        new_rotation="${VERTICAL_ROTATION}"
    elif [ "${CURRENT_ROTATION}" == "${VERTICAL_ROTATION}" ]
    then
        new_rotation="${HORIZONTAL_ROTATION}"
    else
        # we should never get here, but want to set a safe default
        new_rotation="${HORIZONTAL_ROTATION}"
    fi
    echo "Setting new rotation value: ${new_rotation}"
    # Set new rotation
    sed_cmd="s/${ENTRY_KEY}=${CURRENT_ROTATION}/${ENTRY_KEY}=${new_rotation}/"
    sudo sed --in-place "${sed_cmd}" "${CONFIG_FILE}"
    # Make sure changes get written to disk
    echo "Performing sync"
    sudo sync
}


###########################################################################
# Ask user if they want a reboot
###########################################################################
function ask_for_reboot() {
    local reboot_yn="N"
    echo "You need to restart in order for the changes to take affect."
    echo "Would you like to restart now?"
    echo ""
    read -p "(y/N): " reboot_yn
    # Check if user just pressed enter, set to default
    if [ "${reboot_yn}" == "" ]
    then
        reboot_yn="N"
    fi
    # get 1st character only
    reboot_yn="${reboot_yn:0:1}"
    # uppercase string
    reboot_yn="${reboot_yn^^}"
    if [ "${reboot_yn}" == "Y" ]
    then
        echo "Performing reboot..."
        reboot
    else
        echo "Please reboot at your convenience."
    fi
}


main
